<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>ClearBoldDates</title>

<meta name="Microsoft.Help.F1" content="clearbolddates_func" />
</head> 
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="psrefp313.htm" title="Previous"><img src="../image/arrow-left.png" alt="psrefp313.htm"/></a></span><span class="nextbtn"><a href="psrefp315.htm" title="Next"><img src="../image/arrow-right.png" alt="psrefp315.htm"/></a></span></p><a name="babgccgj"></a><h1>ClearBoldDates PowerScript function</h1><a name="TI1140"></a><h4>Description</h4><p>Clears all bold date settings that had been set with <span class="cmdname">SetBoldDate</span>.</p>

<h4>Controls</h4><p>MonthCalendar control</p>

<h4>Syntax</h4><pre class="syntax"><span class="variable">controlname</span>.<span class="cmdname">ClearBoldDates</span> ( )</pre><a name="TI1141"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Argument</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">controlname</span></p>
</td>
<td rowspan="1"><p class="firstpara">The name of the MonthCalendar control
from which you want to clear the bold dates</p>
</td>
</tr>
</table>

<h4>Return Values</h4><p><span class="dt">Integer</span>. Returns 0 for success and -1
for failure.</p>

<h4>Usage</h4><p>You can use the <span class="cmdname">SetBoldDate</span> function to
specify that selected dates, such as holidays, display in bold. <span class="cmdname">ClearBoldDates</span> clears
all such settings. To clear individual bold dates, use the <span class="cmdname">SetBoldDate</span> function
with the <span class="variable">onoff</span> parameter set to <span class="cmdname">false</span>.</p>

<a name="EX0"></a><h4>Examples</h4><div class="example"><p>This example clears all bold settings in the control <span class="dbobject">monthCalVacations</span>:</p><pre class="userinput">integer li_return<br/>li_return = monthCalVacation.ClearBoldDates()</pre>
</div>
<a name="SA0"></a><h4>See Also</h4><ul class="simplelist"><li><p class="li"><a href="psrefp871.htm#BABBAJAD">SetBoldDate</a></p>
  </li>
</ul>

</body>
</html>

