<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Using a Web service to update the database</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp602.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp602.htm"/></a></span><span class="nextbtn"><a href="pbugp604.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp604.htm"/></a></span></p><a name="chdejjje"></a><h1>Using a Web service to update the database</h1>
<p>You
can use a DataWindow with a Web service data source to update a database.
Support for updating data requires one or more <acronym title="wisdull">WSDL</acronym> files that describe methods
and parameters that can be called by the DataWindow engine for insert,
delete, or update operations.</p>
<a name="chdibgjc"></a><div class="brhd"><h4>Generating or selecting
an assembly</h4>
<p>The <acronym title="wisdull">WSDL</acronym> files are not
required on runtime computers. They are used to generate assembly
files that are deployed with the application. If you have an existing
assembly file that allows you to update data in your DataWindow objects,
you can select that assembly instead of generating a new one from
the Web Services Update dialog box. You can generate or select separate assemblies
for insert, delete, and update operations.</p>
</div><a name="TI726"></a><div class="brhd"><h4>Insert, delete, and update operations</h4>
<p>The insert, delete, and update operations imply different
things depending on the original data source. When you insert a
DataWindow row for an RDBMS, a new row is added to the database;
when the data source is an array of structures, a new structure
instance is added to the array; and when the data source is an array
of simple types, a new instance of the simple type is added to the
array. The delete operation removes a database row or an instance
in an array, and the update operation modifies a database row or
an instance in an array.</p>
</div><p>For each operation, you must map DataWindow column values
or expressions to Web service input parameters. At runtime when
performing one of these operations, the DataWindow binds column
or expression values to parameters as instructed and calls the Web
service method. The DataWindow engine does not know what actually
happens in the Web service component (that is, how the component
implements the update), only whether it returns a success or failure
message.</p>
<p><a href="pbugp603.htm#CHDFEIEJ">Figure 21-1</a> displays
the Web Service Update dialog box. You use this dialog box to bind
to Web service parameters to DataWindow columns or expressions. Unlike
the retrieve call, DataWindow update operations can handle bidirectional
parameters. However, you can select an expression or computed column
only for an update method input parameter.</p>
<a name="chdfeiej"></a><div class="fig"><p class="title">Figure 21-1: Web Service Update dialog
box</p>
<div class="fig"><img src="../image/wsupdate.gif" alt="wsupdate.gif"/></div>
</div><div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To use a Web service to update the database</p>
<ol type="1"><li class="fi"><p>In the DataWindow painter, select Rows&gt;Web
Service Update to display the Web Service Update dialog box.</p>  </li>
<li class="ds"><p>Select the tab for the Web service update method
(Update, Insert, or Delete) with which you want to associate a Web
service.</p>  </li>
<li class="ds"><p>Click the browse button next to the <acronym title="wisdull">WSDL</acronym> Filename text box to browse
to a <acronym title="wisdull">WSDL</acronym> file describing the
Web service you want to use to update the DataWindow, and click
OK.</p><p>You use a <acronym title="wisdull">WSDL</acronym> file to
generate an assembly that you can deploy with your Web service DataWindow
application. You can override the default assembly name that will
be generated if you enter an existing assembly in the following
step of this procedure.</p><p>If you already have an assembly that you want to use to update
the DataWindow, you can skip the current step and select the assembly
that you want in step 4.</p><p>You can use the Reset button to clear all entries in the Web
Service Update dialog box.</p>  </li>
<li class="ds"><p>(Optional) Type an assembly name in the Assembly
Name text box to override a default assembly name that you want
to generate from a <acronym title="wisdull">WSDL</acronym> file,
or browse to an existing assembly file that describes the Web service you
want to use to update the DataWindow, and click OK.</p><p>Although you can browse to any mapped directory to find an
assembly file for update operations, you must make sure to copy
the assembly under the current target directory. All assemblies
for retrieving and updating a Web service DataWindow must be deployed
to the same directory as the application executable file, or retrieve
and update operations will not be able to work at runtime.</p>  </li>
<li class="ds"><p>Click Generate if you want to generate and load
an assembly file, or click Load if you entered an existing assembly
file name in step 4.</p><p>After you click Generate, an assembly file is created with
a default name from the <acronym title="wisdull">WSDL</acronym> file
or from a name that you entered in the previous step.</p><p>After you generate the assembly from a <acronym title="wisdull">WSDL</acronym> file
or load an existing assembly, the Web services in that file are
added to the Web Service Name drop-down list and the methods for
the Web services are added to the Method Name drop-down list.</p>  </li>
<li class="ds"><p>Select a Web service name and method name from
the list of Web services and methods.</p><p>The parameters used in the Web service method are displayed
in the Argument Name list in the order in which they are defined.
Column Name lists the columns used in your DataWindow object.</p>  </li>
<li class="ds"><p>Associate a column in the DataWindow object or
an expression with a method parameter.</p><p>If a Web service method uses parameters that are not matched
to column names, you can substitute the value from a DataWindow
object computed field or expression.</p><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Matching a column to a Web service method parameter</span> <p class="notepara">You must be careful to correctly match a column in the DataWindow object
to a method parameter, since PowerBuilder is able to verify only that
datatypes match.</p>
</div>  </li>
<li class="ds"><p>If the parameter is to receive a column value,
indicate whether the parameter will receive the updated column value
entered through the DataWindow object or retain the original column
value from the database.</p><p>Typically, you select Use Original when the Web service parameter
is used in the <span class="cmdname">WHERE</span> clause of an <span class="cmdname">UPDATE or DELETE</span> <acronym title="sequel">SQL</acronym> statement for a Web service method.
If you do not select Use Original, the parameter uses the new value
entered for that column. Typically, you would use the new value when
the Web service parameter is needed for an <span class="cmdname">INSERT</span> <acronym title="sequel">SQL</acronym> statement for the method, or
if it is set in an <span class="cmdname">UPDATE</span> <acronym title="sequel">SQL</acronym> statement.</p>  </li></ol>
</div><a name="chdiedfb"></a><div class="brhd"><h4>Regenerating an assembly </h4>
<p>If you need to regenerate an assembly for a DataWindow that
uses a Web service data source for retrieval, update, insert, or
delete operations, you must add the following line to the [DataWindow] section
of the <span class="filepath">PBDW.INI</span> file:</p><pre class="userinput">GenerateWSAssembliesOnCompile=YES</pre>
<p>After you set this property in the PBDW.INI file, PowerBuilder regenerates
the assembly on each compilation of the target containing the DataWindow.</p>
</div><a name="TI727"></a><div class="brhd"><h4>Using the WSError event</h4>
<p>Because a DataWindow with a Web service data source does not
pass back failure messages in a return code during retrieve, insert,
or update operations, you must use the WSError event to obtain such
error information.</p>
<p>For more information on the WSError event, see WSError in
the <span class="doctitle">DataWindow Reference</span> or in the online Help.</p>
</div><a name="TI728"></a><div class="brhd"><h4>The WebServiceException object</h4>
<p>Because a DataWindow with a Web service data source does not
pass back failure messages in a return code during retrieve, insert,
or update operations, you must use the WebServiceException object
to obtain such error information. The parameters in the following
table are exposed in the WebServiceException object when an error
occurs:</p>
<a name="TI729"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Argument</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">operation</span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="dt">String</span> for the type
of operation (Retrieve, Update, Insert, Delete, Connect, or Disconnect)</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">rowNumber</span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="dt">Int32</span> for the row
number or 0 if not applicable, such as when an error occurs during
connection to the Web service</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">buffername</span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="dt">String</span> for the name
of the buffer being accessed while the error occurred (Primary,
Filter, or Delete)</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">assembly</span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="dt">String</span> for the name
of the assembly being used</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">method</span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="dt">String</span> for the name
of the Web service method invoked</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">returnCode</span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="dt">Int32</span> for the return
code from the Web service</p>
</td>
</tr>
</table>
</div>
</body>
</html>

