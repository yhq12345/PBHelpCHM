<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Dot notation </title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp18.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp18.htm"/></a></span><span class="nextbtn"><a href="apptechp20.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp20.htm"/></a></span></p><a name="x-ref354898923"></a><h1>Dot notation </h1>
<p>Dot notation lets you qualify the item you are referring to&#8212;instance variable,
property, event, or function&#8212;with the object that owns
it.</p>
<p>Dot notation is for objects. You do not use dot notation for
global variables and functions, because they are independent of
any object. You do not use dot notation for shared variables either,
because they belong to an object class, not an object instance.</p>
<a name="TI69"></a><div class="brhd"><h4>Qualifying a reference</h4>
<p>Dot notation names an object variable as a qualifier to the
item you want to access:</p><pre class="syntax"><span class="variable">objectvariable</span>.<span class="variable">item</span></pre>
<p>The object variable name is a qualifier that identifies the
owner of the property or other item.</p>
<a name="TI70"></a><div class="formalpara"><p class="title">Adding a parent qualifier</p><p class="firstpara">To fully identify an object, you can use additional dot qualifiers
to name the parent of an object, and its parent, and so on:</p>
</div><pre class="syntax"><span class="variable">parent</span>.<span class="variable">objectvariable</span>.<span class="variable">item</span></pre>
<p>A <span class="glossterm">parent</span> object contains the child
object. It is not an ancestor-descendent relationship. For example,
a window is a control's parent. A Tab control is the parent
of the tab pages it contains. A Menu object is the parent of the
Menu objects that are the items on that menu.</p>
<a name="TI71"></a><div class="formalpara"><p class="title">Many parent levels</p><p class="firstpara">You can use parent qualifiers up to the level of the application.
You typically need qualifiers only up to the window level.</p>
</div><p>For example, if you want to call the <span class="cmdname">Retrieve</span> function
for a DataWindow control on a tab page, you might qualify the name
like this:</p><pre class="userinput">w_choice.tab_alpha.tabpage_a.dw_names.Retrieve()</pre>
<p>Menu objects often need several qualifiers. Suppose a window <span class="dbobject">w_main</span> has
a menu object <span class="dbobject">m_mymenu</span>, and <span class="dbobject">m_mymenu</span> has
a File menu with an Open item. You can trigger the Open item's <span class="dbobject">Selected</span> event
like this:</p><pre class="userinput">w_main.m_mymenu.m_file.m_open.EVENT Selected()</pre>
<p>As you can see, qualifying a name gets complex, particularly
for menus and tab pages in a Tab control.</p>
<a name="TI72"></a><div class="formalpara"><p class="title">How many qualifiers?</p><p class="firstpara">You need to specify as many qualifiers as are required to
identify the object, function, event, or property.</p>
</div><p>A parent object knows about the objects it contains. In a
window script, you do not need to qualify the names of the window's
controls. In scripts for the controls, you can also refer to other
controls in the window without a qualifier.</p>
<p>For example, if the window <span class="dbobject">w_main</span> contains
a DataWindow control <span class="dbobject">dw_data</span> and a
CommandButton <span class="dbobject">cb_close</span>, a script for
the CommandButton can refer to the DataWindow control without a
qualifier:</p><pre class="userinput">dw_data.AcceptText()<br/>dw_data.Update()</pre>
<p>If a script in another window or a user object refers to the
DataWindow control, the DataWindow control needs to be qualified
with the window name:</p><pre class="userinput">w_main.dw_data.AcceptText()</pre>
</div><a name="TI73"></a><div class="brhd"><h4>Referencing objects</h4>
<p>There are three ways to qualify an element of an object in
the object's own scripts:</p><ul><li><p class="firstbullet">Unqualified:</p><pre class="userinput">li_index = SelectItem(5)</pre>
<p class="lipara">An unqualified name is unclear and might result in ambiguities
if there are local or global variables and functions with the same
name.</p>  </li>
<li><p class="ds">Qualified with the object's name:</p><pre class="userinput">li_index = lb_choices.SelectItem(5)</pre><p class="lipara">Using the object name in the object's own script
is unnecessarily specific.</p>  </li>
<li><p class="ds">Qualified with a generic reference to the object:</p><pre class="userinput">li_index = This.SelectItem(5)</pre><p class="lipara">The pronoun <span class="cmdname">This</span> shows that the item belongs
to the owning object.</p>  </li>
</ul>
<a name="TI74"></a><div class="formalpara"><p class="title">This pronoun</p><p class="firstpara">In a script for the object, you can use the pronoun <span class="cmdname">This</span> as
a generic reference to the owning object:</p>
</div><pre class="syntax">This.<span class="variable">property</span></pre><pre class="syntax">This.<span class="variable">function</span></pre>
<p>Although the property or function could stand alone in a script
without a qualifier, someone looking at the script might not recognize
the property or function as belonging to an object. A script that
uses <span class="cmdname">This</span> is still valid if you rename the object.
The script can be reused with less editing.</p>
<p>You can also use <span class="cmdname">This</span> by itself as a reference
to the current object. For example, suppose you want to pass a DataWindow
control to a function in another user object:</p><pre class="userinput">uo_data.uf_retrieve(This)</pre>
<p>This example in a script for a DataWindow control sets an
instance variable of type DataWindow so that other functions know
the most recently used DataWindow control:</p><pre class="userinput">idw_currentdw = This</pre>
<a name="TI75"></a><div class="formalpara"><p class="title">Parent pronoun</p><p class="firstpara">The pronoun <span class="cmdname">Parent</span> refers to the parent
of an object. When you use <span class="cmdname">Parent</span> and you rename
the parent object or reuse the script in other contexts, it is still
valid.</p>
</div><p>For example, in a DataWindow control script, suppose you want
to call the <span class="cmdname">Resize</span> function for the window. The
DataWindow control also has a <span class="cmdname">Resize</span> function,
so you must qualify it:</p><pre class="userinput">// Two ways to call the window function<br/>w_main.Resize(400, 400)<br/>Parent.Resize(400, 400)</pre>
<pre class="userinput">// Three ways to call the control's function<br/>Resize(400, 400)<br/>dw_data.Resize(400, 400)<br/>This.Resize(400, 400)</pre>
<a name="TI76"></a><div class="formalpara"><p class="title">GetParent function</p><p class="firstpara">The <span class="cmdname">Parent</span> pronoun works only within dot
notation. If you want to get a reference to the parent of an object,
use the <span class="cmdname">GetParent</span> function. You might want to
get a reference to the parent of an object other than the one that
owns the script, or you might want to save the reference in a variable:</p><pre class="userinput">window w_save<br/>w_save = dw_data.GetParent()</pre>
</div><p>For example, in another CommandButton's Clicked event
script, suppose you wanted to pass a reference to the control's
parent window to a function defined in a user object. Use <span class="cmdname">GetParent</span> in
the function call:</p><pre class="userinput">uo_winmgmt.uf_resize(This.GetParent(), 400, 600)</pre>
<a name="TI77"></a><div class="formalpara"><p class="title">ParentWindow property and function</p><p class="firstpara">Other tools for getting the parent of an object include:</p><ul><li><p class="firstbullet"><span class="glossterm">ParentWindow property</span> &#8211; used
in a menu script to refer to the window that is the parent of the
menu</p>
  </li>
<li><p class="ds"><span class="glossterm">ParentWindow function</span> &#8211; used
in any script to get a reference to the window that is the parent
of a particular window</p>  </li>
</ul>
</div><p>For more about these pronouns and functions,
see the <span class="doctitle">PowerScript Reference</span>.</p>
</div><a name="TI78"></a><div class="brhd"><h4>Objects in a container object</h4>
<p>Dot notation also allows you to reach inside an object to
the objects it contains. To refer to an object inside a container,
use the Object property in the dot notation. The structure of the
object in the container determines how many levels are accessible:</p><pre class="syntax"><span class="variable">control</span>.Object.<span class="variable">objectname</span>.<span class="variable">property</span></pre><pre class="syntax"><span class="variable">control</span>.Object.<span class="variable">objectname</span>.Object.<span class="variable">qualifier</span>.<span class="variable">qualifier</span>.<span class="variable">property</span></pre>
<p>Objects that you can access using the Object property are:</p>
<ul><li><p class="firstbullet">DataWindow objects
in DataWindow controls</p>
  </li>
<li><p class="ds">External OLE objects in OLE controls</p>  </li>
</ul>
<p>These expressions refer to properties of the DataWindow object
inside a DataWindow control:</p><pre class="userinput">dw_data.Object.emp_lname.Border<br/>dw_data.Object.nestedrpt[1].Object.salary.Border</pre>
<a name="TI79"></a><div class="formalpara"><p class="title">No compiler checking</p><p class="firstpara">For objects inside the container, the compiler cannot be sure
that the dot notation is valid. For example, the DataWindow object
is not bound to the control and can be changed at any time. Therefore,
the names and properties after the Object property are checked for
validity during execution only. Incorrect references cause an execution
error.</p>
</div><a name="TI80"></a><div class="formalpara"><p class="title">For more information</p><p class="firstpara">For more information about runtime checking, see <a href="apptechp26.htm#X-REF354823874">"Optimizing expressions
for DataWindow and external objects"</a>.</p>
</div><p>For more information about dot notation for properties and
data of DataWindow objects and handling errors, see the <span class="doctitle">DataWindow
Reference</span>.</p>
<p>For more information about OLE objects and dot notation for
OLE automation, see <a href="apptechp219.htm#CAIBCEAE">Chapter 19, "Using OLE in an Application ."</a></p>
</div>
</body>
</html>

