<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>CrosstabAvg</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="dwrefp36.htm" title="Previous"><img src="../image/arrow-left.png" alt="dwrefp36.htm"/></a></span><span class="nextbtn"><a href="dwrefp38.htm" title="Next"><img src="../image/arrow-right.png" alt="dwrefp38.htm"/></a></span></p><a name="caicgage"></a><h1>CrosstabAvg DataWindow expression function</h1><a name="TI105"></a><h4>Description</h4><p>Calculates the average of the values returned by an expression
in the values list of the crosstab. When the crosstab definition
has more than one column, <span class="cmdname">CrosstabAvg</span> can also
calculate averages of the expression's values for groups of
column values.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">For crosstabs only</span> <p class="notepara">You can use this function <span class="emphasis">only</span> in a crosstab DataWindow object.</p>
</div>
<h4>Syntax</h4><pre class="syntax"><span class="cmdname">CrosstabAvg</span> ( <span class="variable">n</span> {, <span class="variable">column</span>, <span class="variable">groupvalue </span>} )</pre><a name="TI106"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Argument</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">n</span></p>
</td>
<td rowspan="1"><p class="firstpara">The number of the crosstab-values expression
for which you want the average of the returned values. The crosstab
expression must be numeric.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">column</span> (optional)</p>
</td>
<td rowspan="1"><p class="firstpara">The number of the crosstab column as
it is listed in the Columns box of the Crosstab Definition dialog
box for which you want intermediate calculations.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">groupvalue</span> (optional)</p>
</td>
<td rowspan="1"><p class="firstpara">A string whose value controls the grouping
for the calculation. <span class="variable">Groupvalue</span> is usually
a value from another column in the crosstab. To specify the current
column value in a dynamic crosstab, rather than a specific value,
specify @ plus the column name as a quoted string.</p>
</td>
</tr>
</table>

<h4>Return Values</h4><p>Double. Returns the average of the crosstab values returned
by expression <span class="variable">n</span> for all the column values or,
optionally, for a subset of column values. To return a decimal datatype,
use <a href="dwrefp38.htm#CAICGAGED">CrosstabAvgDec</a>.</p>

<h4>Usage</h4><p>This function is meaningful <span class="emphasis">only</span> for
the average of the values of the expression in a <span class="emphasis">row</span> in
the crosstab. This means you can use it only in the detail band,
not in a header, trailer, or summary band.</p>
<p>Null values are ignored and are not included in the average.</p>
<a name="TI107"></a><h4>How functions in a crosstab are used</h4>
<p>When a crosstab is generated from your definition, the appropriate
computed fields are automatically created using the Crosstab functions.
To understand the functions, consider a crosstab with two columns
(year and quarter), a row (product), and the values expression Avg(amount
for crosstab). </p>
<p>The Crosstab Definition dialog box looks like this.</p>
<div class="fig"><img src="../image/dwfct13.gif" alt="dwfct13.gif"/></div>
<p>When you define the crosstab described above, the painter
automatically creates the appropriate computed fields. A computed
field named avg_amount returns the average of the quarterly
figures for each year. Its expression is:</p><pre class="userinput"><span class="emphasis">CrosstabAvg</span>(1, 2, "@year")</pre>
<p>A second computed field named grand_avg_amount
computes the average of all the amounts in the row. Its expression
is:</p><pre class="userinput"><span class="emphasis">CrosstabAvg</span>(1)</pre>
<p>Other computed fields in the summary band use the <span class="cmdname">Avg</span> function
to display the average of the values in the amount column, the yearly
averages, and the final average.</p>
<p>The crosstab in the Design view looks like this.</p>
<div class="fig"><img src="../image/dwfct14.gif" alt="dwfct14.gif"/></div>
<p>Each row in the crosstab (after adjusting the column widths)
has cells for the amounts in the quarters, a repeating cell for
the yearly average, and a grand average. The crosstab also displays
averages of the amounts for all the financial codes in the quarters
in the summary band at the bottom.</p>
<div class="fig"><img src="../image/dwfct15.gif" alt="dwfct15.gif"/></div>
<div class="fig"><img src="../image/dwfct16.gif" alt="dwfct16.gif"/></div>
<a name="TI108"></a><h4>What the function arguments mean</h4>
<p>When the crosstab definition has more than one column, you
can specify column qualifiers for any of the <span class="cmdname">Crosstab</span> functions,
so that the crosstab displays calculations for groups of column values.
As illustrated previously, when year and quarter are the columns
in the crosstab, the expression for the computed field is:</p><pre class="userinput"><span class="emphasis">CrosstabAvg</span>(1, 2, "@year")</pre>
<p>The value 2 refers to the quarter column (the second column
in the Crosstab Definition dialog) and "@year" specifies
grouping values from the year column (meaning the function will
average values for the quarters within each year). The value 1 refers
to the crosstab-values expression that will be averaged. In the
resulting crosstab, the computed field repeats in each row after the
cells for the quarters within each year.</p>
<a name="TI109"></a><h4>Tips for defining crosstabs</h4>
<p>When you define a crosstab with more than one column, the
order of the columns in the Columns box of the Crosstab Definition
dialog box governs the way the columns are grouped. To end up with
the most effective expressions, make the column that contains the grouping
values (for example, year or department) the first column in the Columns
box and the column that contains the values to be grouped (for example,
quarter or employee) second.</p>
<p>To display calculations for groups of rows, define groups
as you would for other DataWindow presentation styles and define
computed fields in the group header or footer using noncrosstab
aggregation functions, such as <span class="cmdname">Avg</span>, <span class="cmdname">Sum</span>,
or <span class="cmdname">Max</span>.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Reviewing the expressions</span> <p class="notepara">To review the expressions defined for the crosstab values,
open the Crosstab Definition dialog box (select Design&gt;Crosstab
from the menubar).</p>
</div>
<a name="EX0"></a><h4>Examples</h4><div class="example"><p>The first two examples use the crosstab expressions
shown below:</p><pre class="userinput">Count(emp_id for crosstab),Sum(salary for crosstab)</pre>
</div><div class="example"><p>This expression for a computed field in the crosstab
returns the average of the employee counts (the first expression):</p><pre class="userinput"><span class="emphasis">CrosstabAvg</span>(1)</pre>
</div><div class="example"><p>This expression for a computed field in the crosstab
returns the average of the salary totals (the second expression):</p><pre class="userinput"><span class="emphasis">CrosstabAvg</span>(2)</pre>
</div><div class="example"><p>Consider a crosstab that has two columns (region
and city) and the values expression Avg(sales for crosstab). This
expression for a computed field in the detail band computes the
average sales over all the cities in a region:</p><pre class="userinput"><span class="emphasis">CrosstabAvg</span>(1, 2, "@region")</pre>
</div><div class="example"><p>This expression for another computed field in the
same crosstab computes the grand average over all the cities:</p><pre class="userinput"><span class="emphasis">CrosstabAvg</span>(1)</pre>
</div>
<a name="SA0"></a><h4>See Also</h4><ul class="simplelist"><li><p class="li"><a href="dwrefp38.htm#CAICGAGED">CrosstabAvgDec</a></p>
  </li>
<li><p class="ds"><a href="dwrefp39.htm#CAIBCHGE">CrosstabCount</a></p>  </li>
<li><p class="ds"><a href="dwrefp40.htm#CAIDGJBH">CrosstabMax</a></p>  </li>
<li><p class="ds"><a href="dwrefp42.htm#CAIBJFGH">CrosstabMin</a></p>  </li>
<li><p class="ds"><a href="dwrefp44.htm#CAIBIJHD">CrosstabSum</a></p>  </li>
</ul>

</body>
</html>

