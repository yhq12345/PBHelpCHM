<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Checking objects out from source control</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp86.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp86.htm"/></a></span><span class="nextbtn"><a href="pbugp88.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp88.htm"/></a></span></p><a name="babdjdch"></a><h2>Checking objects out from source control</h2>
<a name="TI103"></a><div class="brhd"><h4>What happens on checking out an object</h4>
<p>When you check out an object, PowerBuilder:</p><ul><li><p class="firstbullet">Locks
the object in the archive so that no one else can modify it&#8212;unless your
source control system permits multiple user checkouts</p>
  </li>
<li><p class="ds">Copies the object to the directory for the target
to which it belongs</p>  </li>
<li><p class="ds">For a PowerScript object, compiles the object and
regenerates it in the target <acronym title="pibble">PBL</acronym> to
which it is mapped </p>  </li>
<li><p class="ds">Displays a check mark icon next to the object in
your System Tree and in your Library painter to show that the object
has been checked out </p>  </li>
</ul>
</div><a name="TI104"></a><div class="brhd"><h4>Checking out multiple objects</h4>
<p>If you select the Check Out menu item for a PowerBuilder target
that is not already checked out, and at least one of the objects
in that target is available for checkout, PowerBuilder displays
a dialog box that prompts you to:</p><ul><li><p class="firstbullet">Select multiple
files contained in the target</p>
  </li>
<li><p class="ds">Check out the target file only</p>  </li>
</ul>
<p>If you select the multiple file option, or if the target file
is already checked out, the Check Out dialog box displays the list
of objects from that target that are available for checkout. A check
box next to each object in the list lets you choose which objects
you want to check out. By default, check boxes are selected for
all objects that are not currently checked out of source control.</p>
<p>The Deselect All button in the Check Out dialog box lets you
clear all the check boxes with a single click. When none of the
objects in the list is selected, the button text becomes Select
All, and you can click the button to select all the objects in the
list.</p>
<p>You can also select multiple objects (without selecting a target)
in the List view of the Library painter. The PowerBuilder SCC API
does not let you check out an object that you or someone else has
already checked out or that is not yet registered with source control.
If you use multiple object selection to select an object that is
already checked out, PowerBuilder does not include this object in the
list view of the Check Out dialog box.</p>
</div><a name="ciheibdf"></a><div class="brhd"><h4>Multiple
user checkout</h4>
<p>Checking out an object from a source control system usually
prevents other users from checking in modified versions of the same
object. Some source control systems, such as Serena Version Manager
(formerly Merant PVCS) and MKS Source Integrity, permit multiple
user checkouts. In these systems, you can allow shared checkouts
of the same object. </p>
<p>By default, PowerBuilder recognizes shared checkouts from
SCC providers that support multiple user checkouts. PowerBuilder
shows a red check mark as part of a compound icon to indicate that
an object is checked out to another user in a shared (nonexclusive)
mode. You can check out an object in shared mode even though another
user has already checked the object out. </p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Managing multiple user check-ins</span> <p class="notepara">If you allow multiple user checkouts, the SCC administrator
should publish a procedure that describes how to merge changes to
the same object by multiple users. Merge functionality is not automatically
supported by the SCC API, so checking in an object in shared mode
might require advanced check-in features of the source control system.
Merging changes might also require using the source control administration
utility instead of the PowerBuilder user interface.</p>
</div><p>If your SCC provider permits multiple user checkouts, you
can still ensure that an item checked out by a user is exclusively
reserved for that user until the object is checked back in, but
only if you add the following instruction to the Library section
of the <span class="filepath">PB.INI</span> file:</p><pre class="userinput">[Library]</pre><pre class="userinput">SccMultiCheckout=0</pre>
<p>After you add this <span class="filepath">PB.INI</span> setting, or
if your SCC provider does not support multiple user checkouts, you
will not see the compound icons with red check marks, and all items
will be checked out exclusively to a single user. For source control
systems that support multiple user checkouts, you can re-enable
shared checkouts by setting the SccMultiCheckout value to <code class="ce">1</code> or <code class="ce">-1</code>.</p>
</div><a name="TI105"></a><div class="brhd"><h4>Creating a source control branch</h4>
<p>If your source control system supports branching and its SCC
API lets you check out a version of an object that is not the most
recent version in source control, you can select the version you
want in the Advanced Check Out dialog box (that you access by clicking
the Advanced button in the Check Out dialog box). When you select
an earlier version, PowerBuilder displays a message box telling
you it will create a branch when you check the object back in. You
can click Yes to continue checking out the object or No to leave
the object unlocked in the source control project. If this is part
of a multiple object checkout, you can select Yes To All or No To
All.</p>
</div><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">If you want just a read-only copy of the latest version
of an object</span> <p class="notepara">Instead of checking out an object and locking it in the source
control system, you can choose to get the latest version of the
object with a read-only attribute. See <a href="pbugp90.htm#BABEAGGB">"Synchronizing objects with
the source control server"</a>.</p>
</div><div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To check out an object from source control:</p>
<ol type="1"><li class="fi"><p>Right-click the object in the System Tree
or in a Library painter view and select Check Out from the pop-up
menu</p><p><span class="emphasis">or</span></p><p>Select the object in a Library painter view and select
Entry&gt;Source Control&gt;Check Out from the Library
painter menu.</p><p>The Check Out dialog box displays the name of the object you
selected. For PowerScript objects, the object listing includes the
name of the <acronym title="pibble">PBL</acronym> that contains
the selected object.</p><p>If you selected multiple objects, the Check Out dialog box
displays the list of objects available for checkout. You can also
display a list of available objects when you select a target file
for checkout. A check mark next to an object in the list marks the
object as assigned for checkout. </p>  </li>
<li class="ds"><p>Make sure that the check box is selected next
to the object you want to check out, and click OK.</p>  </li></ol>
</div>
</body>
</html>

