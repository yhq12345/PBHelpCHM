<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>The EditMask edit style</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp625.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp625.htm"/></a></span><span class="nextbtn"><a href="pbugp627.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp627.htm"/></a></span></p><a name="x-ref356199281"></a><h2>The EditMask edit style</h2>
<p>Sometimes users need to enter data that has a fixed format.
For example, in North America phone numbers have a 3-digit area
code, followed by three digits, followed by four digits. You can
define an edit mask that specifies the format to make it easier
for users to enter values:</p>
<div class="fig"><img src="../image/dwdis18.gif" alt="dwdis18.gif"/></div>
<p>Edit masks consist of special characters that determine what
can be entered in the column. They can also contain punctuation
characters to aid users.</p>
<p>For example, to make it easier for users to enter phone
numbers in the proper format, specify this mask:</p><pre class="userinput">(###) ###-####</pre>
<p>At runtime, the punctuation characters display in the box
and the cursor jumps over them as the user types:</p>
<div class="fig"><img src="../image/dwdis19.gif" alt="dwdis19.gif"/></div>
<a name="TI765"></a><div class="brhd"><h4>Special characters and keywords</h4>
<p>Most edit masks use the same special characters as display
formats, and there are special considerations for using numeric,
string, date, and time masks. For information, see <a href="pbugp612.htm#BFCCJFHF">"Defining display formats"</a>.</p>
<p>The special characters you can use in string edit masks are
different from those you can use in string display formats. </p>
<a name="TI766"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 22-9: Special
characters for string edit masks</caption>
<tr><th rowspan="1"><p class="firstpara">Character</p>
</th>
<th rowspan="1"><p class="firstpara">Meaning</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">!</p>
</td>
<td rowspan="1"><p class="firstpara">Uppercase &#8211; displays all characters
with letters in uppercase</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">^</p>
</td>
<td rowspan="1"><p class="firstpara">Lowercase &#8211; displays all characters
with letters in lowercase</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">#</p>
</td>
<td rowspan="1"><p class="firstpara">Number &#8211; displays only numbers</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">a</p>
</td>
<td rowspan="1"><p class="firstpara">Alphanumeric &#8211; displays only
letters and numbers</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">X</p>
</td>
<td rowspan="1"><p class="firstpara">Any character &#8211; displays all
characters</p>
</td>
</tr>
</table>
<p>If you use the "#" or "a" special
characters in a mask, Unicode characters, spaces, and other characters
that are not alphanumeric do not display.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Semicolons invalid in EditMask edit styles</span> <p class="notepara">In a display format, you can use semicolons to separate sections
in number, date, time, and string formats. You cannot use semicolons
in an EditMask edit style.</p>
</div></div><a name="TI767"></a><div class="brhd"><h4>Keyboard behavior</h4>
<p>Note the following about how certain keystrokes behave in
edit masks:</p><ul><li><p class="firstbullet">Both Backspace
and Shift + Backspace delete the preceding character.</p>
  </li>
<li><p class="ds">Delete deletes everything that is selected.</p>  </li>
<li><p class="ds">Non-numeric edit masks treat any characters that
do not match the mask pattern as delimiters.</p>  </li>
</ul>
</div><p>Also, note certain behavior in Date edit masks:</p><ul><li><p class="firstbullet">Entering zero for the day
or month causes the next valid date to be entered. For example,
if the edit mask is DD/MM/YY, typing <code class="ce">00/11/01</code> results
in <code class="ce">01/11/01</code>.
You can override this behavior in the development environment by
adding the following lines to your <span class="filepath">PB.INI</span> file:</p><pre class="userinput">[Edit Mask Behaviors]<br/>AutocompleteDates=no</pre>
<p class="lipara">For deployed applications, the date is completed automatically
unless you provide a file called <span class="filepath">PB.INI</span> in
the same directory as the executable file that contains these lines.
Note that this section must be in a file called <span class="filepath">PB.INI</span>. Adding
the section to a different INI file shipped with the application
will have no effect. </p>  </li>
<li><p class="ds">You cannot use a partial mask, such as dd or mmm,
in a date edit mask. Any mask that does not include any characters
representing the year will be replaced by a mask that does.</p>  </li>
<li><p class="ds">The strings 00/00/00 or 00/00/0000
are interpreted as the <span class="cmdname">NULL</span> value for the column. </p>  </li>
</ul>
<a name="TI768"></a><div class="brhd"><h4>Using the Mask pop&#8211;up menu</h4>
<p>Click the button to the right of the Mask box on the Mask
property page to display a list that contains complete masks that
you can click to add to the mask box, as well as special characters
that you can use to construct your own mask. For example, the menu
for a Date edit mask contains complete masks such as mm/dd/yy
and dd/mmm/yyyy. It also has components such as
dd and jjj (for a Julian day). You might use these to construct
a mask like dd-mm-yy, typing in the hyphens as separators.</p>
</div><a name="TI769"></a><div class="brhd"><h4>Using masks with "as is" characters</h4>
<p>You can define a mask that contains "as is" characters
that always appear in the control or column. For example, you might
define a numeric mask such as <code class="ce">Rs0000.00</code> to
represent Indian rupees in a currency column. </p>
<p>However, you cannot enter a minus sign to represent negative
numbers in a mask that contains "as is" characters,
and the # special character is treated as a 0 character.
As a result, if you specify a mask such as ###,##0.00EUR,
a value such as 45,000 Euros would display with a leading zero:
045,000.00EUR. Note that you must always specify a mask that has
enough characters to display all possible data values. If the mask
does not have enough characters, for example if the mask is #,##0.00
and the value is 45000, the result is unpredictable.</p>
<p>The preferred method of creating a currency editmask is to
use the predefined <code class="ce">[currency(7)] -
International</code> mask. You can change the number
in parentheses, which is the number of characters in the mask including
two decimal places. When you use this mask, PowerBuilder uses the
currency symbol and format defined in the regional settings section
of the Windows control panel. You can enter negative values in a
column that uses a currency mask. </p>
</div><a name="TI770"></a><div class="brhd"><h4>Using spin controls</h4>
<p>You can define an edit mask as a spin control, a box that
contains up and down arrows that users can click to cycle through
fixed values. For example, you can set up a code table that provides
the valid entries in a column; users simply click an arrow
to select an entry. Used this way, a spin control works like a drop-down
list that displays one value at a time:</p>
<div class="fig"><img src="../image/dwdis21.gif" alt="dwdis21.gif"/></div>
<p>For more about code tables, see <a href="pbugp630.htm#BFCDHGFC">"Defining a code table"</a>.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To use an EditMask edit style:</p>
<ol type="1"><li class="fi"><p>Select EditMask in the Style Type box if it is
not already selected.</p>  </li>
<li class="ds"><p>Define the mask in the Mask box. Click the special characters
in the pop&#8211;up menu to use them in the mask. To display
the pop-up menu, click the button to the right of the Mask box.</p>  </li>
<li class="ds"><p>Specify other properties for the edit mask.</p><p>When you use your EditMask, check its appearance and behavior.
If characters do not appear as you expect, you might want to change
the font size or the size of the EditMask.</p>  </li></ol>
</div></div><a name="TI771"></a><div class="brhd"><h4>Using a drop-down calendar</h4>
<p>You can use a drop-down calendar option on any DataWindow column
with an EditMask edit style and a Date, DateTime, or TimeStamp datatype.
The DDCalendar EditMask property allows for separate selections
of the calendar month, year, and date. This option can be set in
a check box on the Edit page of the DataWindow painter Properties view when
a column with the EditMask edit style is selected. It can also be
set in code, as in this example for the <span class="cmdname">birth_date</span> column:</p><pre class="userinput">dw1.Modify("birth_date.EditMask.DDCalendar='Yes'")</pre>
<p>If you do not include script for client formatting in a Web
DataWindow, the drop&#8211;down calendar uses a default edit
mask to display the column data based on the client computer's
default localization settings. To make sure that dates selected
with the drop&#8211;down calendar option are displayed with the
desired edit mask, specify that the Client Formatting option be
included with the static JavaScript generated and deployed for the
DataWindow. </p>
<p>To conserve bandwidth, JavaScript for client formatting is
not included by default. To include this script, select the Client Formatting check
box on the Web Generation tab of the DataWindow Properties view. </p>
<p>The drop-down calendar option is supported in all Web DataWindow
rendering formats (HTML, XHTML, and XML).</p>
</div>
</body>
</html>

