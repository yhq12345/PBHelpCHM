<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Creating user-defined exception types</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp30.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp30.htm"/></a></span><span class="nextbtn"><a href="apptechp32.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp32.htm"/></a></span></p><a name="babcabdh"></a><h2>Creating user-defined exception types</h2>
<p>You can create your own user-defined exception types from
standard class user objects that inherit from Exception or RuntimeError
or that inherit from an existing user object deriving from Exception
or RuntimeError. </p>
<a name="TI116"></a><div class="brhd"><h4>Inherit from Exception object type</h4>
<p>Normally, user-defined exception types should inherit from
the Exception type or a descendant, since the RuntimeError type
is used to indicate system errors. These user-defined objects are
no different from any other nonvisual user object in the system.
They can contain events, functions, and instance variables. </p>
<p>This is useful, for example, in cases where a specific condition,
such as the failure of a business rule, might cause application
logic to fail. If you create a user-defined exception type to describe
such a condition and then catch and handle the exception appropriately,
you can prevent a runtime error. </p>
</div><a name="TI117"></a><div class="brhd"><h4>Throwing exceptions</h4>
<p>Exceptions can be thrown by the runtime engine to indicate
an error condition. If you want to signal a potential exception
condition manually, you must use the <span class="cmdname">THROW</span> statement. </p>
<p>Typically, the <span class="cmdname">THROW</span> statement is used
in conjunction with some user-defined exception type. Here is a
simple example of the use of the <span class="cmdname">THROW</span> statement:</p><pre class="userinput">Exception&#160;&#160;&#160; le_ex<br/>le_ex = create Exception<br/>Throw le_ex<br/>MessageBox ("Hmm", "We would never get here if" &amp;&#160;&#160;&#160;<br/>&#160;&#160;&#160;+ "the exception variable was not instantiated")</pre>
<p>In this example, the code throws the instance of the exception <span class="dbobject">le_ex</span>.
The variable following the <span class="cmdname">THROW</span> reserved word
must point to a valid instance of the exception object that derives
from Throwable. If you attempt to throw an uninstantiated Exception
variable, a NullObjectError is thrown instead, indicating a null
object reference in this routine. That could only complicate the error
handling for your application.</p>
</div><a name="TI118"></a><div class="brhd"><h4>Declaring exceptions thrown from functions</h4>
<p>If you signal an exception with the <span class="cmdname">THROW</span> statement
inside a method script&#8212;and do not surround the statement
with a try-catch block that can deal with that type of exception&#8212;you
must also declare the exception as an exception type (or as a descendant
of an exception type) thrown by that method. However, you do not
need to declare that a method can throw runtime errors, since PowerBuilder
does that for you.</p>
<p>The prototype window in the Script view of most PowerBuilder
painters allows you to declare what user-defined exceptions, if
any, can be thrown by a function or a user-defined event. You can
drag and drop exception types from the System Tree or a Library
painter view to the Throws box in the prototype window, or you can
type in a comma-separated list of the exception types that the method
can throw. </p>
</div><a name="TI119"></a><div class="brhd"><h4>Example</h4>
<a name="TI120"></a><div class="formalpara"><p class="title">Example catching a user-defined exception</p><p class="firstpara">This code displays a user&#8211;defined error when an <span class="cmdname">arccosine</span> argument,
entered by the application user, is not in the required range. The
try-catch block calls a method, <span class="cmdname">wf_acos</span>,
that catches the system error and sets and throws the user-defined
error:</p>
</div><pre class="userinput">TRY&#160;&#160;&#160;<br/>&#160;&#160;&#160;wf_acos()</pre><pre class="userinput">CATCH (uo_exception u_ex)&#160;&#160;&#160;<br/>&#160;&#160;&#160;MessageBox("Out of Range", u_ex.GetMessage())<br/>END TRY</pre>
<p>This code in the <span class="cmdname">wf_acos</span> method
catches the system error and sets and throws the user-defined error:</p>
<pre class="userinput">uo_exception lu_error<br/>Double ld_num<br/>ld_num = Double (sle_1.text)<br/>TRY<br/>&#160;&#160;&#160;sle_2.text = string (acos (ld_num))<br/>CATCH (runtimeerror er)&#160;&#160;&#160;<br/>&#160;&#160;&#160;lu_error = Create uo_exception<br/>&#160;&#160;&#160;lu_error.SetMessage("Value must be between -1" &amp;<br/>&#160;&#160;&#160;&#160;&#160;&#160;+ "and 1")<br/>&#160;&#160;&#160;Throw lu_error<br/>END TRY</pre>
</div><a name="TI121"></a><div class="brhd"><h4>Integration with EAServer</h4>
<p>If you declare exceptions on a method of a user object and
deploy the user object as a component to <abbr title="e a server">EAServer</abbr>,
the exceptions are translated to IDL (CORBA) as part of the method
prototype. This means that PowerBuilder components in <acronym title="e a server">EAServer</acronym> can be defined to throw
exceptions that can be handled by any type of <abbr title="e a server">EAServer</abbr> client
application. </p>
<a name="TI122"></a><div class="formalpara"><p class="title">Other benefits for EAServer applications</p><p class="firstpara">Another benefit for component development is that you can
handle runtime errors in the component. If you do not handle an
error, it is automatically translated into an exception and the component
stops executing. </p>
</div><p>PowerBuilder client applications that use <abbr title="e a server">EAServer</abbr> components can handle exceptions
thrown by any type of <abbr title="e a server">EAServer</abbr> component.
If a Java <abbr title="e a server">EAServer</abbr> component has
a method on it that is defined to throw an exception and a PowerBuilder
proxy is created to use that component, the method on the PowerBuilder
proxy is also declared to throw a user-defined exception. The definition
of the user-defined exception is created automatically at the time
of the PowerBuilder proxy creation.</p>
<p>For more information about error handling in <abbr title="e a server">EAServer</abbr> clients, see <a href="apptechp349.htm#CCJBBGBC">"Handling errors "</a>.</p>
<a name="TI123"></a><div class="formalpara"><p class="title">IDL restrictions</p><p class="firstpara">Deployment of components to <acronym title="e a server">EAServer</acronym> imposes restrictions
on the way you can use exception handling within PowerBuilder. Only
the public instance variables defined on the exception type are
actually translated to IDL. This is because IDL exceptions cannot
have methods declared on them. Therefore if the exception type has
methods defined on it, those methods can be called within the execution
of the component but cannot be called by client applications that
catch the exception thrown. </p>
</div><p>You must keep this restriction in mind when designing exception
objects for distributed applications, exposing all exception information
as public instance variables instead of through accessor methods
on an exception object. </p>
<p>Two other interface restrictions also apply to exception types
of a user object that is deployed as an <abbr title="e a server">EAServer</abbr> component.
Instance variables of exceptions on the user object methods cannot
have object datatypes. Null data is supported only for instance
variables with simple datatypes; if instance variables are structures
or arrays, null values for individual elements are not maintained.</p>
</div>
</body>
</html>

