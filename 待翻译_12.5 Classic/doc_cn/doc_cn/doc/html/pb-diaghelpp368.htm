<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Additional connection options</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pb-diaghelpp367.htm" title="Previous"><img src="../image/arrow-left.png" alt="pb-diaghelpp367.htm"/></a></span><span class="nextbtn"><a href="pb-diaghelpp369.htm" title="Next"><img src="../image/arrow-right.png" alt="pb-diaghelpp369.htm"/></a></span></p><a name="pb-helpid-dir-additional-connection-options"></a><h2>Additional connection options</h2>
<p>The tabbed pages in the Database Profile Setup - DirectConnect
dialog box include additional options that you can set if desired
to fine-tune your database connection and take advantage of DBMS-specific
features that the DirectConnect interface supports. Each connection
option corresponds to a DBParm parameter or (for Isolation Level
and AutoCommit Mode) SQLCA property that can be set in a PowerBuilder
application script by copying its syntax from the <a href="pb-diaghelpp369.htm#PB-HELPID-DIR-PREVIEW-TAB">Preview
tab (PowerBuilder only)</a>.</p>
<p>The following tables list the connection options on each tabbed
page and their corresponding DBParm parameter or SQLCA property.
Click the DBParm or SQLCA property for a complete description.</p>
<a name="pb-helpid-dir-connection-tab"></a><div class="brhd"><h4>Connection
tab</h4>
<a name="TI1663"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Option</p>
</th>
<th rowspan="1"><p class="firstpara">DBParm parameter or SQLCA property</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Isolation Level</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp191.htm#BFCEAHIJK">Lock</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">AutoCommit Mode</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp188.htm#BFCCFJCB">AutoCommit</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Commit on Disconnect</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp19.htm#BFCBIAID">CommitOnDisconnect</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Choose Gateway</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp180.htm#CDEBGHCA">TRS</a></p>
</td>
</tr>
</table>
</div><a name="pb-helpid-dir-reg-set-tab"></a><div class="brhd"><h4>Regional
Settings tab</h4>
<a name="TI1664"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Option</p>
</th>
<th rowspan="1"><p class="firstpara">DBParm parameter or SQLCA property</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Language</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp83.htm#CHDGCCFA">Language</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Character Set</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp15.htm#BFCEBABF">CharSet</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Locale</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp85.htm#BFCCBHED">Locale</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">UTF-8 Character Set Installed</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp185.htm#CCJBCFEJ">UTF8</a></p>
</td>
</tr>
</table>
</div><a name="pb-helpid-dir-system-tab"></a><div class="brhd"><h4>System
tab</h4>
<a name="TI1665"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Option</p>
</th>
<th rowspan="1"><p class="firstpara">DBParm parameter or SQLCA property</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">CSP Catalog Qualifier</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp168.htm#BFCDIEJE">SystemOwner</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">PowerBuilder Catalog Table Owner</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp111.htm#BFCDCDIB">PBCatalogOwner</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Host Request Lib Owner</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp71.htm#BFCCFEHC">HostReqOwner</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Use Procedure Syntax for RSPs</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp184.htm#BABFECAG">UseProcSyntax</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Table Criteria</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp170.htm#CCJDAIBH">TableCriteria</a></p>
</td>
</tr>
</table>
</div><a name="pb-helpid-dir-transaction-tab"></a><div class="brhd"><h4>Transaction
tab</h4>
<a name="TI1666"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Option</p>
</th>
<th rowspan="1"><p class="firstpara">DBParm parameter or SQLCA property</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Asynchronous</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp7.htm#BFCCABGC">Async</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Number of Seconds to Wait</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp40.htm#BFCCAJBF">DBGetTime</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Release Transaction Resources After Each
Request</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp135.htm#BFCCCBIE">Request</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Static Bind</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp163.htm#BFCDFDHG">StaticBind</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Cursors Declared Updatable</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp30.htm#BFCDAGAJ">CursorUpdate</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Format Arguments in Scientific Notation</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp64.htm#CCJBCBED">FormatArgsAsExp</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Retrieve Blocking Factor</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp11.htm#BFCBFJEA">Block
(DirectConnect and Adaptive Server Enterprise)</a></p>
</td>
</tr>
</table>
</div><a name="pb-helpid-dir-syntax-tab"></a><div class="brhd"><h4>Syntax
tab</h4>
<a name="TI1667"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Option</p>
</th>
<th rowspan="1"><p class="firstpara">DBParm parameter or SQLCA property</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Enclose Table and Column Names in Quotes</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp45.htm#BFCCBHDA">DelimitIdentifier</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Display Identifiers in Lowercase</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp89.htm#CDEBHCEC">LowerCaseIdent</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Show Warning Messages as Errors</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp158.htm#CHDFDBDF">ShowWarnings</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Date Format </p>
<p>DateTime Format</p>
<p>Time Format</p>
</td>
<td rowspan="1"><p class="firstpara">Not relevant. PowerBuilder uses ISO formats
when handling these datatypes with DirectConnect for OS/390.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Decimal Separator</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp43.htm#REF344205077">DecimalSeparator</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Qualify Identifiers with Owner Names</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp161.htm#BFCBEEEE">SQLQualifiers</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">DateTime Datatype Allowed in Where Clause</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp37.htm#BFCCHJBC">DateTimeAllowed</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Trim Trailing Spaces in Char Data</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp177.htm#BFCDBGDH">TrimSpaces</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Maximum Length of Long Varchar</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp42.htm#BFCDAJGG">DBTextLimit</a></p>
</td>
</tr>
</table>
</div><a name="pb-helpid-dir-network-tab"></a><div class="brhd"><h4>Network
tab</h4>
<a name="TI1668"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Option</p>
</th>
<th rowspan="1"><p class="firstpara">DBParm parameter or SQLCA property</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Application Name</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp6.htm#BFCCJGAJ">AppName</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Packet Size</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp110.htm#BFCBBGFC">PacketSize
(ASE, DIR, SNC, SYC)</a></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Maximum Connections for this Context</p>
</td>
<td rowspan="1"><p class="firstpara"><a href="dbparmp92.htm#BFCBJIEE">MaxConnect</a></p>
</td>
</tr>
</table>
</div>
</body>
</html>

