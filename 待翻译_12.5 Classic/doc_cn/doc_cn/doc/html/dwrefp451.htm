<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Handling errors from DataWindow property expressions in PowerBuilder</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="dwrefp450.htm" title="Previous"><img src="../image/arrow-left.png" alt="dwrefp450.htm"/></a></span><span class="nextbtn"><a href="dwrefp452.htm" title="Next"><img src="../image/arrow-right.png" alt="dwrefp452.htm"/></a></span></p><a name="baccibej"></a><h2>Handling errors from DataWindow property expressions in PowerBuilder</h2>
<a name="TI1177"></a><div class="brhd"><h4>What causes errors</h4>
<p>In PowerBuilder, an invalid DataWindow property expression
causes a runtime error in your application. A runtime error causes
the application to terminate unless you catch the error in a runtime
error handler or unless there is a script for the Error event.</p>
</div><a name="TI1178"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 5-4: Conditions that invalidate DataWindow property expressions</caption>
<tr><th rowspan="1"><p class="firstpara">Conditions that cause errors</p>
</th>
<th rowspan="1"><p class="firstpara">Possible causes</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Invalid names of controls within the DataWindow
object</p>
</td>
<td rowspan="1"><p class="firstpara">Mistyping, which the compiler does not catch
because it does not evaluate the expression.</p>
<p>A different DataWindow object has been inserted in the control
and it has different columns and controls.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">A property is not valid for the specified
control</p>
</td>
<td rowspan="1"><p class="firstpara">Mistyping.</p>
<p>The control is a different type than expected.</p>
</td>
</tr>
</table>
<p>You can prevent the application from terminating by handling
the error in the DataWindow control's Error event or by
catching the error in a try-catch block.</p>
<a name="TI1179"></a><div class="brhd"><h4>Responding to errors in the Error event script</h4>
<p>The Error event's arguments give you several options
for responding to the error. You choose a course of action and set
the <span class="variable">action</span> argument to a value of the ExceptionAction
enumerated datatype.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">ExceptionAction enumerated datatype</span> <p class="notepara">If you give the <span class="variable">action</span> argument a value
other than ExceptionIgnore!, you will prevent error-handling code
in try-catch blocks from executing. For more information on values
for the ExceptionAction enumerated datatype, see the Error event
description in the <span class="doctitle">PowerScript Reference</span>.</p>
</div><p>If you are trying to find out a property value and you know
the expression might cause an error, you can include code that prepares
for the error by storing a default value in an instance variable.
Then the Error event script can return that value in place of the
failed expression.</p>
<p>There are three elements to this technique: the declaration
of an instance variable, the script that sets the variable's
default value and then accesses a DataWindow property, and the Error
event script. These elements are shown in Example 2 below. </p>
</div><a name="TI1180"></a><div class="brhd"><h4>Responding to errors in a try-catch block</h4>
<p>You can prevent the application from terminating by handling
the DataWindow runtime error (DWRuntimeError) in a try-catch block.
If you are trying to find out a property value and you know the
expression might cause an error, you can include code that automatically
assigns a valid default value that can be substituted for the failed
expression, as in Example 2 below.</p>
</div><a name="TI1181"></a><div class="brhd"><h4>Examples</h4>
<p><span class="emphasis">Example 1&#160;</span>This code displays
complete information about the error in a multilineedit mle_1.</p>
<p>The error event script:</p><pre class="userinput">mle_1.text = &amp;<br/>&#160;&#160;&#160;"error#: " + string(errornumber) + "~r~n" + &amp;<br/>&#160;&#160;&#160;"text: " + errortext + "~r~n" + &amp;<br/>&#160;&#160;&#160;"parent: " + errorwindowmenu + "~r~n" + &amp;<br/>&#160;&#160;&#160;"object: " + errorobject + "~r~n" + &amp;<br/>&#160;&#160;&#160;"line: " + string(errorline) + "~r~n"<br/>action = ExceptionIgnore!</pre>
<p>The try-catch block:</p><pre class="userinput">Try<br/>&#160;&#160;&#160;... //DataWindow property expression<br/>Catch (DWRuntimeError myExc)<br/>&#160;&#160;&#160;mle_1.text = &amp;<br/>&#160;&#160;&#160;"error#: " + string(myExc.number) + "~r~n" +&amp;<br/>&#160;&#160;&#160;"text: " + myExc.text + "~r~n" + &amp;<br/>&#160;&#160;&#160;"script: " + myExc.routinename + "~r~n" + &amp;<br/>&#160;&#160;&#160;"object: " + myExc.objectname + "~r~n" + &amp;<br/>&#160;&#160;&#160;"line: " + string(myExc.line) + "~r~n"<br/>End Try</pre>
<p>If the correct evaluation of the expression is not critical
to the application, the application continues without terminating.</p>
<p><span class="emphasis">Example 2&#160;</span>This example provides
a return value that will become the expression's value
if evaluation of the expression causes an error.</p>
<p>There are three elements to code in the error event script.
The instance variable is a string:</p><pre class="userinput">string is_dwvalue</pre>
<p>This script for a button or other control stores a valid return
value in an instance variable and then accesses a DataWindow property:</p><pre class="userinput">is_dwvalue = "5"<br/>ls_border = dw_1.Object.id.Border</pre>
<p>The Error event script uses the instance variable to provide
a valid return value:</p><pre class="userinput">action = ExceptionSubstituteReturnValue!<br/>returnvalue = is_dwvalue</pre>
<p>The try-catch block:</p><pre class="userinput">try<br/>&#160;&#160;&#160;ls_border = dw_1.Object.id.Border<br/>catch (DWRuntimeError myDWError)<br/>&#160;&#160;&#160;ls_border = "5"<br/>end try</pre>
<p>At runtime, if the id column does not exist or some other
error occurs, then the expression returns a valid border value&#8212;here
the string "5". If you are using the Error event instead of a try-catch
block, you must first store the value in an instance variable.</p>
</div>
</body>
</html>

