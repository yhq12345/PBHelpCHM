<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Setting up a printer </title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp322.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp322.htm"/></a></span><span class="nextbtn"><a href="apptechp324.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp324.htm"/></a></span></p><a name="apptech-jagcomp-prn"></a><h3>Setting up a printer </h3>
<p>To set up a printer to
print DataWindow objects, you must add access to the printer, set
up the <span class="filepath">dwprint.ini </span>configuration file, and
create an XPPATH environment variable.</p>
<a name="TI893"></a><div class="brhd"><h4>Adding access to the printer</h4>
<p>As the root user, add access to the printer on Solaris using
the Solaris <span class="cmdname">admintool</span> utility. For more information,
see the Solaris documentation.</p>
</div><a name="TI894"></a><div class="brhd"><h4>Setting up dwprint.ini</h4>
<p>The <span class="filepath">dwprint.ini</span> file in the <span class="filepath">$EAServer/bin</span> directory
is the configuration file for DataWindow printing. It closely follows
the Microsoft Windows approach to printer configuration. As a result,
it includes [windows], [devices],
and [ports] sections where you must provide appropriate
entries for your printers. </p>
<p>You usually do not need to modify other sections in this file.
However, some problems can be resolved by adding or changing other
sections. For example, you can try adding an entry like the following
to the [intl] section to change a date format:</p><pre class="userinput">[intl]<br/>sShortDate=m/d/yyyy //Set the year to 4 digit.</pre>
</div><a name="TI895"></a><div class="brhd"><h4>Specifying ports</h4>
<p>Each line in the [ports] section of <span class="filepath">dwprint.ini </span>contains
a user-defined port name and an associated command that is used
to spool the output file. For example, the command to send a print
job to a printer called <span class="dbobject">myprinter</span> connected
directly to your system is: </p><pre class="userinput">lp -s -d myprinter -t$XPDOCNAME</pre>
<p>$XPDOCNAME represents the name of the output file
sent to the printer. The <span class="cmdname">-s</span> option suppresses
the display of messages sent from <span class="cmdname">lp</span> in the <abbr title="e a server">EAServer</abbr> Server console.</p>
<p>The following is an example of the [ports] section
of the <span class="filepath">dwprint.ini</span> file with two ports defined
for remote printers called <span class="dbobject">prnt1</span> and <span class="dbobject">prnt2</span>,
one for a local printer, and an entry for printing to a file. The
name of the output file is enclosed in quotes. This enables file
names with multiple words to be used. The quotes must be escaped
for remote servers because <span class="cmdname">rsh</span> strips them out:</p><pre class="userinput">[ports]<br/>colorpr1=rsh prntsvr lp -s -d prnt1 -t\"$XPDOCNAME\"<br/>colorpr2=rsh prntsvr lp -s -d prnt2 -t\"$XPDOCNAME\"<br/>LOCAL=lp -d myprinter -t"$XPDOCNAME"<br/>FILE: =</pre>
</div><a name="TI896"></a><div class="brhd"><h4>Matching a printer type to a defined port</h4>
<p>The [devices] section contains a list of
all currently configured printers. Each line contains a user-defined
alias for the printer and three arguments: the printer model, the
printer mode (PCL4, PCL5, or PostScript), and one or more ports to
which the printer is connected. </p>
<p>The printer model is the name of the printer description file
(PPD) used by the printer. PPD files are installed in the<span class="filepath"> dwprinter/ppds</span> directory
in your PBVM installation. The text file <span class="filepath">filename_map.txt</span> in
that directory maps the name of the file that contains the printer
description to the type of printer. For example, these are the mappings
for the <span class="dbobject">color_lj</span> model used in the
rest of the examples: </p><pre class="userinput">color_lj.pcl:"HP Color LaserJet PCL Cartridge"<br/>color_lj.ps:"HP Color LaserJet PS"</pre><p class="afterblock">The printer model and mode are separated
by a space. The mode and port are separated by a comma. For example,
for the first device specified in the following [devices] section,
the alias is <span class="dbobject">HP Color LaserJet PS</span>, the model
is <span class="dbobject">color_lj</span>, the mode is <span class="dbobject">PostScript</span>,
and two ports are specified: <span class="dbobject">FILE:</span> and <span class="dbobject">colorpr1</span>. </p><pre class="userinput">[devices]<br/>HP Color LaserJet PS=color_lj PostScript,FILE:,colorpr1<br/>HP Color LaserJet PS=color_lj PCL5,colorpr2<br/>HP Color LaserJet PS=color_lj PostScript,LOCAL<br/>HP LaserJet PS=NULL PostScript,FILE:<br/>HP LaserJet PCL=NULL PCL,FILE:</pre>
</div><a name="TI897"></a><div class="brhd"><h4>Specifying a default printer</h4>
<p>The [windows] section contains default printer
information. Like the ports specification, each device line has
three arguments: the name of the PPD file, the driver, and the port,
but in the [windows] section they are all separated
by commas.</p>
<p>The following example shows a default entry for printing to
a file (when the printer file description is set to <span class="cmdname">NULL</span>)
as well as two other entries. The semicolon at the beginning of
two of the lines is a comment character, so the current default
printer is the HP Color LaserJet printer on the port <span class="dbobject">colorpr1</span>.</p><pre class="userinput">[windows]<br/>device=color_lj,PostScript,colorpr1<br/>;device=color_lj,PostScript,colorpr2<br/>;device=NULL,PostScript,FILE:</pre>
</div><a name="TI898"></a><div class="brhd"><h4>Setting printer options</h4>
<p>The <span class="filepath">dwprint.ini</span> file must contain a configuration
section for each model you have defined in the [windows], [devices],
and [ports] sections. The configuration section
provides default setup information for the printer, including the
number of copies, orientation, page size, and DPI. </p>
<p>For example, for the <span class="dbobject">color_lj</span> printer
used in the preceding examples, add configuration sections like
this:</p><pre class="userinput">[color_lj,PostScript]<br/>Filename=jaguar.ps<br/>Scale=1.00<br/>Copies=1<br/>Orientation=Portrait<br/>PageSize=Letter<br/>DPI=300<br/> <br/>[color_lj,PCL5]<br/>Filename=jaguar.pcl<br/>Scale=1.00<br/>Copies=1<br/>Orientation=Portrait<br/>PageSize=Letter<br/>DPI=300</pre>
</div><a name="TI899"></a><div class="brhd"><h4>Setting the XPPATH environment variable</h4>
<p>Before you start a print job, set the XPPATH environment variable.
The XPPATH variable must contain the path to a directory that includes
printer description files and printer-specific font mapping files.
This information is installed in the dwprinter directory in your
PBVM installation.</p>
<p>For a C shell, set the path as follows: </p><pre class="userinput">setenv XPPATH $EAServer/dwprinter</pre>
<p>For a Korn shell or a Bourne shell, set the path as follows:</p><pre class="userinput">XPPATH = $EAServer/dwprinter;export XPPATH</pre>
</div>
</body>
</html>

