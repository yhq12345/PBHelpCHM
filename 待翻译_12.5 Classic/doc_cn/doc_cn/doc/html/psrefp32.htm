<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Access for instance variables</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="psrefp31.htm" title="Previous"><img src="../image/arrow-left.png" alt="psrefp31.htm"/></a></span><span class="nextbtn"><a href="psrefp33.htm" title="Next"><img src="../image/arrow-right.png" alt="psrefp33.htm"/></a></span></p><a name="ccjcjicb"></a><h3>Access for instance variables</h3>
<a name="TI183"></a><div class="brhd"><h4>Description</h4>
<p>The general syntax for declaring PowerScript variables (see <a href="psrefp28.htm#CCJBJJAA">"Syntax of a variable declaration"</a>) showed that
you can specify access keywords in a declaration for an instance
variable. This section describes those keywords.</p>
</div><p>When you specify an access right for a variable, you are controlling
the visibility of the variable or its visibility access. Access
determines which scripts recognize the variable's name.</p>
<p>For a specified access right, you can control operational
access with modifier keywords. The modifiers specify which scripts
can read the variable's value and which scripts can change
it.</p>
<a name="TI184"></a><div class="brhd"><h4>Syntax</h4>
<pre class="syntax">{ <span class="variable">access-right </span>} { <span class="variable">readaccess </span>} {<span class="variable"> writeaccess </span>}<span class="variable"> datatype variablename</span></pre>
<p>The following table describes the parameters you can use to
specify access rights for instance variables.</p>
</div><a name="bcgbgbib"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 3-4: Instance variable declaration
parameters for access rights</caption>
<tr><th rowspan="1"><p class="firstpara">Parameter</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">access-right</span> <br/>(optional)</p>
</td>
<td rowspan="1"><p class="firstpara">A keyword specifying where the variable's
name will be recognized. Values are:</p><ul><li><p class="firstbullet"><span class="cmdname">PUBLIC</span> &#8211; (Default) Any script
in the application can refer to the variable. In another object's
script, you use dot notation to qualify the variable name and identify
the object it belongs to.</p>
  </li>
<li><p class="ds"><span class="cmdname">PROTECTED</span> &#8211; Scripts
for the object for which the variable is declared and its descendants
can refer to the variable.</p>  </li>
<li><p class="ds"><span class="cmdname">PRIVATE</span> &#8211; Scripts for
the object for which the variable is declared can refer to the variable.
You cannot refer to the variable in descendants of the object.</p>  </li>
</ul>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">readaccess</span> <br/>(optional)</p>
</td>
<td rowspan="1"><p class="firstpara">A keyword restricting the ability of
scripts to read the variable's value. Values are:</p><ul><li><p class="firstbullet"><span class="cmdname">PROTECTEDREAD</span> &#8211; Only
scripts for the object and its descendants can read the variable.</p>
  </li>
<li><p class="ds"><span class="cmdname">PRIVATEREAD</span> &#8211; Only scripts
for the object can read the variable.</p>  </li>
</ul>
<p>When <span class="variable">access-right</span> is <span class="cmdname">PUBLIC</span>,
you can specify either keyword. When <span class="variable">access-right</span> is <span class="cmdname">PROTECTED</span>,
you can specify only <span class="cmdname">PRIVATEREAD</span>. You cannot
specify a modifier for <span class="cmdname">PRIVATE</span> access, because <span class="cmdname">PRIVATE</span> is
already fully restricted. </p>
<p>If <span class="variable">readaccess</span> is omitted, any script
can read the variable. </p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">writeaccess</span> <br/>(optional)</p>
</td>
<td rowspan="1"><p class="firstpara">A keyword restricting the ability of
scripts to change the variable's value. Values are:</p><ul><li><p class="firstbullet"><span class="cmdname">PROTECTEDWRITE</span> &#8211; Only
scripts for the object and its descendants can change the variable.</p>
  </li>
<li><p class="ds"><span class="cmdname">PRIVATEWRITE</span> &#8211; Only
scripts for the object can change the variable.</p>  </li>
</ul>
<p>When <span class="variable">access-right</span> is <span class="cmdname">PUBLIC</span>,
you can specify either keyword. When <span class="variable">access-right</span> is <span class="cmdname">PROTECTED</span>,
you can specify only <span class="cmdname">PRIVATEWRITE</span>. You cannot
specify a modifier for <span class="cmdname">PRIVATE</span> access, because <span class="cmdname">PRIVATE</span> is
already fully restricted.</p>
<p>If <span class="variable">writeaccess</span> is omitted, any script
can change the variable.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">datatype</span></p>
</td>
<td rowspan="1"><p class="firstpara">A valid datatype. See <a href="psrefp28.htm#CCJBJJAA">"Syntax of a variable declaration"</a>.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">variablename</span></p>
</td>
<td rowspan="1"><p class="firstpara">A valid identifier. See <a href="psrefp28.htm#CCJBJJAA">"Syntax of a variable declaration"</a>.</p>
</td>
</tr>
</table>
<a name="ccjdaaca"></a><div class="brhd"><h4>Usage</h4>
<p>Access modifiers give you more control over which objects
have access to a particular object's variables. A typical
use is to declare a public variable but only allow the owner object
to modify it:</p><pre class="userinput">public protectedwrite integer ii_count</pre>
</div><p>You can also group declarations that have the same access
by specifying the access-right keyword as a label (see <a href="psrefp33.htm#CCJDDEBF">"Another format for access-right
keywords"</a>).</p>
<p>When you look at exported object syntax, you might see the
access modifiers <span class="cmdname">SYSTEMREAD</span> and <span class="cmdname">SYSTEMWRITE</span>.
Only PowerBuilder can access variables with these modifiers. You
cannot refer to variables with these modifiers in your scripts and
functions and you cannot use these modifiers in your own definitions.</p>
<a name="TI185"></a><div class="brhd"><h4>Examples</h4>
<p>To declare these variables, select Declare&gt;Instance
Variables in the appropriate painter.</p>
</div><p>These declarations use access keywords to control the scripts
that have access to the variables:</p><pre class="userinput">private integer ii_a, ii_n</pre><pre class="userinput">public  integer ii_Subtotal</pre><pre class="userinput">protected integer ii_WinCount</pre>
<p>This protected variable can only be changed by scripts of
the owner object; descendants of the owner can read it:</p><pre class="userinput">protected privatewrite string is_label</pre>
<p>These declarations have public access (the default) but can
only be changed by scripts in the object itself:</p><pre class="userinput">privatewrite real ir_accum, ir_current_data</pre>
<p>This declaration defines an integer that only the owner objects
can write or read but whose name is reserved at the public level:</p><pre class="userinput">public privateread privatewrite integer ii_reserved</pre>
<a name="TI186"></a><div class="formalpara"><p class="title">Private variable not recognized outside its object</p><p class="firstpara">Suppose you have defined a window w_emp with a private
integer variable <span class="variable">ii_int</span>: </p>
</div><pre class="userinput">private integer ii_int</pre>
<p>In a script you declare an instance of the window called <span class="dbobject">w_myemp</span>.
If you refer to the private variable <span class="variable">ii_int</span>,
you get a compiler warning that the variable is not defined (because
the variable is private and is not recognized in scripts outside the
window itself):</p><pre class="userinput">w_emp w_myemp</pre><pre class="userinput">w_myemp.ii_int = 1 // Variable not defined</pre>
<a name="TI187"></a><div class="formalpara"><p class="title">Public variable with restricted access</p><p class="firstpara">Suppose you have defined a window <span class="dbobject">w_emp</span> with
a public integer variable <span class="variable">ii_int</span> with
write access restricted to private: </p>
</div><pre class="userinput">public privatewrite integer ii_int</pre>
<p>If you write the same script as above, the compiler warning
will say that you cannot write to the variable (the name is recognized
because it is public, but write access is not allowed):</p><pre class="userinput">w_emp w_myemp</pre><pre class="userinput">w_myemp.ii_int = 1 // Cannot write to variable </pre>

</body>
</html>

