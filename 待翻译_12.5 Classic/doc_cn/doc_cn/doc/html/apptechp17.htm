﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="../../styles/sybase.css" type="text/css" />
    <title>Other techniques</title>
</head>
<body id="bodyID">
    <p class="navbtns"><span class="prevbtn"><a href="apptechp16.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp16.htm" /></a></span><span class="nextbtn"><a href="apptechp18.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp18.htm" /></a></span></p><a name="x-ref355062155"></a><h1>Other techniques</h1>
    <p>
        PowerBuilder allows you to implement a wide variety of object-oriented techniques.
        This section discusses selected techniques and relates them to PowerBuilder.
    </p>
    <a name="TI54"></a><div class="brhd">
        <h4>Using function overloading</h4>
        <p>
            In function overloading, the descendent function (or an identically
            named function in the same object) has different arguments or argument
            datatypes. PowerBuilder determines which version of a function to
            execute, based on the arguments and argument datatypes specified
            in the function call:
        </p>
        <a name="TI55"></a><div class="fig">
            <p class="title">Figure 2-6: Function overloading</p>
            <div class="fig"><img src="../image/ltoop160.gif" alt="ltoop160.gif" /></div>
        </div><div class="note">
            <img src="../image/note.png" class="inline" alt="Note" /> <span class="title">Global functions</span> <p class="notepara">Global functions cannot be overloaded.</p>
        </div>
    </div><a name="ccjdeaeg"></a><div class="brhd">
        <h4>Dynamic versus static lookup</h4>
        <a name="TI56"></a><div class="formalpara">
            <p class="title">Dynamic lookup</p><p class="firstpara">
                In certain situations, such as when insulating your application
                from cross-platform dependencies, you create separate descendent objects,
                each intended for a particular situation. Your application calls
                the platform-dependent functions dynamically:
            </p>
        </div><a name="TI57"></a><div class="fig">
            <p class="title">Figure 2-7: Dynamic lookup</p>
            <div class="fig"><img src="../image/ltoop200.gif" alt="ltoop200.gif" /></div>
        </div><p>
            Instantiate the appropriate object at runtime, as shown in
            the following code example:
        </p><pre class="userinput">// This code works with both dynamic and<br />// static lookup.<br />// Assume these instance variables<br />u_platform iuo_platform<br />Environment ienv_env<br />...<br />GetEnvironment(ienv_env)<br />choose case ienv_env.ostype<br />&#160;&#160;&#160;case windows!<br />&#160;&#160;&#160;&#160;&#160;&#160;iuo_platform = CREATE u_platform_win<br />&#160;&#160;&#160;case windowsnt!<br />&#160;&#160;&#160;&#160;&#160;&#160;iuo_platform = CREATE u_platform_win<br />&#160;&#160;&#160;case else<br />&#160;&#160;&#160;&#160;&#160;&#160;iuo_platform = CREATE u_platform_unix<br />end choose</pre>
        <p>
            Although dynamic lookup provides flexibility, it also slows
            performance.
        </p>
        <a name="TI58"></a><div class="formalpara">
            <p class="title">Static lookup</p><p class="firstpara">
                To ensure fast performance, static lookup is a better option. However,
                PowerBuilder enables object access using the reference variable's datatype
                (not the datatype specified in a <span class="cmdname">CREATE</span> statement).
            </p>
        </div><a name="TI59"></a><div class="fig">
            <p class="title">Figure 2-8: Static lookup</p>
            <div class="fig"><img src="../image/ltoop180.gif" alt="ltoop180.gif" /></div>
        </div><p>
            When using static lookup, you must define default implementations
            for functions in the ancestor. These ancestor functions return an
            error value (for example, -1) and are overridden in at least one
            of the descendent objects.
        </p>
        <a name="TI60"></a><div class="fig">
            <p class="title">Figure 2-9: Ancestor functions overridden in descendent functions</p>
            <div class="fig"><img src="../image/ltoop140.gif" alt="ltoop140.gif" /></div>
        </div><p>
            By defining default implementations for functions in the ancestor
            object, you get platform independence as well as the performance
            benefit of static lookup.
        </p>
    </div><a name="TI61"></a><div class="brhd">
        <h4>Using delegation</h4>
        <p>
            Delegation occurs when objects offload processing to other
            objects.
        </p>
        <a name="TI62"></a><div class="formalpara">
            <p class="title">Aggregate relationship</p><p class="firstpara">
                In an aggregate relationship (sometimes called a <span class="emphasis">
                    whole-part
                    relationship
                </span>), an object (called an owner object) associates
                itself with a service object designed specifically for that object
                type.
            </p>
        </div><p>
            For example, you might create a service object that handles
            extended row selection in DataWindow objects. In this case, your
            DataWindow objects contain code in the Clicked event to call the
            row selection object.
        </p>
        <div class="procedure">
            <p class="title"><img src="../image/proc.png" alt="Steps" /> To use objects in an aggregate relationship:</p>
            <ol type="1">
                <li class="fi">
                    <p>
                        Create a service object (<span class="dbobject">u_sort_dw</span> in
                        this example).
                    </p>
                </li>
                <li class="ds">
                    <p>
                        Create an instance variable (also called a reference
                        variable) in the owner (a DataWindow control in this example):
                    </p><pre class="userinput">u_sort_dw iuo_sort</pre>
                </li>
                <li class="ds">
                    <p>
                        Add code in the owner object to create the service
                        object:
                    </p><pre class="userinput">iuo_sort = CREATE u_sort_dw</pre>
                </li>
                <li class="ds">
                    <p>
                        Add code to the owner's system events
                        or user events to call service object events or functions. This
                        example contains the code you might place in a <span class="dbobject">ue_sort</span> user
                        event in the DataWindow control:
                    </p><pre class="userinput">IF IsValid(iuo_sort) THEN<br />&#160;&#160;&#160;Return iuo_sort.uf_sort()<br />ELSE<br />&#160;&#160;&#160;Return -1<br />END IF</pre>
                </li>
                <li class="ds">
                    <p>
                        Add code to call the owner object's user
                        events. For example, you might create a CommandButton or Edit&gt;Sort
                        menu item that calls the <span class="dbobject">ue_sort</span> user
                        event on the DataWindow control.
                    </p>
                </li>
                <li class="ds">
                    <p>
                        Add code to the owner object's Destructor
                        event to destroy the service object:
                    </p><pre class="userinput">IF IsValid(iuo_sort) THEN<br />&#160;&#160;&#160;DESTROY iuo_sort<br />END IF</pre>
                </li>
            </ol>
        </div><a name="TI63"></a><div class="formalpara">
            <p class="title">Associative relationship</p><p class="firstpara">
                In an associative relationship, an object associates itself
                with a service to perform a specific type of processing.
            </p>
        </div><p>
            For example, you might create a string-handling service that
            can be enabled by any of your application's objects.
        </p>
        <p>
            The steps you use to implement objects in an associative relationship
            are the same as for aggregate relationships.
        </p>
    </div><a name="babjhhie"></a><div class="brhd">
        <h4>Using user objects as structures</h4>
        <p>
            When you enable a user object's AutoInstantiate property,
            PowerBuilder instantiates the user object along with the object,
            event, or function in which it is declared. You can also declare
            instance variables for a user object. By combining these two capabilities,
            you create user objects that function as structures. The advantages
            of creating this type of user object are that you can:
        </p><ul>
            <li>
                <p class="firstbullet">
                    Create descendent objects
                    and extend them.
                </p>
            </li>
            <li>
                <p class="ds">
                    Create functions to access the structure all at
                    once.
                </p>
            </li>
            <li>
                <p class="ds">
                    Use access modifiers to limit access to certain
                    instance variables.
                </p>
            </li>
        </ul>
        <div class="procedure">
            <p class="title"><img src="../image/proc.png" alt="Steps" /> To create a user object to be used as a structure:</p>
            <ol type="1">
                <li class="fi">
                    <p>
                        Create the user object, defining instance
                        variables only.
                    </p>
                </li>
                <li class="ds">
                    <p>
                        Enable the user object's AutoInstantiate
                        property by checking AutoInstantiate on the General property page.
                    </p>
                </li>
                <li class="ds">
                    <p>
                        Declare the user object as a variable in objects,
                        functions, or events as appropriate.
                    </p><p>
                        PowerBuilder creates the user object when the object, event,
                        or function is created and destroys it when the object is destroyed
                        or the event or function ends.
                    </p>
                </li>
            </ol>
        </div>
    </div><a name="TI64"></a><div class="brhd">
        <h4>Subclassing DataStores</h4>
        <p>
            Many applications use a DataWindow visual user object instead
            of the standard DataWindow window control. This allows you to standardize
            error checking and other, application-specific DataWindow behavior.
            The <span class="dbobject">u_dwstandard</span> DataWindow visual
            user object found in the tutorial library <span class="filepath">TUTOR_PB.PBL</span> provides
            an example of such an object.
        </p>
        <p>
            Since DataStores function as nonvisual DataWindow controls,
            many of the same application and consistency requirements apply
            to DataStores as to DataWindow controls. Consider creating a DataStore
            standard class user object to implement error checking and application-specific
            behavior for DataStores.
        </p>
    </div>
</body>
</html>

