<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Defining the component interface</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp311.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp311.htm"/></a></span><span class="nextbtn"><a href="apptechp313.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp313.htm"/></a></span></p><a name="ccjeabhc"></a><h1>Defining the component interface</h1>
<a name="TI856"></a><div class="brhd"><h4>How the interface is specified</h4>
<p><abbr title="e a server">EAServer</abbr> stores all component
interfaces in CORBA Interface Definition Language (IDL) modules.
IDL is defined by the Object Management Group as a standard language
for defining component interfaces. When you deploy a PowerBuilder
custom class user object as an <abbr title="e a server">EAServer</abbr> component,
the methods (functions and events) and instance variables defined
for the object are added to the component interface. You do not
need to write IDL for the interface, because the <abbr title="e a server">EAServer</abbr> component generator writes
the IDL for you. </p>
<p>In <abbr title="e a server">EAServer</abbr> 6.0 and later,
PowerBuilder components are wrapped as EJBs. For more information,
see the <span class="doctitle">CORBA Components Guide</span> in the <abbr title="e a server">EAServer</abbr> documentation set on the
Sybase Product Manuals Web site.</p>
</div><a name="TI857"></a><div class="brhd"><h4>What gets included in the interface</h4>
<p>The <abbr title="e a server">EAServer</abbr> component generator
includes all public functions declared for the user object in the
component interface. Depending on the build options you specify
for the component, the generator may also include accessor methods for
the public instance variables and also expose user events as methods. </p>
</div><a name="TI858"></a><div class="brhd"><h4>Method names and method overloading</h4>
<p>Although IDL does not provide support for method overloading,
you can nonetheless deploy PowerBuilder custom class user objects
to <abbr title="e a server">EAServer</abbr> that have overloaded
methods. To work around the IDL restriction, the component generator
appends two underscores (__) and a unique suffix
to the method name that will be overloaded. If you look at the IDL
generated for a PowerBuilder object, you therefore see suffixes
appended to methods that were overloaded in PowerBuilder. </p>
<p>When you generate stubs or proxy objects for components that
have overloaded methods, <abbr title="e a server">EAServer</abbr> strips
off the IDL suffix so that the client can access the method by using
its correct name.</p>
<p>For more information about IDL, see the <abbr title="e a server">EAServer</abbr> documentation.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Do not use two consecutive underscores in your method
names</span> <p class="notepara">Because <abbr title="e a server">EAServer</abbr> treats
two underscores (__) as a reserved delimiter,
you should not use two consecutive underscores in a function name
in a custom class user object that you plan to deploy as an <abbr title="e a server">EAServer</abbr> component.</p>
</div></div><a name="TI859"></a><div class="brhd"><h4>Datatypes</h4>
<p>You can use the following datatypes in the interface of a
user object that you deploy as an <abbr title="e a server">EAServer</abbr> component:</p><ul><li><p class="firstbullet">Standard datatypes (except
for the <span class="dt">Any</span> datatype)</p>
  </li>
<li><p class="ds">Structures</p>  </li>
<li><p class="ds">Custom class (nonvisual) user objects that have
been deployed as <abbr title="e a server">EAServer</abbr> components</p>  </li>
</ul>
<p>These datatypes can be used for public instance variables
as well as for the arguments and return values of public methods.
Private and protected instance variables and methods can use all
datatypes supported by PowerBuilder.</p>
<p>The <span class="dt">Any</span> datatype is not supported in
the public interface of a component. In addition, with the exception
of the ResultSet and ResultSets objects, the component interface
cannot include built-in PowerBuilder system objects (for example,
the Transaction or DataStore object). The component interface also cannot
include visual objects (such as windows or menus). </p>
<p>Component methods can pass arrays of standard datatypes and
arrays of structures, and they can use custom class user objects
to pass arrays.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">int and uint map to short</span> <p class="notepara">In <abbr title="e a server">EAServer</abbr> 6.x, both the <span class="cmdname">int</span> and <span class="cmdname">uint</span> PowerBuilder
datatypes map to <span class="cmdname">short</span>, so if you have defined
a function that returns <span class="cmdname">int</span> and has an <span class="cmdname">int</span> argument, deployment
will fail if you define a function with the same name on the same component
that returns <span class="cmdname">uint</span> and has a <span class="cmdname">uint</span> argument.</p>
</div><p>For a list of datatypes used in <abbr title="e a server">EAServer</abbr>,
their CORBA IDL equivalents, and the PowerBuilder datatypes to which
they map, see the <span class="doctitle">PowerScript Reference</span> or
the online Help. For a list of PowerBuilder to EJB datatype mappings,
see the <span class="doctitle">CORBA Components Guide</span> in the <abbr title="e a server">EAServer</abbr> documentation set on the Sybase
Product Manuals Web site.</p>
</div><a name="TI860"></a><div class="brhd"><h4>Passing by reference</h4>
<p>You can pass arguments to component methods by reference.
However, the behavior is somewhat different in a distributed application
than in a nondistributed application.</p>
<p>When you pass by reference, the variable is actually copied
to the server before the method is executed and then copied back
when the method completes execution. This behavior is usually transparent
to the application, but in some situations it can affect the outcome
of processing.</p>
<p>For example, suppose you define a method called <span class="cmdname">increment_values</span> that
takes two arguments called <span class="cmdname">x</span> and <span class="cmdname">y</span>,
both of which are passed by reference. The script for the method
increments <span class="cmdname">x</span> and <span class="cmdname">y</span> as shown
below:</p><pre class="userinput">x = x + 1<br/>y = y + 1</pre>
<p>The client uses the following code to call the method:</p><pre class="userinput">int z<br/>z = 1<br/>increment_values(z,z)</pre>
<p>In a nondistributed application, the value of <span class="cmdname">z</span> after
the method completed execution would be 3 (because the local invocation
passes a <span class="emphasis">pointer</span> to <span class="cmdname">z</span>, and <span class="cmdname">z</span> is
incremented twice). In a distributed application, the value of <span class="cmdname">z</span> would
be 2 (because the remote invocation passes two <span class="emphasis">copies</span> of <span class="cmdname">z</span>,
which are incremented separately).</p>
</div><a name="TI861"></a><div class="brhd"><h4>Passing a read-only value</h4>
<p>When
you pass a read-only value, the behavior is similar to passing by
value, except that the data cannot be modified. A copy of the data
is passed across the wire to the server.</p>
</div><a name="TI862"></a><div class="brhd"><h4>Passing objects</h4>
<p>Objects created within <abbr title="e a server">EAServer</abbr> components
can be passed back to clients, but these objects must be installed <abbr title="e a server">EAServer</abbr> components. If you try
to pass back a PowerBuilder object that is not an <abbr title="e a server">EAServer</abbr> component, you will get
a runtime error. To use a component that was passed back from the
server, the client must have the corresponding <abbr title="e a server">EAServer</abbr> proxy (for a PowerBuilder client)
or stub (for a non-PowerBuilder client).</p>
<p>A client application <span class="emphasis">cannot</span> pass a PowerBuilder
object reference to <abbr title="e a server">EAServer</abbr>. Therefore,
you cannot use a PowerBuilder object reference to push messages from
the server back to a PowerBuilder client. However, you can simulate
this behavior by using a shared object on the client to communicate
with <abbr title="e a server">EAServer</abbr>. </p>
<p>To simulate server push, the client uses the <span class="cmdname">SharedObjectRegister</span> and <span class="cmdname">SharedObjectGet</span> functions
to create a shared object. Once the object has been created, the
client can post a method to the shared object, passing it a callback object
that should be notified when processing has finished on the server.
The method on the shared object makes a synchronous call to the <abbr title="e a server">EAServer</abbr> component method that performs
processing. Since the shared object is running in a separate thread
on the client, the client application can proceed with other work
while the process is running on the server. </p>
</div><a name="TI863"></a><div class="brhd"><h4>Providing support for NULL values</h4>
<p>PowerBuilder allows you to specify whether the methods of
an <abbr title="e a server">EAServer</abbr> component can accept <span class="cmdname">NULL</span> values
as function arguments or return types. To provide support for <span class="cmdname">NULL</span> values
in the component interface, check the Support <span class="cmdname">NULL</span> Values
check box in the property sheet for the project used to generate the <abbr title="e a server">EAServer</abbr> component. If this box
is not checked, clients cannot pass <span class="cmdname">NULL</span> values
in any argument and the server cannot set any argument to <span class="cmdname">NULL</span> or return
a <span class="cmdname">NULL</span> value.</p>
<p>If you allow null values in the prototype for a component
method, PowerBuilder appends a "_N" suffix
to the method name in the <abbr title="e a server">EAServer</abbr> proxy
that you generate from the Project painter. To call this method,
you must create an instance of the proxy, rather than an instance
of the NVO, and you must reference the method with the "_N" suffix. For
example, if <span class="cmdname">of_gen</span> is the name of a
method in the NVO, and you create an <abbr title="e a server">EAServer</abbr> proxy
that allows null return values, you must instantiate the proxy and
call <span class="cmdname">of_gen_N</span> to use this method.</p>
</div><a name="TI864"></a><div class="brhd"><h4>EAServer validation</h4>
<p>If you are designing a custom class user object that you plan
to deploy as an <abbr title="e a server">EAServer</abbr> component,
you can have PowerBuilder warn you when you use code elements that
are not valid in <abbr title="e a server">EAServer</abbr>. <abbr title="e a server">EAServer</abbr> validation checks public
instance variables and public functions for system types, visual
types, structures, and any variables. </p>
<p><abbr title="e a server">EAServer</abbr> validation is on
by default if you created the user object using an <abbr title="e a server">EAServer</abbr> wizard. To check, select
the Design menu in the User Object painter and make sure <abbr title="e a server">EAServer</abbr> Validation is checked.
When you save the object, the Output window lists warnings such
as the following:</p><pre class="computerout">---------- Compiler: Information messages<br/>Information C0197: Component Validation<br/>Warning     C0198: illegal Jaguar type: 'window' return type for function: 'of_badfunc'<br/>Warning     C0198: illegal Jaguar type: 'any' return type for function: 'of_badfunc'</pre>
<p>Validation is associated with the object you are editing,
not with the User Object painter. When you reopen an object, it
has the same validation state as when you closed it. </p>
</div><a name="TI865"></a><div class="brhd"><h4>Throwing exceptions</h4>
<p>When you declare an exception on a function of a user object
deployed to EAServer, the exceptions are translated to CORBA IDL
as part of the method prototype. The exceptions can be handled by
any type of <abbr title="e a server">EAServer</abbr> client application
or calling component. For more information, see <a href="apptechp27.htm#BABDCHCF">"Exception handling in PowerBuilder"</a>.</p>
</div>
</body>
</html>

