<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Migrating targets</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp165.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp165.htm"/></a></span><span class="nextbtn"><a href="pbugp167.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp167.htm"/></a></span></p><a name="bgbjhcbc"></a><h1>Migrating targets</h1>
<p>When you upgrade to a new version of PowerBuilder, your existing
targets need to be migrated to the new version. Typically, when
you open a workspace that contains targets that need to be migrated,
or add a target that needs to be migrated to your workspace, PowerBuilder
prompts you to migrate the targets. However, there are some situations
when you need to migrate a target manually. For example, if you
add a library that has not been migrated to a target's
library list, you will not be able to open objects in that library
until the target has been migrated.</p>
<p>You cannot migrate a target that is not in your current workspace
and you must set the root of the System Tree or the view in the
Library painter to the current workspace.</p>
<a name="TI204"></a><div class="brhd"><h4>Before you migrate</h4>
<p>There are some steps you should take before you migrate a
target:</p><ul><li><p class="firstbullet">Use the Migration Assistant to check
for obsolete syntax or the use of reserved words in your code</p>
  </li>
<li><p class="ds">Check the release notes for migration issues</p>  </li>
<li><p class="ds">Make backup copies of the target and libraries</p>  </li>
<li><p class="ds">Make sure that the libraries you will migrate are
writable</p>  </li>
</ul>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Always back up your PBLs before migrating</span> <p class="notepara">Make sure you make a copy of your PBLs before migrating. After
migration, you cannot open them in an earlier version of PowerBuilder.</p>
</div><p>The Migration Assistant is available on the Tool page of the
New dialog box. For help using the Migration Assistant, click the
Help (?) button in the upper&#8211;right corner of the window
and click the field you need help with, or click the field and press
F1. If the Migration Assistant finds obsolete code, you can fix
it in an earlier version of PowerBuilder to avoid errors when you migrate
to the current version.</p>
</div><a name="chdijidi"></a><div class="brhd"><h4>PowerBuilder libraries and
migration</h4>
<p>PowerBuilder libraries (PBLs) contain a header, source code
for the objects in the PBL, and binary code. There are two differences
between PowerBuilder 10 and later PBLs and PBLs developed in earlier
versions of PowerBuilder:</p><ul><li><p class="firstbullet">The source code in
PowerBuilder 10 and later PBLs is encoded in Unicode (UTF&#8211;16LE,
where LE stands for little endian) instead of DBCS (versions 7,
8, and 9) or ANSI (version 6 and earlier).</p>
  </li>
<li><p class="ds">The format of the header lets PowerBuilder determine
whether it uses Unicode encoding. The header format for PowerBuilder
10 is the same as that used for PUL files in PowerBuilder 6.5 and
for PKL files in PocketBuilder. These files do not need to be converted
to Unicode when they are migrated to PowerBuilder 10 or later.</p>  </li>
</ul>
</div><a name="TI205"></a><div class="brhd"><h4>When PBLs are migrated</h4>
<p>Before opening a PBL, PowerBuilder checks its header to determine
whether or not it uses Unicode encoding. PBLs are not converted
to Unicode unless you specifically request that they be migrated. </p>
<p>You cannot expand the icon for a PBL from PowerBuilder 9 or
earlier in the Library painter. To examine its contents, you must
migrate it to PowerBuilder 10 or later.</p>
<p>When you attempt to open a workspace that contains targets
from a previous release in PowerBuilder, the Targets to be Migrated
dialog box displays. You can migrate targets from this dialog box,
or clear the No Prompting check box to open the Migrate Application
dialog box. </p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">PowerBuilder dynamic libraries</span> <p class="notepara">If you plan to reference a PowerBuilder dynamic library (PBD)
that was encoded in ANSI formatting (for example, if it was created
in PowerBuilder 9 or earlier), you must regenerate the PBD to use
Unicode formatting. Dynamic libraries that you create in PowerBuilder
10 or later use Unicode formatting exclusively.</p>
<p class="notepara">For information on creating PBDs, see <a href="pbugp168.htm#X-REF345205729">"Creating runtime libraries"</a>.</p>
</div></div><a name="chdhcgig"></a><div class="brhd"><h4>The Migrate Application
dialog box</h4>
<p>The Migrate Application dialog box lists each PBL that will
be migrated and lets you choose the type of messages that display
during the migration process.</p>
<div class="fig"><img src="../image/lib19.gif" alt="lib19.gif"/></div>
<p>If you click OK, each PBL is first migrated to the new version
of PowerBuilder. If necessary, PowerBuilder converts source code
from DBCS to Unicode. PowerBuilder performs a full build and saves
the source code back to the same PBL files. Changes to scripts display
in informational messages in the Output window and are written to
a log file for each PBL so that you can examine the changes later.
Recommended changes are also written to the log file.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Migration from DBCS versions</span> <p class="notepara">The migration process automatically converts multibyte strings
in DBCS applications to unicode strings. You do not need to select
the Automatically Convert DBCS String Manipulation Functions check
box for this conversion. If the migration encounters an invalid
multibyte string, it sets the invalid string to a question mark
and reports the error status. You can modify question marks in the
Unicode output string after the migration.</p>
</div><p>The following two lines from a log file indicate that the <span class="cmdname">FromAnsi</span> function
is obsolete and was replaced with the <span class="cmdname">String</span> function,
and that an encoding parameter should be added to an existing instance
of the <span class="cmdname">String</span> function:</p><pre class="computerout">2006/01/27 08:20:11    test.pbl(w_main).cb_1.clicked.4: Information C0205: Function 'FromAnsi' is replaced with function 'String'.<br/></pre><pre class="computerout">2006/01/27 08:20:11    test.pbl(w_main).cb_2.clicked.4: Information C0206: Append extra argument 'EncodingAnsi!' to function 'String' for backward compatibility.</pre>
<p>The log file has the same name as the PBL with the string <span class="filepath">_mig</span> appended
and the extension <span class="filepath">.log</span> and is created in the
same directory as the PBL. If no changes are made, PowerBuilder
creates an empty log file. If the PBL is migrated more than once,
output is appended to the existing file.</p>
<p>PowerBuilder makes the following changes:</p><ul><li><p class="firstbullet">The <span class="cmdname">FromUnicode</span> function
is replaced with the <span class="cmdname">String</span> function and the second
argument EncodingUTF16LE! is added</p>
  </li>
<li><p class="ds">The <span class="cmdname">ToUnicode</span> function is replaced
with the <span class="cmdname">Blob</span> function and the second argument
EncodingUTF16LE! is added</p>  </li>
<li><p class="ds">The <span class="cmdname">FromAnsi</span> function is replaced
with the <span class="cmdname">String</span> function and the second argument
EncodingAnsi! is added</p>  </li>
<li><p class="ds">The <span class="cmdname">ToAnsi</span> function is replaced
with the <span class="cmdname">Blob</span> function and the second argument
EncodingAnsi! is added</p>  </li>
<li><p class="ds">An Alias For clause with the following format is
appended to declarations of external functions that take strings,
chars, or structures as arguments or return any of these datatypes: </p><pre class="syntax">ALIAS FOR "<span class="variable">functionname</span>;ansi"</pre><p class="lipara">If the declaration already has an Alias For clause, only the
string ;ansi is appended.</p>  </li>
</ul>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">DBCS users only</span> <p class="notepara">If you select the Automatically Convert DBCS String Manipulation
Functions check box, PowerBuilder automatically makes appropriate
conversions to scripts in PowerBuilder 9 applications. For example,
if you used the <span class="cmdname">LenW</span> function, it is converted
to <span class="cmdname">Len</span>, and if you used the <span class="cmdname">Len</span> function,
it is converted to <span class="cmdname">LenA</span>. The changes are written
to the Output window and the log file. This box should be selected
only in DBCS environments.</p>
</div></div><a name="TI206"></a><div class="brhd"><h4>Adding PBLs to a PowerBuilder target</h4>
<p>When you add PBLs from a previous release to a PowerBuilder
target's library list, the PBLs display in the System Tree.
The PBLs are not migrated when you add them to the library list.
Their contents do not display because they have not yet been converted.
To display their contents, you must migrate the target. </p>
</div><p>You can migrate a target from the Workspace tab of the System
Tree by selecting Migrate from the pop-up menu for the target. You
can also migrate targets in the Library painter if they are in your
current workspace.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To migrate a target in the Library painter:</p>
<ol type="1"><li class="fi"><p>Select the target you want to migrate and
select Entry&gt;Target&gt;Migrate from the menu bar.</p><p>The Migrate Application dialog box displays.</p>  </li>
<li class="ds"><p>Select OK to migrate all objects and libraries
in the target's path to the current version.</p>  </li></ol>
</div>
</body>
</html>

