<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Providing support for instance pooling</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp301.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp301.htm"/></a></span><span class="nextbtn"><a href="apptechp303.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp303.htm"/></a></span></p><a name="ccjcbead"></a><h1>Providing support for instance pooling</h1>
<a name="TI814"></a><div class="brhd"><h4>Benefits of instance pooling</h4>
<p><abbr title="e a server">EAServer</abbr> components can
optionally support instance pooling. <span class="glossterm">Instance pooling</span> allows <abbr title="e a server">EAServer</abbr> clients to reuse component
instances. By eliminating the resource drain caused by repeated
allocation of component instances, instance pooling improves the
overall performance of <abbr title="e a server">EAServer</abbr>. </p>
</div><a name="TI815"></a><div class="brhd"><h4>Specifying pooling options in the wizards</h4>
<p>When you create an <abbr title="e a server">EAServer</abbr> component
using one of the PowerBuilder wizards, you have the option to specify
one of the pooling options for the component shown in <a href="apptechp302.htm#CEGGJFHJ">Table 23-2</a>.</p>
<a name="ceggjfhj"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 23-2: EAServer component
pooling options</caption>
<tr><th rowspan="1"><p class="firstpara">Pooling option</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Supported</p>
</td>
<td rowspan="1"><p class="firstpara">The component is <span class="emphasis">always</span> pooled
after each client use. When this option is selected, the CanBePooled
event is not triggered for the component.</p>
<p>This option has the effect of setting the component's
pooling property to <span class="cmdname">TRUE</span>. If the Automatic Demarcation/Deactivation setting
for the component is enabled, instances are pooled after each method
invocation. If the setting is disabled, instances are pooled when
the component calls the <span class="cmdname">SetComplete</span> (or <span class="cmdname">SetAbort</span>) method
of the TransactionServer context object.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Not supported</p>
</td>
<td rowspan="1"><p class="firstpara">By default, the component is not pooled
after each client use. However, you can override the default behavior
by scripting the CanBePooled event. In the CanBePooled event, you
can specify programmatically whether a particular component instance should
be pooled. If you script the CanBePooled event, this event is triggered
after each client use.</p>
<p>This option has the effect of setting the component's
pooling property to <span class="cmdname">FALSE</span>.</p>
</td>
</tr>
</table>
</div><a name="TI816"></a><div class="brhd"><h4>Controlling the state of a pooled instance</h4>
<p>When you create an <abbr title="e a server">EAServer</abbr> component
that supports instance pooling, that component may need to reset
its state after each client has finished using the pooled instance. </p>
<p>To allow you to control the state of a component, <abbr title="e a server">EAServer</abbr> triggers one or more of
the events shown in <a href="apptechp302.htm#CEGCJDDH">Table 23-3</a> during the lifecycle of the component.</p>
<a name="cegcjddh"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 23-3: Component state events</caption>
<tr><th rowspan="1"><p class="firstpara">Event</p>
</th>
<th rowspan="1"><p class="firstpara">PBM code</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Activate</p>
</td>
<td rowspan="1"><p class="firstpara">PBM_COMPONENT_ACTIVATE</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">CanBePooled</p>
</td>
<td rowspan="1"><p class="firstpara">PBM_COMPONENT_CANBEPOOLED</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Deactivate</p>
</td>
<td rowspan="1"><p class="firstpara">PBM_COMPONENT_DEACTIVATE</p>
</td>
</tr>
</table>
<p>When the component's pooling option is set to Supported
(the pooling property is set to <span class="cmdname">TRUE</span>), you may
need to script the Activate and Deactivate events to reset the state
of the pooled component. This is necessary if the component maintains
state in an instance, shared, or global variable.</p>
<p>When the component's pooling option is set to Not
Supported (the pooling property is set to <span class="cmdname">FALSE</span>),
you can optionally script the CanBePooled event to specify whether
a particular component instance should be pooled. If you script the
CanBePooled event, you may also need to script the Activate and Deactivate
events to reset the state of the pooled component. If you do not script
the CanBePooled event, the component instance is not pooled.</p>
<p>The <abbr title="e a server">EAServer</abbr> Component Target
and Object wizards automatically include the Activate and Deactivate
events to a custom class user object that will be deployed as an <abbr title="e a server">EAServer</abbr> component. If you want
to script the CanBePooled event, you need to add this event yourself.
If you do this, be sure to map the event to the correct PBM code.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Constructor and Destructor are fired once</span> <p class="notepara">When instance pooling is in effect, the Constructor and Destructor
events are fired only once for the component. The Constructor and
Destructor events are not fired each time a new client uses the
component instance. Therefore, to reset the state of a component
instance that is pooled, add logic to the Activate and Deactivate
events, not the Constructor and Destructor events.</p>
</div></div><a name="TI817"></a><div class="brhd"><h4>Instance pool timeout</h4>
<p>Instance pooling can decrease client response time, but can
also increase memory usage in the server. You can specify how long,
in seconds, an instance can remain idle in the pool. The default
is 600 (ten minutes). To free resources used by idle component instances,
the server may remove instances that remain idle past this time
limit. </p>
<p>You can set environment variables to configure the way memory
is managed in PowerBuilder and EAServer. For more information, see <a href="apptechp35.htm#BCGCEECG">"Configuring memory management"</a> and the technical
document <a href="http://www.sybase.com/detail?id=1027319">EAServer/PowerBuilder Memory Tuning and Troubleshooting</a>
.</p>
</div><a name="TI818"></a><div class="brhd"><h4>The lifecycle of a component</h4>
<p>To understand how instance pooling works, you need to understand
the lifecycle of a component instance. This is what happens during
the component lifecycle:</p><ol><li><p class="firstbullet">The
component is typically instantiated on the first method invocation. When
this occurs on a component developed in PowerBuilder, <abbr title="e a server">EAServer</abbr> creates a new PowerBuilder
session for the component to run in. </p>
  </li>
<li><p class="ds">The PowerBuilder session creates the instance of
the PowerBuilder nonvisual object that represents the <abbr title="e a server">EAServer</abbr> component. Creating the object
causes the <span class="glossterm">Constructor event</span> to be fired.</p>  </li>
<li><p class="ds">After the object has been instantiated, <abbr title="e a server">EAServer</abbr> triggers the <span class="glossterm">Activate event</span> on
the nonvisual object to notify the object that it is about to be
used by a new client. At this point, the component must ensure that
its state is ready for execution.</p>  </li>
<li><p class="ds"><abbr title="e a server">EAServer</abbr> then
executes the method called by the client on the component. </p>  </li>
<li><p class="ds">When the component indicates that its work is complete, <abbr title="e a server">EAServer</abbr> triggers the <span class="glossterm">Deactivate
event</span> to allow the component to clean up its state. If
the Automatic Demarcation/Deactivation setting for the
component is enabled, the Deactivate event is triggered automatically
after each method invocation. If the setting is disabled, the Deactivate
event is triggered when the component calls the <span class="cmdname">SetComplete</span> (or <span class="cmdname">SetAbort</span>)
method of the TransactionServer context object. </p>  </li>
<li><p class="ds">If you have selected the Not Supported pooling option
(or set the component's pooling property to <span class="cmdname">FALSE</span>)
and also scripted the <span class="glossterm">CanBePooled event</span>, <abbr title="e a server">EAServer</abbr> triggers this event to
ask the component whether it is able to be pooled at this time.
The CanBePooled event allows the component instance to selectively
enable or refuse pooling. </p><p class="lipara">The return value of the CanBePooled event determines whether
the component instance is pooled. A return value of 1 enables pooling;
a return value of 0 disables pooling. If the CanBePooled event has
not been scripted, then by default the instance is not pooled.</p><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">What happens when the pooling property is enabled</span> <p class="notelistpara">When you select the Supported pooling option (or set the component's pooling
property to <span class="cmdname">TRUE</span>), component instances are <span class="emphasis">always</span> pooled
and the CanBePooled event is never triggered.</p>
</div>  </li>
<li><p class="ds">If an instance is not pooled after deactivation, <abbr title="e a server">EAServer</abbr> triggers the <span class="glossterm">Destructor
event</span>. Then it destroys the PowerBuilder object and terminates
the runtime session.</p>  </li>
</ol>
</div>
</body>
</html>

