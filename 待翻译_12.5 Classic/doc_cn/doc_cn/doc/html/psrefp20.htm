<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>The Any datatype</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="psrefp19.htm" title="Previous"><img src="../image/arrow-left.png" alt="psrefp19.htm"/></a></span><span class="nextbtn"><a href="psrefp21.htm" title="Next"><img src="../image/arrow-right.png" alt="psrefp21.htm"/></a></span></p><a name="ccjeahic"></a><h1>The Any datatype</h1>
<a name="TI129"></a><div class="brhd"><h4>General information</h4>
<p>PowerBuilder also supports the <span class="dt">Any</span> datatype,
which can hold any kind of value, including standard datatypes,
objects, structures, and arrays. A variable whose type is <span class="dt">Any</span> is
a chameleon datatype&#8212;it takes the datatype of the value assigned
to it. </p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Do not use Any in EAServer component
definition</span> <p class="notepara">The <span class="dt">Any</span> datatype is specific to PowerScript
and is not supported in the IDL of an <acronym title="E A Server">EAServer</acronym> component.
CORBA has a datatype called <span class="dt">Any</span> that can assume any
legal IDL type at runtime, but it is not semantically equivalent
to the PowerBuilder <span class="dt">Any</span> type. You must exclude
the PowerBuilder <span class="dt">Any</span> datatype from the component
interface definition, but you can use it within the component. </p>
</div></div><a name="TI130"></a><div class="brhd"><h4>Declarations and assignments </h4>
<p>You declare <span class="dt">Any</span> variables just as you
do any other variable. You can also declare an array of <span class="dt">Any</span> variables,
where each element of the array can have a different datatype.</p>
</div><p>You assign data to <span class="dt">Any</span> variables with
standard assignment statements. You can assign an array to a simple <span class="dt">Any</span> variable. </p>
<p>After you assign a value to an <span class="dt">Any</span> variable,
you can test the variable with the <span class="cmdname">ClassName</span> function
and find out the actual datatype:</p><pre class="userinput">any la_spreadsheetdata</pre><pre class="userinput">la_spreadsheetdata = ole_1.Object.cells(1,1).value</pre><pre class="userinput">CHOOSE CASE ClassName(la_spreadsheetdata)</pre><pre class="userinput">    CASE "integer"</pre><pre class="userinput">        ...</pre><pre class="userinput">    CASE "string"</pre><pre class="userinput">        ...</pre><pre class="userinput">END CHOOSE</pre>
<p>These rules apply to <span class="dt">Any</span> assignments:</p><ul><li><p class="firstbullet">You can assign anything into
an <span class="dt">Any</span> variable. </p>
  </li>
<li><p class="ds">You must know the content of an <span class="dt">Any</span> variable
to make assignments from the <span class="dt">Any</span> variable to
a compatible datatype.</p>  </li>
</ul>
<a name="TI131"></a><div class="brhd"><h4>Restrictions </h4>
<p>If the value of a simple <span class="dt">Any</span> variable
is an array, you cannot access the elements of the array until you
assign the value to an array variable of the appropriate datatype.
This restriction does not apply to the opposite case of an array
of <span class="dt">Any</span> variables&#8212;you can access each <span class="dt">Any</span> variable
in the array.</p>
</div><p>If the value of an <span class="dt">Any</span> variable is a
structure, you cannot use dot notation to access the elements of
the structure until you assign the value to a structure of the appropriate
datatype.</p>
<p>After a value has been assigned to an <span class="dt">Any</span> variable,
it cannot be converted back to a generic <span class="dt">Any</span> variable
without a datatype. Even if you set it to <span class="cmdname">NULL</span>,
it retains the datatype of the assigned value until you assign another
value.</p>
<a name="TI132"></a><div class="brhd"><h4>Operations and expressions </h4>
<p>You can perform operations on <span class="dt">Any</span> variables
as long as the datatype of the data in the <span class="dt">Any</span> variable
is appropriate to the operator. If the datatype is not appropriate
to the operator, an execution error occurs.</p>
</div><p>For example, if instance variables <span class="variable">ia_1</span> and <span class="variable">ia_2</span> contain
numeric data, this statement is valid:</p><pre class="userinput">any la_3</pre><pre class="userinput">la_3 = ia_1 - ia_2</pre>
<p>If <span class="variable">ia_1</span> and <span class="variable">ia_2</span> contain
strings, you can use the concatenation operator:</p><pre class="userinput">any la_3</pre><pre class="userinput">la_3 = ia_1 + ia_2</pre>
<p>However, if <span class="variable">ia_1</span> contained a
number and <span class="variable">ia_2</span> contained a string,
you would get an execution error.</p>
<a name="TI133"></a><div class="formalpara"><p class="title">Datatype conversion functions</p><p class="firstpara">PowerScript datatype conversion functions accept <span class="cmdname">Any</span> variables
as arguments. When you call the function, the <span class="cmdname">Any</span> variable
must contain data that can be converted to the specified type.</p>
</div><p>For example, if <span class="variable">ia_any</span> contains
a string, you can assign it to a <span class="dt">string</span> variable:</p><pre class="userinput">ls_string = ia_any</pre>
<p>If <span class="variable">ia_any</span> contains a number
that you want to convert to a <span class="dt">string</span>, you can
call the <span class="cmdname">String</span> function:</p><pre class="userinput">ls_string = String(ia_any)</pre>
<a name="TI134"></a><div class="formalpara"><p class="title">Other functions</p><p class="firstpara">If a function's prototype does not allow <span class="dt">Any</span> as
a datatype for an argument, you cannot use an <span class="dt">Any</span> variable
without a conversion function, even if it contains a value of the
correct datatype. When you compile the script, you get compiler
errors such as <code class="ce">Unknown function</code> or <code class="ce"></code><code class="ce">Function
not found</code>.</p>
</div><p>For example, the argument for the <span class="cmdname">Len</span> function
refers to a <span class="dt">string</span> column in a DataWindow,
but the expression itself has a type of <span class="dt">Any</span>:</p><pre class="userinput">IF Len(dw_notes.Object.Notes[1]) &gt; 0 THEN  // Invalid</pre>
<p>This works because the <span class="dt">string</span> value of
the <span class="dt">Any</span> expression is explicitly converted
to a string:</p><pre class="userinput">IF Len(String(dw_notes.Object.Notes[1])) &gt; 0 THEN</pre>
<a name="TI135"></a><div class="formalpara"><p class="title">Expressions whose datatype is Any</p><p class="firstpara">Expressions that access data whose type is unknown when the
script is compiled have a datatype of <span class="dt">Any</span>.
These expressions include expressions or functions that access data
in an OLE object or a DataWindow object: </p><pre class="userinput">myoleobject.application.cells(1,1).value</pre><pre class="userinput">dw_1.Object.Data[1,1]</pre><pre class="userinput">dw_1.Object.Data.empid[99]</pre>
</div><p>The objects these expressions point to can change so that
the type of data being accessed also changes.</p>
<p>Expressions that refer to DataWindow data can return arrays
and structures and arrays of structures as <span class="dt">Any</span> variables.
For best performance, assign the DataWindow expression to the appropriate
array or structure without using an intermediate <span class="dt">Any</span> variable.</p>
<a name="TI136"></a><div class="brhd"><h4>Overusing the Any datatype</h4>
<p>Do not use <span class="dt">Any</span> variables as a substitute
for selecting the correct datatype in your scripts. There are two
reasons for this:</p><ul><li><a name="TI137"></a><div class="formalpara"><p class="title">At execution
time, using Any variables is slow</p><p class="firstpara">PowerBuilder must do much more processing to determine datatypes
before it can make an assignment or perform an operation involving <span class="dt">Any</span> variables.
In particular, an operation performed many times in a loop will
suffer greatly if you use <span class="dt">Any</span> variables instead
of variables of the appropriate type.</p>
</div>  </li>
<li><a name="TI138"></a><div class="formalpara"><p class="title">At compile time, using Any variables
removes a layer of error checking from your programming</p><p class="firstpara"> The PowerBuilder compiler makes sure datatypes are correct
before code gets executed. With <span class="dt">Any</span> variables, some
of the errors that can be caught by the compiler are not found until the
code is run.</p>
</div>  </li>
</ul>
</div>
</body>
</html>

