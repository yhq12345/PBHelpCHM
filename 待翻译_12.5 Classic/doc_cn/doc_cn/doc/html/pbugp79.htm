<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Setting up a connection profile</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp78.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp78.htm"/></a></span><span class="nextbtn"><a href="pbugp80.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp80.htm"/></a></span></p><a name="babcgdib"></a><h2>Setting up a connection profile</h2>
<p>In
PowerBuilder you can set up a source control connection profile
at the workspace level only. Local and advanced connection options
can be defined differently on each computer for PowerBuilder workspaces
that contain the same targets.</p>
<a name="TI90"></a><div class="brhd"><h4>Local connection options</h4>
<p>Local connection options allow you to create a trace log to
record all source control activity for your current workspace session.
You can overwrite or preserve an existing log file for each session.</p>
<p>You can also make sure a comment is included for every file
checked into source control from your local computer. If you select
this connection option, the OK button on the Check In dialog box
is disabled until you type a comment for all the objects you are
checking in.</p>
<p>The following table lists the connection options you can use for
each local connection profile:</p>
<a name="TI91"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 3-2: Source control properties for a PowerBuilder workspace</caption>
<tr><th rowspan="1"><p class="firstpara">Select this option</p>
</th>
<th rowspan="1"><p class="firstpara">To do this</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Log All Source Management Activity (not
selected by default)</p>
</td>
<td rowspan="1"><p class="firstpara">Enable trace logging. By default the
log file name is <span class="filepath">PBSCC125.LOG</span>, which is saved
in your workspace directory, but you can select a different path
and file name.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Append To Log File (default selection
when logging is enabled)</p>
</td>
<td rowspan="1"><p class="firstpara">Append source control activity information to
named log file when logging is enabled.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Overwrite Log File (not selected by default)</p>
</td>
<td rowspan="1"><p class="firstpara">Overwrite the named log file with source control
activity of the current session when logging is enabled.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Require Comments On Check In (not selected
by default; not available for PBNative source control)</p>
</td>
<td rowspan="1"><p class="firstpara">Disable the OK button on the Check In
dialog box until you type a comment. </p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">This Project Requires That I Sometimes
Work Offline (not selected by default)</p>
</td>
<td rowspan="1"><p class="firstpara">Disable automatic connection to source control
when you open the workspace.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Delete PowerBuilder Generated Object
Files (not selected by default)</p>
</td>
<td rowspan="1"><p class="firstpara">Remove object files (such as SRDs) from
the local directory after they are checked into source control.
This may increase the time it takes for PowerBuilder to refresh
source control status, but it minimizes the drive space used by
temporary files. You cannot select this option for the Perforce,
ClearCase, or Continuus source control systems.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Perform Diff On Status Update</p>
</td>
<td rowspan="1"><p class="firstpara">Permit display of out-of-sync icons for
local objects that are different from objects on the source control
server. Selecting this also increases the time it takes to refresh
source control status. You cannot select this option for Perforce.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Suppress prompts to overwrite read&#8211;only
files</p>
</td>
<td rowspan="1"><p class="firstpara">Avoid message boxes warning that read-only files
exist on your local project directory. </p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Show warning when opening objects not
checked out</p>
</td>
<td rowspan="1"><p class="firstpara">Avoid message boxes when opening objects that
are still checked in to source control.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Status Refresh Rate (5 minutes by default)</p>
</td>
<td rowspan="1"><p class="firstpara">Specifies the minimum time elapsed before PowerBuilder
automatically requests information from the source control server
to determine if objects are out of sync. Valid values are between
1 and 59 minutes. Status refresh rate is ignored when you are working offline.</p>
</td>
</tr>
</table>
</div><a name="TI92"></a><div class="brhd"><h4>Advanced connection options</h4>
<p>Advanced
connection options depend on the source control system you are using
to store your workspace objects. Different options exist for different source
control systems.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Applicability of advanced options</span> <p class="notepara">Some advanced options might not be implemented or might be
rendered inoperable by the PowerBuilder SCC API interface. For example,
if an advanced option allows you to make local files writable after
an Undo Check Out operation, PowerBuilder still creates read-only
files when reverting an object to the current version in source
control. (PowerBuilder might even delete these files if you selected
the Delete PowerBuilder Generated Object Files option.)</p>
</div></div><div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To set up a connection profile:</p>
<ol type="1"><li class="fi"><p>Right-click the Workspace object in the
System Tree (or in the Tree view of the Library painter) and select
Properties from the pop-up menu.</p>  </li>
<li class="ds"><p>Select the Source Control tab from the Workspace
Properties dialog box.</p>  </li>
<li class="ds"><p>Select the system you want to use from the Source
Control System drop&#8211;down list.</p><p>Only source control systems that are defined in your registry (<span class="filepath">HKEY_LOCAL_MACHINE\SOFTWARE\SourceCodeControlProvider\<br/>InstalledSCCProviders</span>)
appear in the drop-down list.</p>  </li>
<li class="ds"><p>Type in your user name for the source control
system.</p><p>Some source control systems use a login name from your registry
rather than the user name that you enter here. For these systems
(such as Perforce or PVCS), you can leave this field blank.</p>  </li>
<li class="ds"><p>Click the ellipsis button next to the Project
text box.</p><p>A dialog box from your source control system displays. Typically
it allows you to select or create a source control project. </p><p>The dialog box displayed for PBNative is shown below:</p><div class="listfig"><img src="../image/pbnat1.gif" alt="pbnat1.gif"/></div>
  </li>
<li class="ds"><p>Fill in the information required by your source
control system and click OK.</p><p>The Project field on the Source Control page of the Workspace
Properties dialog box is typically populated with the project name
from the source control system you selected. However, some source
control systems (such as Perforce or Vertical Sky) do not return
a project name. For these systems, you can leave this field blank.</p>  </li>
<li class="ds"><p>Type or select a path for the local root directory.</p><p>All the files that you check into and out of source control
must reside in this path or in a subdirectory of this path. </p>  </li>
<li class="ds"><p>(Option) Select the options you want for your
local workspace connection to the source control server.</p>  </li>
<li class="ds"><p>(Option) Click the Advanced button and make any
changes you want to have apply to advanced options defined for your
source control system.</p><p>The Advanced button might be grayed if you are not first connected
to a source control server. If Advanced options are not supported
for your source control system, you see only a splash screen for
the system you selected and an OK button that you can click to return
to the Workspace Properties dialog box.</p>  </li>
<li class="ds"><p>Click Apply or click OK.</p>  </li></ol>
</div>
</body>
</html>

