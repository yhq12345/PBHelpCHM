<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>OLEObject object type</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp237.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp237.htm"/></a></span><span class="nextbtn"><a href="apptechp239.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp239.htm"/></a></span></p><a name="apptech-ole-objtype"></a><h2>OLEObject object type</h2>
<p>PowerBuilder's OLEObject object type is designed
for automation. OLEObject is a dynamic object type, which means
that the compiler will accept any property names, function names,
and parameter lists for the object. PowerBuilder does not have to
know whether the properties and functions are valid. This allows
you to call methods and set properties for the object that are known
to the server application that created the object. If the functions
or properties do not exist during execution, you will get runtime
errors.</p>
<p>Using an OLEObject variable involves these steps:</p>
<ol><li><p class="ds">Declare the variable and instantiate it.</p>  </li>
<li><p class="ds">Connect to the OLE object.</p>  </li>
<li><p class="ds">Manipulate the object as appropriate using the OLE
server's properties and functions.</p>  </li>
<li><p class="ds">Disconnect from the OLE object and destroy the variable.</p>  </li>
</ol>
<p>These steps are described next.</p>
<a name="TI691"></a><div class="brhd"><h4>Declaring an OLEObject variable</h4>
<p>You need to declare an OLEObject variable and allocate memory
for it:</p><pre class="userinput">OLEObject myoleobject<br/>myoleobject = CREATE OLEObject</pre>
<p>The Object property of the OLE container controls (OLEControl
or OLECustomControl) has a datatype of <span class="dt">OLEObject</span>.</p>
</div><a name="TI692"></a><div class="brhd"><h4>Connecting to the server</h4>
<p>You establish a connection between the OLEObject object and
an OLE server with one of the <span class="cmdname">ConnectToObject</span> functions.
Connecting to an object starts the appropriate server:</p>
<a name="TI693"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 19-4: ConnectToObject
functions </caption>
<tr><th rowspan="1"><p class="firstpara">When you want to</p>
</th>
<th rowspan="1"><p class="firstpara">Choose this function</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Create a new object for an OLE server
that you specify. Its purpose is similar to <span class="cmdname">InsertClass</span> for
a control.</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">ConnectToNewObject</span></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Create a new OLE object in the specified
remote server application if security on the server allows it and
associate the new object with a PowerBuilder OLEObject variable.</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">ConnectToNewRemoteObject</span></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Open an existing OLE object from a file.
If you do not specify an OLE class, PowerBuilder uses the file's
extension to determine what server to start.</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">ConnectToObject</span></p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Associate an OLE object with a PowerBuilder OLEObject
variable and start the remote server application.</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">ConnectToRemoteObject</span></p>
</td>
</tr>
</table>
<p>After you establish a connection, you can use the server's
command set for automation to manipulate the object (see <a href="apptechp242.htm#X-REF348432491">"OLE objects in scripts "</a>).</p>
<p>You do not need to include application qualifiers for the
commands. You already specified those qualifiers as the application's
class when you connected to the server. For example, the following
commands create an OLEObject variable, connect to Microsoft Word 's
OLE interface (word.application), open a document and display information
about it, insert some text, save the edited document, and shut down
the server:</p><pre class="userinput">OLEObject o1<br/>string s1<br/>o1 = CREATE oleobject<br/> <br/>o1.ConnectToNewObject("word.application")<br/>o1.documents.open("c:\temp\temp.doc")<br/> <br/>// Make the object visible and display the <br/>// MS Word user name and file name<br/>o1.Application.Visible = True<br/>s1 = o1.UserName<br/>MessageBox("MS Word User Name", s1)<br/>s1 = o1.ActiveDocument.Name<br/>MessageBox("MS Word Document Name", s1)<br/> <br/>//Insert some text in a new paragraph<br/>o1.Selection.TypeParagraph()<br/>o1.Selection.typetext("Insert this text")<br/>o1.Selection.TypeParagraph() <br/> <br/>// Insert text at the first bookmark<br/>o1.ActiveDocument.Bookmarks[1].Select<br/>o1.Selection.typetext("Hail!")<br/> <br/>// Insert text at the bookmark named End<br/>o1.ActiveDocument.Bookmarks.item("End").Select<br/>o1.Selection.typetext("Farewell!")<br/> <br/>// Save the document and shut down the server<br/>o1.ActiveDocument.Save()<br/>o1.quit()<br/>RETURN</pre>
<p>For earlier versions of Microsoft Word, use word.basic instead
of word.application. The following commands connect to the Microsoft
Word 7.0 OLE interface (word.basic), open a document, go to a bookmark
location, and insert the specified text:</p><pre class="userinput">myoleobject.ConnectToNewObject("word.basic")<br/>myoleobject.fileopen("c:\temp\letter1.doc")<br/>myoleobject.editgoto("NameAddress")<br/>myoleobject.Insert("Text to insert")</pre>
<p>Do <span class="emphasis">not</span> include word.application or word.basic
(the class in <span class="cmdname">ConnectToNewObject</span>) as a qualifier:</p><pre class="userinput">// Incorrect command qualifier<br/>myoleobject.word.basic.editgoto("NameAddress")</pre>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Microsoft Word 7.0 implementation </span> <p class="notepara">For an OLEObject variable, word.basic is the class name of
Word 7.0 as a server application. For an object in a control, you
must use the qualifier application.wordbasic to tell Word how to
traverse its object hierarchy and access its wordbasic object. </p>
</div></div><a name="TI694"></a><div class="brhd"><h4>Shutting down and disconnecting from the server</h4>
<p>After your application has finished with the automation, you
might need to tell the server explicitly to shut down. You can also
disconnect from the server and release the memory for the object:</p>
<pre class="userinput">myoleobject.Quit() <br/>rtncode = myoleobject.DisconnectObject()<br/>DESTROY myoleobject</pre>
<p>You can rely on garbage collection to destroy the OLEObject
variable. Destroying the variable automatically disconnects from
the server. </p>
<p>It is preferable to use garbage collection to destroy objects,
but if you want to release the memory used by the variable immediately
and you know that it is not being used by another part of the application,
you can explicitly disconnect and destroy the OLEObject variable,
as shown in the code above.</p>
<p>For more information, see <a href="apptechp34.htm#CAICHCGE">"Garbage collection and memory
management"</a>.</p>
</div>
</body>
</html>

