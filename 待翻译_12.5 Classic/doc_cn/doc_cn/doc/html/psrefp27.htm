<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>About using variables </title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="psrefp26.htm" title="Previous"><img src="../image/arrow-left.png" alt="psrefp26.htm"/></a></span><span class="nextbtn"><a href="psrefp28.htm" title="Next"><img src="../image/arrow-right.png" alt="psrefp28.htm"/></a></span></p><a name="bcgedffg"></a><h2>About using variables </h2>
<a name="TI153"></a><div class="brhd"><h4>General information</h4>
<p>To use or set a variable's value in a PowerBuilder
script, you name the variable. The variable must be known to the
compiler&#8212;in other words, it must be in scope.</p>
</div><p>You can use a variable anywhere you need its value&#8212;for
example, as a function argument or in an assignment statement.</p>
<a name="TI154"></a><div class="brhd"><h4>How PowerBuilder looks for variables </h4>
<p>When PowerBuilder executes a script and finds an unqualified
reference to a variable, it searches for the variable in the following
order:</p><ol><li><p class="ds">A local variable</p>  </li>
<li><p class="ds">A shared variable</p>  </li>
<li><p class="ds">A global variable</p>  </li>
<li><p class="ds">An instance variable</p>  </li>
</ol>
</div><p>As soon as PowerBuilder finds a variable with the specified
name, it uses the variable's value.</p>
<a name="TI155"></a><div class="brhd"><h4>Referring to global variables</h4>
<p>To refer to a global variable, you specify its name in a script.
However, if the global variable has the same name as a local or
shared variable, the local or shared variable will be found first.</p>
</div><p>To refer to a global variable that is masked by a local or
shared variable of the same name, use the global scope operator
(::) before the name:</p><pre class="syntax">::<span class="variable">globalname</span></pre>
<p>For example, this statement compares the value of local and
global variables, both named total:</p><pre class="userinput">IF total &lt; ::total THEN ...</pre>
<a name="TI156"></a><div class="brhd"><h4>Referring to instance variables </h4>
<p>You can refer to an instance variable in a script if there
is an instance of the object open in the application. Depending
on the situation, you might need to qualify the name of the instance
variable with the name of the object defining it.</p>
</div><a name="TI157"></a><div class="formalpara"><p class="title">Using unqualified names</p><p class="firstpara">You can refer to instance variables without qualifying them
with the object name in the following cases:</p><ul><li><p class="firstbullet">For application-level variables, in scripts for
the application object</p>
  </li>
<li><p class="ds">For window-level variables, in scripts for the window
itself and in scripts for controls in that window</p>  </li>
<li><p class="ds">For user-object-level variables, in scripts for
the user object itself and in scripts for controls in that user
object</p>  </li>
<li><p class="ds">For menu-level variables, in scripts for a menu
object, either the highest-level menu or scripts for the menu objects
included as items on the menu</p>  </li>
</ul>
</div><p>For example, if <span class="dbobject">w_emp</span> has an
instance variable <span class="variable">EmpID</span>, then you can reference <span class="variable">EmpID</span> without
qualification in any script for <span class="dbobject">w_emp</span> or
its controls as follows:</p><pre class="userinput">sle_id.Text = EmpID</pre>
<a name="TI158"></a><div class="formalpara"><p class="title">Using qualified names</p><p class="firstpara">In all other cases, you need to qualify the name of the instance
variable with the name of the object using dot notation: </p>
</div><pre class="syntax"><span class="variable">object.instancevariable</span></pre>
<p>This requirement applies only to Public instance variables.
You cannot reference Private instance variables outside the object
at all, qualified or not.</p>
<p>For example, to refer to the <span class="dbobject">w_emp</span> instance
variable <span class="variable">EmpID</span> from a script outside the window,
you need to qualify the variable with the window name:</p><pre class="userinput">sle_ID.Text = w_emp.EmpID</pre>
<p>There is another situation in which references must be qualified.
Suppose that <span class="dbobject">w_emp</span> has an instance
variable <span class="variable">EmpID</span> and that in <span class="dbobject">w_emp</span> there
is a CommandButton that declares a local variable <span class="variable">EmpID</span> in
its Clicked script. In that script, you must qualify all references
to the instance variable:</p><pre class="userinput">Parent.EmpID</pre>
<a name="TI159"></a><div class="brhd"><h4>Using pronouns as name qualifiers</h4>
<p>To avoid ambiguity when referring to variables, you might
decide to always use qualified names for object variables. Qualified
names leave no doubt about whether a variable is local, instance,
or shared.</p>
</div><p>To write generic code but still use qualified names, you can
use the pronouns <span class="cmdname">This</span> and <span class="cmdname">Parent</span> to
refer to objects. Pronouns keep a script general by allowing you
to refer to the object without naming it specifically.</p>
<a name="TI160"></a><div class="formalpara"><p class="title">Window variables in window scripts</p><p class="firstpara">In a window script, use the pronoun <span class="cmdname">This</span> to
qualify the name of a window instance variable. For example, if
a window has an instance variable called <span class="variable">index</span>,
then the following statements are equivalent in a script for that
window, as long as there is no local or global variable named <span class="variable">index</span>: </p>
</div><pre class="userinput">index = 5</pre><pre class="userinput">This.index = 5</pre>
<a name="TI161"></a><div class="formalpara"><p class="title">Window variables in control scripts</p><p class="firstpara">In a script for a control in a window, use the pronoun <span class="cmdname">Parent</span> to
qualify the name of a window instance variable&#8212;the window
is the parent of the control. In this example, the two statements
are equivalent in a script for a control in that window, as long
as there is no local or global variable named "index":</p>
</div><pre class="userinput">index = 5</pre><pre class="userinput">Parent.index = 5</pre>
<a name="TI162"></a><div class="formalpara"><p class="title">Naming errors</p><p class="firstpara">If a local or global variable exists with the name "index," then
the unqualified name refers to the local or global variable. It
is a programming error if you meant to refer to the object variable.
You get an informational message from the compiler if you use the
same name for instance and global variables.</p>
</div>
</body>
</html>

