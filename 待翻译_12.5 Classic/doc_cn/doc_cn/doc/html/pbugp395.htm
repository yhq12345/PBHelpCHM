<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Working with database components</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp394.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp394.htm"/></a></span><span class="nextbtn"><a href="pbugp396.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp396.htm"/></a></span></p><a name="bfccaeff"></a><h1>Working with database components</h1>
<p>A database is an electronic storage place for data. Databases
are designed to ensure that data is valid and consistent and that
it can be accessed, modified, and shared.</p>
<p>A database management system (DBMS) governs the activities
of a database and enforces rules that ensure data integrity. A <span class="emphasis">relational</span> DBMS stores
and organizes data in tables.</p>
<a name="bdchbaje"></a><div class="brhd"><h4>How you work with databases
in PowerBuilder</h4>
<p>You can use PowerBuilder to work with the following database
components:</p><ul><li><p class="firstbullet">Tables and columns</p>
  </li>
<li><p class="ds">Keys</p>  </li>
<li><p class="ds">Indexes</p>  </li>
<li><p class="ds">Database views</p>  </li>
<li><p class="ds">Extended attributes</p>  </li>
<li><p class="ds">Additional database components</p>  </li>
</ul>
</div><a name="bdcedbid"></a><div class="brhd"><h4>Tables and columns</h4>
<p>A database usually has many tables, each of which contains
rows and columns of data. Each row in a table has the same columns,
but a column's value for a particular row could be empty
or <span class="cmdname">NULL</span> if the column's definition allows
it. </p>
<p>Tables often have relationships with other tables. For example,
in the <span class="dbobject">EAS Demo DB</span> included with PowerBuilder,
the <span class="dbobject">Department</span> table has a <span class="dbobject">Dept_id</span> column,
and the <span class="dbobject">Employee</span> table also has a <span class="dbobject">Dept_id</span> column
that identifies the department in which the employee works. When
you work with the <span class="dbobject">Department</span> table and the <span class="dbobject">Employee</span> table,
the relationship between them is specified by a join of the two
tables.</p>
</div><a name="bdchhdbf"></a><div class="brhd"><h4>Keys</h4>
<p>Relational databases use keys to ensure database integrity. </p>
<a name="TI494"></a><div class="formalpara"><p class="title">Primary keys</p><p class="firstpara">A primary key is a column or set of columns that uniquely identifies
each row in a table. For example, two employees may have the same first
and last names, but they have unique ID numbers. The <span class="dbobject">Emp_id</span> column
in the Employee table is the primary key column.</p>
</div><a name="TI495"></a><div class="formalpara"><p class="title">Foreign keys</p><p class="firstpara">A foreign key is a column or set of columns that contains primary
key values from another table. For example, the <span class="dbobject">Dept_id</span> column
is the primary key column in the <span class="dbobject">Department</span> table
and a foreign key in the <span class="dbobject">Employee</span> table.</p>
</div><a name="TI496"></a><div class="formalpara"><p class="title">Key icons</p><p class="firstpara">In PowerBuilder, columns defined as keys are displayed with
key icons that use different shapes and colors for primary and foreign. PowerBuilder automatically
joins tables that have a primary/foreign key relationship,
with the join on the key columns. </p>
</div><p>In the following illustration there is a join on the <span class="dbobject">dept_id</span> column,
which is a primary key for the <span class="dbobject">department</span> table
and a foreign key for the <span class="dbobject">employee</span> table:</p>
<div class="fig"><img src="../image/db0001.gif" alt="db0001.gif"/></div>
<p>For more information, see <a href="pbugp418.htm#CCJDECFJ">"Working with keys"</a>.</p>
</div><a name="bdcdjhdf"></a><div class="brhd"><h4>Indexes</h4>
<p>An index is a column or set of columns you identify to improve
database performance when searching for data specified by the index.
You index a column that contains information you will need frequently.
Primary and foreign keys are special examples of indexes.</p>
<p>You specify a column or set of columns with unique values
as a unique index, represented by an icon with a single key.</p>
<p>You specify a column or set of columns that has values that
are not unique as a duplicate index, represented by an icon with
two file cabinets.</p>
<p>For more information, see <a href="pbugp419.htm#BFCDAGAJ">"Working with indexes"</a>.</p>
</div><a name="bdcjffdf"></a><div class="brhd"><h4>Database views</h4>
<p>If you often select data from the same tables and columns,
you can create a database view of the tables. You give the database
view a name, and each time you refer to it the associated <span class="cmdname">SELECT</span> command
executes to find the data. </p>
<p>Database views are listed in the Objects view of the Database painter and
can be displayed in the Object Layout view, but a database view
does not physically exist in the database in the same way that a
table does. Only its definition is stored in the database, and the
view is re-created whenever the definition is used. </p>
<p>Database administrators often create database views for security
purposes. For example, a database view of an Employee table that
is available to users who are not in Human Resources might show
all columns except Salary.</p>
<p>For more information, see <a href="pbugp420.htm#CCJCAHJF">"Working with database views "</a>.</p>
</div><a name="bdchgehi"></a><div class="brhd"><h4>Extended attributes</h4>
<p>Extended attributes enable you to store information about
a table's columns in special system tables. Unlike tables,
keys, indexes, and database views (which are DBMS-specific), extended
attributes are PowerBuilder-specific. The most powerful extended attributes
determine the edit style, display format, and validation rules for
the column. </p>
<p>For more information about extended attributes,
see <a href="pbugp407.htm#BABBHCBH">"Specifying column extended
attributes"</a>. For
more information about the extended attribute system tables, see <a href="pbugp901.htm#CCJEBFGB">Appendix A, "The Extended
Attribute System Tables"</a></p>
</div><a name="bdcijeac"></a><div class="brhd"><h4>Additional database components</h4>
<p>Depending on the database to which you are connected and on
your user privileges, you may be able to view or work with a variety
of additional database components through PowerBuilder. These components
might include:</p><ul class="simplelist"><li><p class="firstsimple">Driver information</p>
  </li>
<li><p class="ds">Groups</p>  </li>
<li><p class="ds">Metadata types</p>  </li>
<li><p class="ds">Procedures and functions</p>  </li>
<li><p class="ds">Users</p>  </li>
<li><p class="ds">Logins</p>  </li>
<li><p class="ds">Triggers</p>  </li>
<li><p class="ds">Events</p>  </li>
<li><p class="ds">Web services</p>  </li>
</ul>
<p>For example, driver information is relevant to ODBC connections.
It lists all the ODBC options associated with the ODBC driver, allowing
you to determine how the ODBC interface will behave for a given
connection. Login information is listed for Adaptive Server&#174; Enterprise
database connections. Information about groups and users is listed
for several of the databases and allows you to add new users and
groups and maintain passwords for existing users.</p>
<p>You can drag most items in these folders to the Object Details
view to display their properties. You can also drag procedures,
functions, triggers, and events to the ISQL view.</p>
<p>Trigger information is listed for Adaptive Server Enterprise
and SQL Anywhere tables. A trigger is a special form of stored procedure
that is associated with a specific database table. Triggers fire
automatically whenever someone inserts, updates or deletes rows
of the associated table. Triggers can call procedures and fire other
triggers, but they have no parameters and cannot be invoked by a <span class="cmdname">CALL</span> statement.
You use triggers when referential integrity and other declarative
constraints are insufficient.</p>
<p>Events can be used in a <acronym title="sequel">SQL</acronym> Anywhere
database to automate database administration tasks, such as sending
a message when disk space is low. Event handlers are activated when
a provided trigger condition is met. If any events are defined for
a <acronym title="sequel">SQL</acronym> Anywhere connection, they
display in the Events folder for the connection in the Objects view. </p>
</div>
</body>
</html>

