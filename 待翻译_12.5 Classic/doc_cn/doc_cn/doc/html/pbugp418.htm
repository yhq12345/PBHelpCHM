<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Working with keys</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp417.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp417.htm"/></a></span><span class="nextbtn"><a href="pbugp419.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp419.htm"/></a></span></p><a name="ccjdecfj"></a><h1>Working with keys</h1>
<p>If your DBMS supports primary and foreign keys, you can work
with the keys in PowerBuilder. </p>
<a name="TI510"></a><div class="brhd"><h4>Why you should use keys</h4>
<p>If your DBMS supports them, you should use primary and foreign
keys to enforce the referential integrity of your database. That
way you can rely on the DBMS to make sure that only valid values
are entered for certain columns instead of having to write code
to enforce valid values.</p>
<p>For example, say you have two tables called <span class="dbobject">Department</span> and <span class="dbobject">Employee</span>.
The <span class="dbobject">Department</span> table contains the column <span class="dbobject">Dept_Head_ID</span>,
which holds the ID of the department's manager. You want
to make sure that only valid employee IDs are entered in this column.
The only valid values for <span class="dbobject">Dept_Head_ID</span> in
the <span class="dbobject">Department</span> table are values for <span class="dbobject">Emp_ID</span> in
the <span class="dbobject">Employee</span> table.</p>
<p>To enforce this kind of relationship, you define a foreign
key for <span class="dbobject">Dept_Head_ID</span> that
points to the <span class="dbobject">Employee</span> table. With this key
in place, the DBMS disallows any value for <span class="dbobject">Dept_Head_ID</span> that
does not match an <span class="dbobject">Emp_ID</span> in the <span class="dbobject">Employee</span> table.</p>
<p>For more about primary and foreign keys, consult
a book about relational database design or your DBMS documentation.</p>
</div><a name="TI511"></a><div class="brhd"><h4>What you can do in the Database painter</h4>
<p>You can work with keys in the following ways:</p><ul><li><p class="firstbullet">Look at existing primary and foreign keys</p>
  </li>
<li><p class="ds">Open all tables that depend on a particular primary
key</p>  </li>
<li><p class="ds">Open the table containing the primary key used by
a particular foreign key</p>  </li>
<li><p class="ds">Create, alter, and drop keys</p>  </li>
</ul>
<p>For the most part, you work with keys the same way for each
DBMS that supports keys, but there are some DBMS-specific issues.
For complete information about using keys with your DBMS, see your
DBMS documentation.</p>
</div><a name="bdcfcfhj"></a><div class="brhd"><h4>Viewing keys</h4>
<p>Keys can be viewed in several ways:</p><ul><li><p class="firstbullet">In
the expanded tree view of a table in the Objects view</p>
  </li>
<li><p class="ds">As icons connected by lines to a table in the Object
Layout view</p>  </li>
</ul>
<p>In the following picture, the Department table has two keys:</p><ul><li><p class="firstbullet">A primary key (on <span class="dbobject">dept_id</span>)</p>
  </li>
<li><p class="ds">A foreign key (on <span class="dbobject">dept_head_id</span>)</p>  </li>
</ul>
<div class="fig"><img src="../image/db0013.gif" alt="db0013.gif"/></div>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">If you cannot see the lines</span> <p class="notepara">If the color of your window background makes it difficult
to see the lines for the keys and indexes, you can set the colors
for each component of the Database painter's graphical table
representation, including keys and indexes. For information, see <a href="pbugp398.htm#BABDFCJG">"Modifying database preferences"</a>.</p>
</div></div><a name="TI512"></a><div class="brhd"><h4>Opening related tables</h4>
<p>When working with tables containing keys, you can easily open
related tables.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To open the table that a particular foreign key
references:</p>
<ol type="1"><li class="fi"><p>Display the foreign key pop-up menu.</p>  </li>
<li class="ds"><p>Select Open Referenced Table.</p>  </li></ol>
</div><div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To open all tables referencing a particular primary
key:</p>
<ol type="1"><li class="fi"><p>Display the primary key pop-up menu.</p>  </li>
<li class="ds"><p>Select Open Dependent Table(s).</p><p>PowerBuilder opens and expands all tables in the database containing foreign
keys that reference the selected primary key.</p>  </li></ol>
</div></div><a name="bdcfdfif"></a><div class="brhd"><h4>Defining primary keys</h4>
<p>If your DBMS supports primary keys, you can define them in PowerBuilder.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To create a primary key:</p>
<ol type="1"><li class="fi"><p>Do one of the following:<ul><li><p class="firstbullet">Highlight
the table for which you want to create a primary key and click the
Create Primary Key drop-down toolbar button in PainterBar1.</p>
  </li>
<li><p class="ds">Select Object&gt;Insert&gt;Primary Key
from the main menu or New&gt;Primary Key from the pop-up menu.</p>  </li>
<li><p class="ds">Expand the table's tree view, right-click
Primary Key, and select New Primary Key from the pop-up menu. </p>  </li>
</ul>
                      </p><p>The Primary Key properties display in the Object Details view. </p>  </li>
<li class="ds"><p>Select one or more columns for the primary key.</p><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Columns that are allowed in a primary key</span> <p class="notepara">Only a column that does not allow null values can be included
as a column in a primary key definition. If you choose a column
that allows null values<span class="cmdname"></span>, you get a DBMS error
when you save the table. In DBMSs that allow rollback for Data Definition
Language (DDL), the table definition is rolled back. In DBMSs that
do not allow rollback for DDL, the Database painter is refreshed with
the current definition of the table.</p>
</div>  </li>
<li class="ds"><p>Specify any information required by your DBMS.</p><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Naming a primary key</span> <p class="notepara">Some DBMSs allow you to name a primary key and specify whether
it is clustered or not clustered. For these DBMSs, the Primary Key
property page has a way to specify these properties.</p>
</div><p>For DBMS-specific information, see your DBMS
documentation.</p>  </li>
<li class="ds"><p>Right-click on the Object Details view and select
Save Changes from the pop-up menu. </p><p>Any changes you made in the view are immediately saved to
the table definition. </p>  </li></ol>
</div><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Completing the primary key</span> <p class="notepara">Some DBMSs automatically create a unique index when you define
a primary key so that you can immediately begin to add data to the
table. Others require you to create a unique index separately to
support the primary key before populating the table with data.</p>
<p class="notepara">To find out what your DBMS does, see your
DBMS documentation.</p>
</div></div><a name="bdcbadbb"></a><div class="brhd"><h4>Defining foreign keys</h4>
<p>If your DBMS supports foreign keys, you can define them in PowerBuilder.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To create a foreign key:</p>
<ol type="1"><li class="fi"><p>Do one of the following:<ul><li><p class="firstbullet">Highlight
the table and click the Create Foreign Key drop-down toolbar button
in PainterBar1.</p>
  </li>
<li><p class="ds">Select Object&gt;Insert&gt;Foreign Key
from the main menu or New&gt;Foreign Key from the pop-up menu.</p>  </li>
<li><p class="ds">Expand the table's tree view and right-click
on Foreign Keys and select New Foreign Key from the pop-up menu. </p>  </li>
</ul>
                      </p><p>The Foreign Key properties display in the Object Details view.
Some of the information is DBMS-specific. </p>  </li>
<li class="ds"><p>Name the foreign key in the Foreign Key Name box.</p>  </li>
<li class="ds"><p>Select the columns for the foreign key.</p>  </li>
<li class="ds"><p>On the Primary Key tab page, select the table
and column containing the Primary key referenced by the foreign
key you are defining.</p><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Key definitions must match exactly</span> <p class="notepara">The definition of the foreign key columns must match the primary
key columns, including datatype, precision (width), and scale (decimal specification).</p>
</div>  </li>
<li class="ds"><p>On the Rules tab page, specify any information
required by your DBMS.</p><p>For example, you might need to specify a delete rule by selecting
one of the rules listed for On Delete of Primary Table Row.</p><p>For DBMS-specific information, see your DBMS
documentation.</p>  </li>
<li class="ds"><p>Right-click on the Object Details view and select
Save Changes from the pop-up menu. </p><p>Any changes you make in the view are immediately saved to
the table definition. </p>  </li></ol>
</div></div><a name="bdceigab"></a><div class="brhd"><h4>Modifying keys</h4>
<p>You can modify a primary key in PowerBuilder.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To modify a primary key:</p>
<ol type="1"><li class="fi"><p>Do one of the following:<ul><li><p class="firstbullet">Highlight
the primary key listed in the table's expanded tree view
and click the Properties button.</p>
  </li>
<li><p class="ds">Select Properties from the Object or pop-up menu.</p>  </li>
<li><p class="ds">Drag the primary key icon and drop it in the Object
Details view. </p>  </li>
</ul>
                      </p>  </li>
<li class="ds"><p>Select one or more columns for the primary key.</p>  </li>
<li class="ds"><p>Right-click on the Object Details view and select
Save Changes from the pop-up menu. </p><p>Any changes you make in the view are immediately saved to
the table definition. </p>  </li></ol>
</div></div><a name="bdcefadf"></a><div class="brhd"><h4>Dropping a key</h4>
<p>You can drop keys (remove them from the database) from within PowerBuilder.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To drop a key:</p>
<ol type="1"><li class="fi"><p>Highlight the key in the expanded tree
view for the table in the Objects view or right-click the key icon
for the table in the Object Layout view.</p>  </li>
<li class="ds"><p>Select Drop Primary Key or Drop Foreign Key from
the key's pop-up menu.</p>  </li>
<li class="ds"><p>Click Yes.</p>  </li></ol>
</div></div>
</body>
</html>

