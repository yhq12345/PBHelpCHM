<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Errors in property and data expressions and the Error event</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="dwprgugp45.htm" title="Previous"><img src="../image/arrow-left.png" alt="dwprgugp45.htm"/></a></span><span class="nextbtn"><a href="dwprgugp47.htm" title="Next"><img src="../image/arrow-right.png" alt="dwprgugp47.htm"/></a></span></p><a name="dwprgug-dwobj-errorprop"></a><h2>Errors in property and data expressions and the Error event</h2>
<p>A DataWindow control's Error event is triggered whenever
an error occurs in a data or property expression at execution time.
These expressions that refer to data and properties of a DataWindow
object might be valid under some execution-time conditions but not
others. The Error event allows you to respond with error recovery
logic when an expression is not valid.</p>
<a name="TI147"></a><div class="brhd"><h4>PowerBuilder syntax checking</h4>
<p>In PowerBuilder, when you use a data or property expression,
the PowerScript compiler checks the syntax only as far as the Object
property. Everything following the Object property is evaluated
at execution time. For example, in the following expression, the
column name emp_name and the property Visible are not checked
until execution time:</p><pre class="userinput">dw_1.Object.emp_name.Visible = "0"</pre>
</div><p>If the emp_name column did not exist in the DataWindow,
or if you had misspelled the property name, the compiler would not
detect the error. However, at execution time, PowerBuilder would
trigger the DataWindow control's Error event.</p>
<a name="TI148"></a><div class="brhd"><h4>Using a Try-Catch block</h4>
<p>The Error event is triggered even if you have surrounded an
error-producing data or property expression in a Try-Catch block.
The catch statement is executed after the Error event is triggered,
but only if you do not code the Error event or do not change the
default Error event action from ExceptionFail!. The following example
shows a property expression in a Try-Catch block:</p><pre class="userinput">TRY<br/>&#160;&#160;&#160;dw_1.Object.emp_name.Visible = "0"<br/>CATCH (dwruntimeerror dw_e)<br/>&#160;&#160;&#160;MessageBox ("DWRuntimeError", dw_e.text)<br/>END TRY</pre>
</div><a name="TI149"></a><div class="brhd"><h4>Determining the cause of the error</h4>
<p>The Error event has several arguments that provide information
about the error condition. You can check the values of the arguments
to determine the cause of the error. For example, you can obtain
the internal error number and error text, the name of the object
whose script caused the error, and the full text of the script where
the error occurred. The information provided by the Error event's arguments
can be helpful in debugging expressions that are not checked by
the compiler.</p>
<p>If you catch a DWRuntimeError error, you can use the properties
of that class instead of the Error event arguments to provide information
about the error condition. The following table displays the correspondences
between the Error event arguments and the DWRuntimeError properties.</p>
<a name="TI150"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 2-6: Correspondence between Error event arguments and DWRuntimeError
properties</caption>
<tr><th rowspan="1"><p class="firstpara">Error
event argument</p>
</th>
<th rowspan="1"><p class="firstpara">DWRuntimeError
property</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">errornumber </p>
</td>
<td rowspan="1"><p class="firstpara">number</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">errorline</p>
</td>
<td rowspan="1"><p class="firstpara">line</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">errortext</p>
</td>
<td rowspan="1"><p class="firstpara">text</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">errorwindowmenu</p>
</td>
<td rowspan="1"><p class="firstpara">objectname</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">errorobject</p>
</td>
<td rowspan="1"><p class="firstpara">class</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">errorscript</p>
</td>
<td rowspan="1"><p class="firstpara">routinename</p>
</td>
</tr>
</table>
</div><a name="TI151"></a><div class="brhd"><h4>Controlling the outcome of the event</h4>
<p>When the Error event is triggered, you can have the application
ignore the error and continue processing, substitute a different
return value, or escalate the error by triggering the SystemError
event. In the Error event, you can set two arguments passed by reference
to control the outcome of the event. </p>
</div><a name="TI152"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 2-7: Setting arguments in the Error event</caption>
<tr><th rowspan="1"><p class="firstpara">Argument</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Action</p>
</td>
<td rowspan="1"><p class="firstpara">A value you specify to control the application's
course of action as a result of the error. Values are:</p>
<ul class="simplelist"><li><p class="firstsimple">ExceptionIgnore!</p>
  </li>
<li><p class="firstbullet">ExceptionSubstituteReturnValue!</p>
  </li>
<li><p class="ds">ExceptionFail! (default action)</p>  </li>
</ul>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">ReturnValue</p>
</td>
<td rowspan="1"><p class="firstpara">A value whose datatype matches the expected
value that the DataWindow would have returned. This value is used
when the value of action is ExceptionSubstituteReturnValue!.</p>
</td>
</tr>
</table>
<p>For a complete description of the arguments
of the Error event, see the <span class="doctitle">DataWindow Reference</span>.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">When to substitute a return value</span> <p class="notepara">The ExceptionSubstituteReturnValue! action allows you to substitute
a return value when the last element of an expression causes an
error. Do not use ExceptionSubstituteReturnValue! to substitute
a return value when an element in the middle of an expression causes
an error.</p>
<p class="notepara">The ExceptionSubstituteReturnValue! action is most useful
for handling errors in data expressions.</p>
</div>
</body>
</html>

