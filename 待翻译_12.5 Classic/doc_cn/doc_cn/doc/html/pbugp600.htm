<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Specifying the WHERE clause for update/delete</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp599.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp599.htm"/></a></span><span class="nextbtn"><a href="pbugp601.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp601.htm"/></a></span></p><a name="bhbhdieg"></a><h1>Specifying the WHERE clause for update/delete</h1>
<p>Sometimes multiple users are accessing the same tables at
the same time. In these situations, you need to decide when to allow
your application to update the database. If you allow your application to
always update the database, it could overwrite changes made by other
users:</p>
<div class="fig"><img src="../image/aacu06.gif" alt="aacu06.gif"/></div>
<p>You can control when updates succeed by specifying which columns PowerBuilder includes
in the <span class="cmdname">WHERE</span> clause in the <span class="cmdname">UPDATE</span> or <span class="cmdname">DELETE</span> statement
used to update the database:</p><pre class="syntax">UPDATE table...<br/>SET <span class="variable">column = newvalue</span><span class="cmdname"><br/>WHERE </span><span class="variable">col1 = value1</span><br/>AND <span class="variable">col2 = value2 ...</span><br/> <br/>DELETE<br/>FROM <span class="variable">table</span><span class="cmdname"><br/>WHERE</span> <span class="variable">col1 = value1</span><br/>AND <span class="variable">col2 = value2 ...</span></pre>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Using timestamps</span> <p class="notepara">Some DBMSs maintain timestamps so you can ensure that users are
working with the most current data. If the <span class="cmdname">SELECT</span> statement
for the DataWindow object contains a timestamp column, PowerBuilder includes
the key column and the timestamp column in the <span class="cmdname">WHERE</span> clause
for an <span class="cmdname">UPDATE</span> or <span class="cmdname">DELETE</span> statement regardless
of which columns you specify in the Where Clause for Update/Delete
box.</p>
<p class="notepara">If the value in the timestamp column changes (possibly due
to another user modifying the row), the update fails.</p>
<p class="notepara">To see whether you can use timestamps with
your DBMS, see <span class="doctitle">Connecting to Your Database</span>.</p>
</div><p>Choose one of the options in <a href="pbugp600.htm#CHDGAGIC">Table 21-1</a> in the Where Clause for Update/Delete box.
The results are illustrated by an example following the table.</p>
<a name="chdgagic"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 21-1: Specifying the WHERE clause for
UPDATE and DELETE </caption>
<tr><th rowspan="1"><p class="firstpara">Option</p>
</th>
<th rowspan="1"><p class="firstpara">Result</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Key Columns</p>
</td>
<td rowspan="1"><p class="firstpara">The <span class="cmdname">WHERE</span> clause includes
the key columns only. These are the columns you specified in the
Unique Key Columns box.</p>
<p>The values in the originally retrieved key columns for the
row are compared against the key columns in the database. No other comparisons
are done. If the key values match, the update succeeds.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Caution</span> <p class="notepara">Be very careful when using this option. If you tell PowerBuilder only
to include the key columns in the <span class="cmdname">WHERE</span> clause
and someone else modified the same row after you retrieved it, their
changes will be overwritten when you update the database (see the
example following this table).</p>
</div><p class="firstpara">Use this option only with a single-user database or if you
are using database locking. In other situations, choose one of the
other two options described in this table.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Key and Updatable Columns</p>
</td>
<td rowspan="1"><p class="firstpara">The <span class="cmdname">WHERE</span> clause includes
all key and updatable columns.</p>
<p>The values in the originally retrieved key columns and the originally
retrieved updatable columns are compared against the values in the
database. If any of the columns have changed in the database since
the row was retrieved, the update fails.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Key and Modified Columns</p>
</td>
<td rowspan="1"><p class="firstpara">The <span class="cmdname">WHERE</span> clause includes
all key and modified columns.</p>
<p>The values in the originally retrieved key columns and the
modified columns are compared against the values in the database.
If any of the columns have changed in the database since the row
was retrieved, the update fails.</p>
</td>
</tr>
</table>
<a name="TI719"></a><div class="brhd"><h4>Example</h4>
<p>Consider this situation: a DataWindow object is updating the <span class="dbobject">Employee</span> table, whose
key is <span class="dbobject">Emp_ID</span>; all columns in the
table are updatable. Suppose the user has changed the salary of
employee 1001 from $50,000 to $65,000. This is what
happens with the different settings for the <span class="cmdname">WHERE</span> clause
columns:</p><ul><li><p class="firstbullet">If you choose Key
Columns for the <span class="cmdname">WHERE</span> clause, the <span class="cmdname">UPDATE</span> statement looks
like this:</p><pre class="userinput">UPDATE Employee<br/>SET Salary = 65000<br/>WHERE Emp_ID = 1001</pre>
<p class="lipara">This statement will succeed <span class="emphasis">regardless of whether
other users have modified the row since your application retrieved
the row</span>. For example, if another user had modified the
salary to $70,000, that change will be overwritten when
your application updates the database.</p>  </li>
<li><p class="ds">If you choose Key and Modified Columns for the <span class="cmdname">WHERE</span> clause,
the <span class="cmdname">UPDATE</span> statement looks like this:</p><pre class="userinput">UPDATE Employee<br/>SET Salary = 65000<br/>WHERE Emp_ID = 1001<span class="emphasis">    <br/>&#160;&#160;&#160;AND Salary = 50000</span></pre><p class="lipara">Here the <span class="cmdname">UPDATE</span> statement is also checking
the original value of the modified column in the <span class="cmdname">WHERE</span> clause.
The statement will fail if another user changed the salary of employee
1001 since your application retrieved the row.</p>  </li>
<li><p class="ds">If you choose Key and Updatable Columns for the <span class="cmdname">WHERE</span> clause,
the <span class="cmdname">UPDATE</span> statement looks like this:</p><pre class="userinput">UPDATE Employee<br/>SET Salary = 65000<br/>WHERE Emp_ID = 1001    <span class="emphasis"><br/>&#160;&#160;&#160;AND Salary = 50000</span><span class="emphasis">    <br/>&#160;&#160;&#160;AND Emp_Fname = original_value</span><span class="emphasis">    <br/>&#160;&#160;&#160;AND Emp_Lname = original_value</span><span class="emphasis">    <br/>&#160;&#160;&#160;AND Status = original_value</span>    <span class="emphasis"><br/>&#160;&#160;&#160;...</span></pre><p class="lipara">Here the <span class="cmdname">UPDATE</span> statement is checking all
updatable columns in the <span class="cmdname">WHERE</span> clause. This statement
will fail if any of the updatable columns for employee 1001 have
been changed since your application retrieved the row.</p>  </li>
</ul>
</div>
</body>
</html>

