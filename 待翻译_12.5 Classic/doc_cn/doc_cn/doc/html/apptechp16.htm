<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>PowerBuilder techniques</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp15.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp15.htm"/></a></span><span class="nextbtn"><a href="apptechp17.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp17.htm"/></a></span></p><a name="x-ref355156086"></a><h1>PowerBuilder techniques</h1>
<p>PowerBuilder provides full support for inheritance, encapsulation,
and polymorphism in both visual and nonvisual objects.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Creating reusable objects</span> <p class="notepara">In most cases, the person developing reusable objects is not
the same person using the objects in applications. This discussion
describes defining and creating reusable objects. It does not address
usage.</p>
</div><a name="TI39"></a><div class="brhd"><h4>Implementing inheritance</h4>
<p>PowerBuilder makes it easy to create descendent objects. You
implement inheritance in PowerBuilder by using a painter to inherit
from a specified ancestor object.</p>
<p>For examples of inheritance in visual objects,
see the <span class="dbobject">w_employee</span> window and <span class="dbobject">u_employee_object</span> in
the Code Examples sample application.</p>
<a name="TI40"></a><div class="formalpara"><p class="title">Example of ancestor service object</p><p class="firstpara">One example of using inheritance in custom class user objects
is creating an ancestor service object that performs basic services
and several descendent service objects. These descendent objects
perform specialized services, as well as having access to the ancestor's services:</p>
</div><a name="TI41"></a><div class="fig"><p class="title">Figure 2-1: Ancestor service object</p>
<div class="fig"><img src="../image/ltoop060.gif" alt="ltoop060.gif"/></div>
</div><a name="TI42"></a><div class="formalpara"><p class="title">Example of virtual function in ancestor object</p><p class="firstpara">Another example of using inheritance in custom class user
objects is creating an ancestor object containing functions for
all platforms and then creating descendent objects that perform
platform-specific functions. In this case, the ancestor object contains a <span class="glossterm">virtual
function</span> (<span class="cmdname">uf_change_dir</span> in
this example) so that developers can create descendent objects using
the ancestor's datatype.</p>
</div><a name="TI43"></a><div class="fig"><p class="title">Figure 2-2: Virtual function in ancestor object</p>
<div class="fig"><img src="../image/ltoop080.gif" alt="ltoop080.gif"/></div>
</div><p>For more on virtual functions, see <a href="apptechp17.htm#X-REF355062155">"Other techniques"</a>.</p>
</div><a name="TI44"></a><div class="brhd"><h4>Implementing encapsulation</h4>
<p>Encapsulation allows you to insulate your object's
data, restricting access by declaring instance variables as private
or protected. You then write object functions to provide selective
access to the instance variables.</p>
<a name="TI45"></a><div class="formalpara"><p class="title">One approach</p><p class="firstpara">One approach to encapsulating processing and data is as follows:</p><ul><li><p class="firstbullet">Define
instance variables as public, private, or protected, depending on
the desired degree of outside access. To ensure complete encapsulation,
define instance variables as either private or protected.</p>
  </li>
<li><p class="ds">Define object functions to perform processing and
provide access to the object's data.</p><a name="TI46"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 2-1: Defining object functions</caption>
<tr><th rowspan="1"><p class="firstpara">To do this</p>
</th>
<th rowspan="1"><p class="firstpara">Provide
this function</p>
</th>
<th rowspan="1"><p class="firstpara">Example</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Perform processing</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_do_<span class="variable">operation</span></span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_do_retrieve</span> (which
retrieves rows from the database)</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Modify instance variables</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_set_<span class="variable">variablename</span></span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_set_style</span> (which
modifies the is_style string variable)</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Read instance variables</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_get_<span class="variable">variablename</span></span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_get_style</span> (which
returns the is_style string variable)</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">(Optional) Read boolean instance variables</p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_is_<span class="variable">variablename</span></span></p>
</td>
<td rowspan="1"><p class="firstpara"><span class="cmdname">uf_is_protected</span> (which
returns the ib_protected boolean variable)</p>
</td>
</tr>
</table>
  </li>
</ul>
</div><a name="TI47"></a><div class="formalpara"><p class="title">Another approach</p><p class="firstpara">Another approach to encapsulating processing and data is to
provide a single entry point, in which the developer specifies the
action to be performed:</p><ul><li><p class="firstbullet">Define <span class="emphasis">instance
variables</span> as private or protected, depending on the desired degree
of outside access</p>
  </li>
<li><p class="ds">Define private or protected <span class="emphasis">object functions</span> to
perform processing</p>  </li>
<li><p class="ds">Define a single <span class="emphasis">public function</span> whose
arguments indicate the type of processing to perform</p><a name="TI48"></a><div class="fig"><p class="title">Figure 2-3: Defining a public function for encapsulation</p>
<div class="fig"><img src="../image/ltoop120.gif" alt="ltoop120.gif"/></div>
</div>  </li>
</ul>
</div><p>For an example, see the <span class="dbobject">uo_sales_order</span> user
object in the Code Examples sample application.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Distributed components</span> <p class="notepara">When you generate an application server component, public
functions are available in the interface of the generated component
and you can choose to make public instance variables available.
Private and protected functions and variables are never exposed
in the interface of the generated component.</p>
<p class="notepara">For more information, see Part 6, "Distributed Application
Techniques."</p>
</div></div><a name="TI49"></a><div class="brhd"><h4>Implementing polymorphism</h4>
<p>Polymorphism refers to a programming language's ability
to process objects differently depending on their datatype or class.
Polymorphism means that functions with the same name behave differently
depending on the referenced object. Although there is some discussion
over an exact definition for polymorphism, many people find it helpful
to think of it as follows:</p>
<a name="TI50"></a><div class="formalpara"><p class="title">Operational polymorphism</p><p class="firstpara">Separate, unrelated objects define functions with the same
name. Each function performs the appropriate processing for its object
type:</p>
</div><a name="TI51"></a><div class="fig"><p class="title">Figure 2-4: Operational polymorphism</p>
<div class="fig"><img src="../image/ltoop020.gif" alt="ltoop020.gif"/></div>
</div><p>For an example, see the <span class="dbobject">u_external_functions</span> user
object and its descendants in the Code Examples sample application.</p>
<a name="TI52"></a><div class="formalpara"><p class="title">Inclusional polymorphism</p><p class="firstpara">Various objects in an inheritance chain define functions with
the same name. </p>
</div><p>With inclusional polymorphism PowerBuilder determines which
version of a function to execute, based on where the current object
fits in the inheritance hierarchy. When the object is a descendant,
PowerBuilder executes the descendent version of the function, overriding
the ancestor version:</p>
<a name="TI53"></a><div class="fig"><p class="title">Figure 2-5: Inclusional polymorphism</p>
<div class="fig"><img src="../image/ltoop040.gif" alt="ltoop040.gif"/></div>
</div><p>For an example, see the <span class="dbobject">u_employee_object</span> user
object in the Code Examples sample application.</p>
</div>
</body>
</html>

