<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title></title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="psrefp975.htm" title="Previous"><img src="../image/arrow-left.png" alt="psrefp975.htm"/></a></span><span class="nextbtn"><a href="psrefp977.htm" title="Next"><img src="../image/arrow-right.png" alt="psrefp977.htm"/></a></span></p><a name="ccjccjdi"></a><h2>Syntax 1&#160;For formatting data</h2><h4>Description</h4><p>Formats data, such as time or date values, according to a
format mask. You can convert and format date, DateTime, numeric,
and time data. You can also apply a display format to a string.</p>

<h4>Syntax</h4><pre class="syntax"><span class="cmdname">String</span> ( <span class="variable">data</span>, { <span class="variable">format</span> } )</pre><a name="TI2370"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Argument</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">data</span></p>
</td>
<td rowspan="1"><p class="firstpara">The data you want returned as a string
with the specified formatting. <span class="variable">Data</span> can have
a date, DateTime, numeric, time, or string datatype. <span class="variable">Data</span> can
also be an Any variable containing one of these datatypes.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara"><span class="variable">format</span> (optional)</p>
</td>
<td rowspan="1"><p class="firstpara">A string whose value is the display masks
you want to use to format the data. The masks consists of formatting
information specific to the datatype of <span class="variable">data</span>.
If <span class="variable">data</span> is type string, <span class="variable">format</span> is
required.</p>
<p>The format can consist of more than one mask, depending on
the datatype of <span class="variable">data</span>. Each mask is separated
by a semicolon. (For details on each datatype, see Usage).</p>
</td>
</tr>
</table>

<h4>Return Values</h4><p><span class="dt">String</span>. Returns <span class="variable">data</span> in
the specified format if it succeeds and the empty string ("") if
the datatype of <span class="variable">data</span> does not match the type
of display mask specified, <span class="variable">format</span> is not a
valid mask, or <span class="variable">data</span> is an incompatible datatype.</p>

<h4>Usage</h4><p>For date, DateTime, numeric, and time data, PowerBuilder uses
the system's default format for the returned string if
you do not specify a format. For numeric data, the default format
is the <span class="cmdname">[General]</span> format.</p>
<p>For string data, a display format mask is required. (Otherwise,
the function would have nothing to do.)</p>
<p>The format can consist of one or more masks:</p><ul><li><p class="firstbullet">Formats for date, DateTime,
string, and time data can include one or two masks. The first mask
is the format for the data; the second mask is the format for a
null value.</p>
  </li>
<li><p class="ds">Formats for numeric data can have up to four masks.
A format with a single mask handles both positive and negative data.
If there are additional masks, the first mask is for positive values,
and the additional masks are for negative, zero, and <span class="cmdname">null</span> values.</p>  </li>
</ul>
<p>To display additional characters as part of
the mask for a decimal value, you must precede each character with
a backslash. For example, to display a decimal number with two digits
of precision preceded by four asterisks, you must type a backslash
before each asterisk:</p><pre class="userinput">dec{2} amount<br/>string = ls_result<br/>amount = 123456.32<br/>ls_result = string(amount,"\*\*\*\*0.00")</pre>
<p>The resulting string is <code class="ce">****123456.32</code>.</p>
<p>For more information on specifying display
formats, see the <span class="doctitle">PowerBuilder Users Guide</span>.
Note that, although a format can include color specifications, the colors
are ignored when you use <span class="cmdname">String</span> in PowerScript.
Colors appear only for display formats specified in the DataWindow
painter.</p>
<p>If the display format does not match the datatype, PowerBuilder
tries to apply the mask, which can produce unpredictable results.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Times and dates from a DataWindow control</span> <p class="notepara">When you call <span class="cmdname">GetItemTime</span> or <span class="cmdname">GetItemString</span> as
an argument for the <span class="cmdname">String</span> function and do not
specify a display format, the value is formatted as a DateTime value.
This statement returns a string like "2/26/03
00:00:00":</p><pre class="userinput">String(dw_1.GetItemTime(1, "start_date"))</pre>
</div><a name="TI2371"></a><h4>International deployment</h4>
<p>When you use <span class="cmdname">String</span> to format a date and
the month is displayed as text (for example, the display format
includes "mmm"), the month is in the language of the runtime DLLs
available when the application is run. If you have installed localized
runtime files in the development environment or on a user's
machine, then on that machine, the month in the resulting string
is in the language of the localized files.</p>
<p>For information about the localized runtime
files, which are available in French, German, Italian, Spanish,
Dutch, Danish, Norwegian, and Swedish, see the chapter on internationalization
in <span class="doctitle">Application Techniques</span>.</p>
<a name="TI2372"></a><h4>Handling ANSI data</h4>
<p>Since this function does not have an encoding argument to
allow you to specify the encoding of the data, the string returned
can contain garbage characters if the data has ANSI encoding. You
can handle this by converting the ANSI string returned from the <span class="cmdname">String</span> function
to a Unicode blob, and then converting the ANSI string in the blob
to a Unicode string, using the encoding parameters provided in the <span class="cmdname">Blob</span> and <span class="cmdname">String</span> conversion
functions:</p><pre class="userinput">ls_temp = String(long, "address" )<br/>lb_blob = blob(ls_temp) //EncodingUTF16LE! is default<br/>ls_result = string(lb_blob, EncodingANSI!)</pre>
<a name="TI2373"></a><h4>Message object</h4>
<p>You can also use <span class="cmdname">String</span> to extract a string
from the Message object after calling <span class="cmdname">TriggerEvent</span> or <span class="cmdname">PostEvent</span>.
For more information, see the <span class="cmdname">TriggerEvent</span> or <span class="cmdname">PostEvent</span> functions.</p>

<a name="EX0"></a><h4>Examples</h4><div class="example"><p>This statement applies a display format to a date
value and returns <code class="ce">Jan 31, 2002</code>:</p><pre class="userinput"><span class="emphasis">String</span>(2002-01-31, "mmm dd, yyyy")</pre>
</div><div class="example"><p>This example applies a format to the value in <span class="variable">order_date</span> and
sets <span class="variable">date1</span> to <code class="ce">6&#8211;11&#8211;02</code>:</p><pre class="userinput">Date order_date = 2002-06-11</pre><pre class="userinput">string date1</pre><pre class="userinput">date1 = <span class="emphasis">String</span>(order_date,"m-d-yy")</pre>
</div><div class="example"><p>This example includes a format for a <span class="cmdname">null</span> date
value so that when <span class="variable">order_date</span> is <span class="cmdname">null</span>, <span class="variable">date1</span> is
set to <code class="ce">none</code>:</p><pre class="userinput">Date order_date = 2002-06-11</pre><pre class="userinput">string date1</pre><pre class="userinput">SetNull(order_date)</pre><pre class="userinput">date1 = <span class="emphasis">String</span>(order_date, "m-d-yy;'none'")</pre>
</div><div class="example"><p>This statement applies a format to a DateTime value
and returns <code class="ce">Jan 31, 2001 6 hrs and 8 min</code>:</p><pre class="userinput"><span class="emphasis">String</span>(DateTime(2001-01-31, 06:08:00), &amp;</pre><pre class="userinput">   'mmm dd, yyyy h "hrs and" m "min"')</pre>
</div><div class="example"><p>This example builds a DateTime value from the system
date and time using the <span class="cmdname">Today</span> and <span class="cmdname">Now</span> functions.
The <span class="cmdname">String</span> function applies formatting and sets
the text of <span class="dbobject">sle_date</span> to that value,
for example, <code class="ce">6-11-02 8:06 pm</code>:</p><pre class="userinput">DateTime sys_datetime</pre><pre class="userinput">string datetime1</pre><pre class="userinput">sys_datetime = DateTime(Today(), Now())</pre><pre class="userinput">sle_date.text = <span class="emphasis">String</span>(sys_datetime, &amp;</pre><pre class="userinput">   "m-d-yy h:mm am/pm;'none'")</pre>
</div><div class="example"><p>This statement applies a format to a numeric value
and returns <code class="ce">$5.00</code>:</p><pre class="userinput"><span class="emphasis">String</span>(5,"$#,##0.00")</pre>
</div><div class="example"><p>These statements set <span class="variable">string1</span> to <code class="ce">0123</code>:</p><pre class="userinput">integer nbr = 123</pre><pre class="userinput">string string1</pre><pre class="userinput">string1 = <span class="emphasis">String</span>(nbr,"0000;(000);****;empty")</pre>
</div><div class="example"><p>These statements set <span class="variable">string1</span> to <code class="ce">(123)</code>:</p><pre class="userinput">integer nbr = -123</pre><pre class="userinput">string string1</pre><pre class="userinput">string1 = <span class="emphasis">String</span>(nbr,"000;(000);****;empty")</pre>
</div><div class="example"><p>These statements set <span class="variable">string1</span> to <code class="ce">****</code>:</p><pre class="userinput">integer nbr = 0</pre><pre class="userinput">string string1</pre><pre class="userinput">string1 = <span class="emphasis">String</span>(nbr,"0000;(000);****;empty")</pre>
</div><div class="example"><p>These statements set <span class="variable">string1</span> to <code class="ce">"empty"</code>:</p><pre class="userinput">integer nbr</pre><pre class="userinput">string string1</pre><pre class="userinput">SetNull(nbr)</pre><pre class="userinput">string1 = <span class="emphasis">String</span>(nbr,"0000;(000);****;empty")</pre>
</div><div class="example"><p>This statement formats string data and returns <code class="ce">A-B-C</code>.
The display format assigns a character in the source string to each <code class="ce">@</code> and
inserts other characters in the format at the appropriate positions:</p><pre class="userinput"><span class="emphasis">String</span>("ABC", "@-@-@")</pre>
</div><div class="example"><p>This statement returns A*B:</p><pre class="userinput"><span class="emphasis">String</span>("ABC", "@*@")</pre>
</div><div class="example"><p>This statement returns ABC:</p><pre class="userinput"><span class="emphasis">String</span>("ABC", "@@@")</pre>
</div><div class="example"><p>This statement returns a space:</p><pre class="userinput"><span class="emphasis">String</span>("ABC", " ")</pre>
</div><div class="example"><p>This statement applies a display format to time data
and returns <code class="ce">6 hrs and 8 min</code>:</p><pre class="userinput"><span class="emphasis">String</span>(06:08:02,'h "hrs and" m "min"')</pre>
</div><div class="example"><p>This statement returns <code class="ce">08:06:04
pm</code>:</p><pre class="userinput"><span class="emphasis">String</span>(20:06:04,"hh:mm:ss am/pm")</pre>
</div><div class="example"><p>This statement returns <code class="ce">8:06:04
am</code>:</p><pre class="userinput"><span class="emphasis">String</span>(08:06:04,"h:mm:ss am/pm")</pre>
</div>
<a name="SA0"></a><h4>See Also</h4><ul class="simplelist"><li><p class="li"><span class="cmdname">String</span> <span class="doctitle">method
for DataWindows in the DataWindow Reference or online Help</span></p>
  </li>
</ul>

</body>
</html>

