<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Handling exceptions</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp29.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp29.htm"/></a></span><span class="nextbtn"><a href="apptechp31.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp31.htm"/></a></span></p><a name="babcaahb"></a><h2>Handling exceptions</h2>
<p>Whether an exception is thrown by the runtime system or by
a <span class="cmdname">THROW</span> statement in an application script, you
handle the exception by catching it. This is done by surrounding
the set of application logic that throws the exception with code
that indicates how the exception is to be dealt with.</p>
<a name="TI111"></a><div class="brhd"><h4>TRY-CATCH-FINALLY block</h4>
<p>To handle an exception in PowerScript, you must include some
set of your application logic inside a try-catch block. A try-catch
block begins with a <span class="cmdname">TRY</span> clause and ends with
the <span class="cmdname">END TRY</span> statement. It must also contain either
a <span class="cmdname">CATCH</span> clause or a <span class="cmdname">FINALLY</span> clause.
A try-catch block normally contains a <span class="cmdname">FINALLY</span> clause
for error condition cleanup. In between the <span class="cmdname">TRY</span> and <span class="cmdname">FINALLY</span> clauses
you can add any number of <span class="cmdname">CATCH</span> clauses. </p>
<p><span class="cmdname">CATCH</span> clauses are not obligatory, but if
you do include them you must follow each <span class="cmdname">CATCH</span> statement
with a variable declaration. In addition to following all of the
usual rules for local variable declarations inside a script, the
variable being defined must derive from the Throwable system type.</p>
<p>You can add a <span class="cmdname">TRY-CATCH-FINALLY</span>, <span class="cmdname">TRY-CATCH</span>,
or <span class="cmdname">TRY-FINALLY</span> block using the Script view Paste
Special feature for PowerScript statements.     If you select
the Statement Templates check box on the AutoScript tab of the Design Options
dialog box, you can also use the AutoScript feature to insert these block
structures.</p>
</div><a name="TI112"></a><div class="brhd"><h4>Example</h4>
<a name="TI113"></a><div class="formalpara"><p class="title">Example catching a system error</p><p class="firstpara">This is an example of a <span class="cmdname">TRY-CATCH-FINALLY</span> block
that catches a system error when an <span class="cmdname">arccosine</span> argument,
entered by the application user (in a SingleLineEdit) is not in
the required range. If you do not catch this error, the application
goes to the system error event, and eventually terminates: </p>
</div><pre class="userinput">Double ld_num<br/>ld_num = Double (sle_1.text)<br/>TRY<br/>&#160;&#160;&#160;sle_2.text = string (acos (ld_num))<br/>CATCH (runtimeerror er)&#160;&#160;&#160;<br/>&#160;&#160;&#160;MessageBox("Runtime Error", er.GetMessage())<br/>FINALLY&#160;&#160;&#160;<br/>&#160;&#160;&#160;// Add cleanup code here&#160;&#160;&#160;<br/>&#160;&#160;&#160;of_cleanup()&#160;&#160;&#160;<br/>&#160;&#160;&#160;Return<br/>END TRY&#160;&#160;&#160;<br/>MessageBox("After", "We are finished.")</pre>
<p>The system runtime error message might be confusing to the
end user, so for production purposes, it would be better to catch
a user-defined exception&#8212;see the example in <a href="apptechp31.htm#BABCABDH">"Creating user-defined exception
types"</a>&#8212;and
set the message to something more understandable.</p>
<p>The <span class="cmdname">TRY</span> reserved word signals the start
of a block of statements to be executed and can include more than
one <span class="cmdname">CATCH</span> clause. If the execution of code in
the <span class="cmdname">TRY</span> block causes an exception to be thrown,
then the exception is handled by the first <span class="cmdname">CATCH</span> clause
whose variable can be assigned the value of the exception thrown.
The variable declaration after a <span class="cmdname">CATCH</span> statement
indicates the type of exception being handled (a system runtime
error, in this case).</p>
</div><a name="TI114"></a><div class="brhd"><h4>CATCH order</h4>
<p>It is important to order your <span class="cmdname">CATCH</span> clauses
in such a way that one clause does not hide another. This would
occur if the first <span class="cmdname">CATCH</span> clause catches an exception
of type Exception and a subsequent <span class="cmdname">CATCH</span> clause
catches a descendant of Exception. Since they are processed in order,
any exception thrown that is a descendant of Exception would be
handled by the first <span class="cmdname">CATCH</span> clause and never by
the second. The PowerScript compiler can detect this condition and
signals an error if found.</p>
<p>If an exception is not dealt with in any of the <span class="cmdname">CATCH</span> clauses,
it is thrown up the call stack for handling by other exception handlers
(nested try-catch blocks) or by the system error event. But before
the exception is thrown up the stack, the <span class="cmdname">FINALLY</span> clause
is executed.</p>
</div><a name="TI115"></a><div class="brhd"><h4>FINALLY clause</h4>
<p>The <span class="cmdname">FINALLY</span> clause is generally used to
clean up after execution of a <span class="cmdname">TRY</span> or <span class="cmdname">CATCH</span> clause.
The code in the <span class="cmdname">FINALLY</span> clause is guaranteed
to execute if any portion of the try-catch block is executed, regardless
of how the code in the try&#8211;catch block completes. </p>
<p>If no exceptions occur, the <span class="cmdname">TRY</span> clause
completes, followed by the execution of the statements contained
in the <span class="cmdname">FINALLY</span> clause. Then execution continues
on the line following the <span class="cmdname">END TRY</span> statement.</p>
<p>In cases where there are no <span class="cmdname">CATCH</span> clauses
but only a <span class="cmdname">FINALLY</span> clause, the code in the <span class="cmdname">FINALLY</span> clause
is executed even if a return is encountered or an exception is thrown
in the <span class="cmdname">TRY</span> clause. </p>
<p>If an exception occurs within the context of the <span class="cmdname">TRY</span> clause
and an applicable <span class="cmdname">CATCH</span> clause exists, the <span class="cmdname">CATCH</span> clause
is executed, followed by the <span class="cmdname">FINALLY</span> clause.
But even if no <span class="cmdname">CATCH</span> clause is applicable to
the exception thrown, the <span class="cmdname">FINALLY</span> clause still
executes before the exception is thrown up the call stack.</p>
<p>If an exception or a return is encountered within a <span class="cmdname">CATCH</span> clause,
the <span class="cmdname">FINALLY</span> clause is executed before execution
is transferred to the new location.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">FINALLY clause restriction</span> <p class="notepara">Do not use <span class="cmdname">RETURN</span> statements in the <span class="cmdname">FINALLY</span> clause
of a <span class="cmdname">TRY</span>-<span class="cmdname">CATCH</span> block. This
can prevent the exception from being caught by its invoker.</p>
</div></div>
</body>
</html>

