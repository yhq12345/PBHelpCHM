<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Using connection caching</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="apptechp304.htm" title="Previous"><img src="../image/arrow-left.png" alt="apptechp304.htm"/></a></span><span class="nextbtn"><a href="apptechp306.htm" title="Next"><img src="../image/arrow-right.png" alt="apptechp306.htm"/></a></span></p><a name="ccjbehag"></a><h2>Using connection caching</h2>
<a name="TI830"></a><div class="brhd"><h4>Benefits of connection caching</h4>
<p>To optimize database processing, <abbr title="e a server">EAServer</abbr> provides
support for connection caching. Connection caching allows <abbr title="e a server">EAServer</abbr> components to share pools
of preallocated connections to a remote database server, avoiding
the overhead imposed when each instance of a component creates a
separate connection. By establishing a connection cache, a server
can reuse connections made to the same data source. Connection caches are
called data sources in <abbr title="e a server">EAServer</abbr> 6.x.</p>
</div><a name="TI831"></a><div class="brhd"><h4>How it works</h4>
<p>Ordinarily, when a PowerBuilder application connects to a
database, PowerBuilder physically terminates each database connection
for which a <span class="cmdname">DISCONNECT</span> statement is issued. By
contrast, when a PowerBuilder component uses an <abbr title="e a server">EAServer</abbr> connection cache, <abbr title="e a server">EAServer</abbr> logically terminates the
database connection but does not physically remove the connection. Instead,
the database connection is kept open in the connection cache so
that it can be reused for other database operations.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Do not disconnect in destructor event</span> <p class="notepara"><abbr title="e a server">EAServer</abbr> releases all connection
handles to the cache when a transaction is completed or when the
component is deactivated. If you place a <span class="cmdname">DISCONNECT</span> statement
in the destructor event, which is triggered after the deactivate
event, the connection has already been logically terminated and
the <span class="cmdname">DISCONNECT</span> causes a physical termination. <span class="cmdname">DISCONNECT</span> statements
can be placed in the deactivate event.</p>
</div><p>All connections in a cache must share a common user name,
password, server name, and connectivity library.</p>
</div><a name="TI832"></a><div class="brhd"><h4>Accessing a cache by user</h4>
<p>If you want to retrieve a connection from the cache that uses
a specified set of user name, password, server, and connectivity
library values, you do not need to modify your database access code
to enable it to use the cache. You simply need to create a new cache
in <abbr title="e a server">EAServer</abbr> Manager that has the
database connection properties (user name, password, server name,
and connectivity library) required by the component. In <abbr title="e a server">EAServer</abbr> 6.x, you create a data
source (cache) by selecting Resources&gt;Data Sources&gt;Add
in the Management Console. At runtime, when the component tries
to connect to the database, <abbr title="e a server">EAServer</abbr> automatically
returns a connection from the cache that matches the connection
values requested by the component. </p>
</div><a name="TI833"></a><div class="brhd"><h4>Accessing a cache by name</h4>
<p>If you want to retrieve a connection from a cache by specifying
the cache name, set the CacheName DBParm to identify the cache you
want to use. Accessing a cache by name allows you to change the
user name, password, or server in the Management Console without
requiring corresponding changes to your component source code.</p>
<p>This code for a PowerBuilder component shows how to access
a cache by name:</p><pre class="userinput">SQLCA.DBMS = "ODBC"<br/>SQLCA.Database = "EAS Demo DB"<br/>SQLCA.AutoCommit = FALSE<br/>SQLCA.DBParm = "ConnectString='DSN=EAS Demo DB;<br/>&#160;&#160;&#160;UID=dba;PWD=sql',CacheName='mycache'"</pre>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Cache names are case-sensitive</span> <p class="notepara">Cache names are case-sensitive; therefore, make sure the case
of the cache name you specify in your script matches the case used
for the name in <abbr title="e a server">EAServer</abbr>.</p>
</div></div><a name="TI834"></a><div class="brhd"><h4>Retrieving a connection by proxy</h4>
<p>Regardless of whether you access a cache by user or name,
you can retrieve a connection by proxy. Retrieving a connection
by proxy means that you can assume the identity and privileges of
another user by providing an alternative login name.</p>
<p>This feature can be used with any database that recognizes
the SQL command <span class="glossterm">set session authorization</span>.
In order for user A to use the ProxyUserName DBParm to assume the
identity of another user B, user A must have permission to execute
this statement. For example, for SQL Anywhere, user A must have DBA
authority, and for ASE, user A must have been granted permission
to execute <span class="cmdname">set session authorization</span> by a System
Security Officer.</p>
<p>For more information about the PowerBuilder database interfaces
that support proxy connections, see <span class="doctitle">Connecting to Your
Database</span>. </p>
<p>To use proxy connections, set the <span class="glossterm">ProxyUserName</span> DBParm
to identify the alternative login name. This example shows how to
retrieve a connection by proxy:</p><pre class="userinput">SQLCA.DBMS = "ODBC"<br/>SQLCA.DBParm = "CacheName='MyEAServerCache',<br/>&#160;&#160;&#160;UseContextObject='Yes',ProxyUserName='pikachu'"</pre>
</div><a name="TI835"></a><div class="brhd"><h4>Before you can use a connection by proxy </h4>
<p>Set-proxy support must be enabled in the cache properties
file before components can take advantage of it. In <abbr title="e a server">EAServer</abbr> 5.x, <abbr title="e a server">EAServer</abbr> Manager does
not automatically create an individual cache properties file when
you create a cache, so you must create this file manually. Name
the file <span class="filepath">cachename.props</span> and put it in the <span class="filepath">EAServer\Repository\ConnCache</span> directory. Once
you have created the cache properties file, add the following line: </p><pre class="userinput">com.sybase.jaguar.conncache.ssa=true</pre>
<p>For this setting to take effect, you must refresh <abbr title="e a server">EAServer</abbr>. </p>
<p>In <abbr title="e a server">EAServer</abbr> 6.x, you create
a data source by selecting Resources&gt;Data Sources&gt;Add
in the Management Console. Select Set Session Authorization and
specify a name in the Set Session Authorization System ID box. The properties
file for the data source is stored in the Repository in the <span class="filepath">Instance\com\sybase\djc\sql\DataSource</span> directory.</p>
<p>For more information on managing connection caches (or data
sources), see the <abbr title="e a server">EAServer</abbr> documentation<span class="doctitle"></span>. </p>
<p>You must also set up your database server to recognize and
give privileges to the alternative login name defined in the ProxyUserName
DBParm. </p>
</div><a name="TI836"></a><div class="brhd"><h4>What happens when all connections are in use</h4>
<p>You can control what happens if all connections in a cache
are in use. To do this, set the <span class="glossterm">GetConnectionOption</span> DBParm
to one of the following values:</p>
<a name="TI837"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Value</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">JAG_CM_NOWAIT</p>
</td>
<td rowspan="1"><p class="firstpara">Causes the attempt to connect to fail
with an error if no connection can be returned.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">JAG_CM_WAIT</p>
</td>
<td rowspan="1"><p class="firstpara">Causes the component to wait until a
connection becomes available.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">JAG_CM_FORCE</p>
</td>
<td rowspan="1"><p class="firstpara">Allocates and opens a new connection.
The new connection is not cached and is destroyed when it is no longer
needed.</p>
</td>
</tr>
</table>
<p>By default, PowerBuilder uses JAG_CM_FORCE.</p>
</div><a name="TI838"></a><div class="brhd"><h4>What happens when a connection is released</h4>
<p>You can also control what happens when a connection is released.
To do this, set the <span class="glossterm">ReleaseConnectionOption</span> DBParm
to one of the following values:</p>
<a name="TI839"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><tr><th rowspan="1"><p class="firstpara">Value</p>
</th>
<th rowspan="1"><p class="firstpara">Description</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">JAG_CM_DROP</p>
</td>
<td rowspan="1"><p class="firstpara">Closes and deallocates the connection.
If the connection came from a cache, a new connection is created
in its place. Use JAG_CM_DROP to destroy a connection
when errors have made it unusable.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">JAG_CM_UNUSED</p>
</td>
<td rowspan="1"><p class="firstpara">If the connection was taken from a cache,
it is placed back in the cache. A connection created outside of
a cache is closed and destroyed.</p>
</td>
</tr>
</table>
<p>By default, PowerBuilder uses JAG_CM_UNUSED. </p>
</div><a name="chddiehc"></a><div class="brhd"><h4>EAServer connection caches
for Unicode support</h4>
<p>The following <abbr title="e a server">EAServer</abbr> native
connection caches support Unicode connections for PowerBuilder components.</p>
<p>For <abbr title="e a server">EAServer</abbr> 5.x:</p><ul><li><p class="firstbullet">OCI_9U &#8211; Oracle9<span class="emphasis">i</span> Unicode
Cache</p>
  </li>
<li><p class="ds">OCI_10U &#8211; Oracle 10<span class="emphasis">g</span> Unicode
Cache</p>  </li>
<li><p class="ds">ODBCU &#8211; ODBC Unicode Cache</p>  </li>
</ul>
<p>For <abbr title="e a server">EAServer</abbr> 6.x:</p><ul><li><p class="firstbullet">JCM_Oracle_Unicode &#8211; Oracle9<span class="emphasis">i</span> or
10<span class="emphasis">g</span> Unicode Cache</p>
  </li>
<li><p class="ds">JCM_Odbc_Unicode &#8211; ODBC
Unicode Cache</p>  </li>
</ul>
<p>These connection cache types accept Unicode connection parameters
and then send a request to the database driver to open a Unicode
connection to the database. With a Unicode connection, PowerBuilder
components can communicate with the database using Unicode.</p>
<p>If you are using the Oracle9<span class="emphasis">i</span> native
interface (O90) to access an Oracle9<span class="emphasis">i</span> database
in a PowerBuilder component in <abbr title="e a server">EAServer</abbr> 5.x,
use the database driver type OCI_9U for the connection
cache. If you do not, access will fail.</p>
<p>For an ODBC connection cache in <abbr title="e a server">EAServer</abbr> 5.x,
use the database driver type ODBCU to access multiple-language data
in a SQL Anywhere Unicode database or DBCS data in a SQL Anywhere DBCS
database and set the database parameter ODBCU_CONLIB to&#160;1.
For example:</p><pre class="userinput">SQLCA.DBParm = "CacheName='EASDemo_u',<br/>&#160;&#160;&#160;UseContextObject='Yes',ODBCU_CONLIB=1"</pre>
</div>
</body>
</html>

