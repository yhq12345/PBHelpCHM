<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Getting and storing the data from a DataWindow data expression</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="dwrefp420.htm" title="Previous"><img src="../image/arrow-left.png" alt="dwrefp420.htm"/></a></span><span class="nextbtn"><a href="dwrefp422.htm" title="Next"><img src="../image/arrow-right.png" alt="dwrefp422.htm"/></a></span></p><a name="bceeeiaj"></a><h3>Getting and storing the data from a DataWindow data expression</h3>
<p>A DataWindow data expression can return a large amount of
data.</p>
<a name="TI1062"></a><div class="brhd"><h4>Data structures for data</h4>
<a name="TI1063"></a><div class="formalpara"><p class="title">Single row and column</p><p class="firstpara">When your data expression refers to a single row and column,
you can assign the data to a variable whose data matches the column's
datatype. When the expression refers to a single column but can
refer to multiple rows, you must specify an array of the appropriate
datatype.</p>
</div></div><a name="TI1064"></a><div class="formalpara"><p class="title">More than one column</p><p class="firstpara">When the expression refers to more than one column, you can
get or set the data with a structure or user object. When you create
the definition, you must assign datatypes to the fields (in a structure)
or instance variables (in a user object) that match the datatypes
of the columns. When your expression refers to multiple rows, you
get an array of the structure or user object.</p>
</div><p>Likewise, if you want to set data in the DataWindow control,
you will set up the data in structures or user objects whose elements
match the columns referred to in the expression. An array of those
structures or user objects will provide data for multiple rows.</p>
<a name="TI1065"></a><div class="formalpara"><p class="title">Datatypes</p><p class="firstpara">For matching purposes, the datatypes should be appropriate
to the data&#8212;for example, any numeric datatype matches any
other numeric type.</p>
</div><a name="TI1066"></a><div class="brhd"><h4>Examples of data structures</h4>
<p>The following table presents some examples of data specified
by an expression and the type of data structures you might define
for storing the data:</p>
</div><a name="TI1067"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 4-2: Types of storage for data specified by an expression</caption>
<tr><th rowspan="1"><p class="firstpara">Type of selection</p>
</th>
<th rowspan="1"><p class="firstpara">Sample data storage</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">A single item</p>
</td>
<td rowspan="1"><p class="firstpara">A single variable of the appropriate
datatype.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">A column of values</p>
</td>
<td rowspan="1"><p class="firstpara">An array of the appropriate datatype.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">A row</p>
</td>
<td rowspan="1"><p class="firstpara">A structure whose elements have datatypes
that match the DataWindow object's columns.</p>
<p>A user object whose instance variables match the DataWindow
object's columns.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Selected rows or all rows</p>
</td>
<td rowspan="1"><p class="firstpara">An array of the structure or user object
defined for a row.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">A block of values</p>
</td>
<td rowspan="1"><p class="firstpara">An array of structures or user objects
whose elements or instance variables match the columns included
in the selected range.</p>
</td>
</tr>
</table>
<a name="TI1068"></a><div class="brhd"><h4>Assigning data to arrays</h4>
<p>When a data expression is assigned to an array, values are
assigned beginning with array element 1 regardless of the starting
row number. If the array is larger than the number of rows accessed,
elements beyond that number are unchanged. If it is smaller, a variable-size
array will grow to hold the new values. However, a fixed-size array
that is too small for the number of rows will cause an execution
error.</p>
</div><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Two ways to instantiate user objects</span> <p class="notepara"> A user object needs to be instantiated before it is used.</p>
<p class="notepara">One way is to use the <span class="cmdname">CREATE</span> statement
after you declare the user object. If you declare an array of the
user object, you must use <span class="cmdname">CREATE</span> for each array element.</p>
<p class="notepara">The second way is to select the Autoinstantiate box for the
user object in the User Object painter. When you declare the user
object in a script, the user object will be automatically instantiated,
like a structure. </p>
</div><a name="TI1069"></a><div class="brhd"><h4>Any datatype and data expressions</h4>
<p>The actual datatype of a DataWindow data expression is Any,
which allows the compiler to process the expression even though
the final datatype is unknown. When data is accessed at runtime,
you can assign the result to another Any variable or to a variable,
structure, or user object whose datatype matches the real data.</p>
</div><a name="TI1070"></a><div class="brhd"><h4>Examples</h4>
<a name="TI1071"></a><div class="formalpara"><p class="title">A single value</p><p class="firstpara">This example gets a value from column 2, whose datatype is string:</p>
</div></div><pre class="userinput">string ls_name</pre><pre class="userinput">ls_name = dw_1.Object.Data[1,2]</pre>
<a name="TI1072"></a><div class="formalpara"><p class="title">A structure that matches DataWindow columns</p><p class="firstpara">In this example, a DataWindow object has four columns: </p><ul class="simplelist"><li><p class="firstsimple">An ID (number)</p>
  </li>
<li><p class="ds">A name (string)</p>  </li>
<li><p class="ds">A retired status (boolean)</p>  </li>
<li><p class="ds">A birth date (date)</p>  </li>
</ul>
</div><p>A structure to hold these values has been defined in the Structure
painter. It is named str_empdata and has four elements
whose datatypes are integer, string, boolean, and date. To store
the values of an expression that accesses some or all the rows,
you need an array of str_empdata structures to hold the
data:</p>
<pre class="userinput">str_empdata lstr_currdata[]</pre><pre class="userinput">lstr_currdata = dw_1.Object.Data</pre>
<p>After this example executes, the upper bound of the array
of structures, which is variable-size, is equal to the number of
rows in the DataWindow control.</p>
<a name="TI1073"></a><div class="formalpara"><p class="title">A user object that matches DataWindow columns</p><p class="firstpara">If the preceding example involved a user object instead of
a structure, then a user object defined in the User Object painter,
called uo_empdata, would have four instance variables,
defined in the same order as the DataWindow columns:</p>
</div><pre class="userinput">integer id</pre><pre class="userinput">string name</pre><pre class="userinput">boolean retired</pre><pre class="userinput">date birthdate</pre>
<p>Before accessing three rows, three array elements of the user
object have been created (you could use a <span class="cmdname">FOR NEXT</span> loop
for this). The user object was not defined with Autoinstantiate
enabled:</p><pre class="userinput">uo_empdata luo_empdata[3]</pre><pre class="userinput">luo_empdata[1] = CREATE uo_empdata</pre><pre class="userinput">luo_empdata[2] = CREATE uo_empdata</pre><pre class="userinput">luo_empdata[3] = CREATE uo_empdata</pre><pre class="userinput">luo_empdata = dw_1.Object.Data[1,1,3,4]</pre>

</body>
</html>

