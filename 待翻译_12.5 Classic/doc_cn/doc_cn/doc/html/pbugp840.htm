<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Setting breakpoints</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp839.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp839.htm"/></a></span><span class="nextbtn"><a href="pbugp841.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp841.htm"/></a></span></p><a name="caibjeig"></a><h2>Setting breakpoints</h2>
<p>A breakpoint is a point in your application code where you
want to interrupt the normal execution of the application while
you are debugging. If you suspect a problem is occurring in a particular
script or function call, set a breakpoint at the beginning of the
script or at the line where the function is called.</p>
<p>When you close the debugger, any breakpoints you set are written
to a file called <span class="variable">targetname</span>.<span class="filepath">usr</span>.<span class="filepath">opt</span> in
the same directory as the target, where <span class="variable">targetname</span> is
the name of the target. The breakpoints are available when you reopen
the debugger. When you clear breakpoints, they are permanently removed
from the <span class="filepath">usr</span>.<span class="filepath">opt</span> file
(if it is not marked readonly).</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Sharing targets</span> <p class="notepara">If multiple developers use the same target without using source
control (a practice that is not recommended) individual developers
can save the breakpoints they set in a separate file by adding the
following entry to the [pb] section of their <span class="filepath">pb.ini</span> file:</p><pre class="userinput">UserOptionFileExt=<span class="variable">abc</span></pre>
<p class="notepara">where <span class="variable">abc</span> might be the developer's
name or initials. Breakpoints set by the developer would be saved
in a file called <span class="variable">appname_abc</span>.<span class="filepath">usr</span>.<span class="filepath">opt</span>.</p>
</div><a name="TI1156"></a><div class="brhd"><h4>Setting a simple breakpoint</h4>
<p>This procedure describes setting a breakpoint in the Source
view in the debugger. You can also set a breakpoint by selecting
Add Breakpoint from the pop-up menu in the Script view when you
are not running the debugger.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To set a breakpoint on a line in a script:</p>
<ol type="1"><li class="fi"><p>Display the script in a Source view and
place the cursor where you want to set the breakpoint.</p><p>For how to change the script shown in the
Source view, see <a href="pbugp847.htm#X-REF379091997">"Using the Source view"</a>.</p>  </li>
<li class="ds"><p>Double-click the line or select Add Breakpoint
from the pop-up menu.</p><p>PowerBuilder sets a breakpoint and a red circle displays at
the beginning of the line. If you select a line that does not contain
executable code, PowerBuilder sets the breakpoint at the beginning
of the next executable statement.</p>  </li></ol>
</div></div><a name="TI1157"></a><div class="brhd"><h4>Setting special breakpoints</h4>
<p>Breakpoints can be triggered when a statement has been executed
a specified number of times (an occasional breakpoint), when a specified
expression is true (a conditional breakpoint), or when the value
of a variable changes.</p>
<p>You use the Edit Breakpoints dialog box to set and edit occasional
and conditional breakpoints. You can also use it to set a breakpoint
when the value of a variable changes. The Edit Breakpoints dialog
box opens when you:</p><ul><li><p class="firstbullet">Click the Edit Stop button
on the PainterBar</p>
  </li>
<li><p class="ds">Select Breakpoints from the pop-up menu in the Source,
Variables, Watch, or Breakpoints view</p>  </li>
<li><p class="ds">Select Edit&gt;Breakpoints from the menu bar</p>  </li>
<li><p class="ds">Double-click a line in the Breakpoints view</p>  </li>
</ul>
</div><a name="TI1158"></a><div class="brhd"><h4>Setting occasional and conditional breakpoints</h4>
<p>If you want to check the progress of a loop without interrupting
execution in every iteration, you can set an occasional breakpoint
that is triggered only after a specified number of iterations. To
specify that execution stops only when conditions you specify are
met, set a conditional breakpoint. You can also set both occasional
and conditional breakpoints at the same location.</p>
<ul><li><a name="TI1159"></a><div class="formalpara"><p class="title">If you
specify an occurrence</p><p class="firstpara">Each time PowerBuilder passes through the specified location,
it increments a counter by one. When the counter reaches the number
specified, it triggers the breakpoint and resets the counter to
zero.</p>
</div>  </li>
<li><a name="TI1160"></a><div class="formalpara"><p class="title">If you specify a condition</p><p class="firstpara">Each time PowerBuilder passes through the specified location,
it evaluates the expression. When the expression is true, it triggers
the breakpoint.</p>
</div>  </li>
<li><a name="TI1161"></a><div class="formalpara"><p class="title">If you specify both an occurrence and
a condition</p><p class="firstpara">Each time PowerBuilder passes through the specified location,
it evaluates the expression. When the expression is true, it increments
the counter. When the counter reaches the number specified, it triggers
the breakpoint and resets the counter to zero.</p>
</div><p class="firstbullet">For example, if you specify an occurrence of 3 and the condition <code class="ce">notisNull(val)</code>,
PowerBuilder checks whether <span class="dbobject">val</span> is <span class="cmdname">NULL</span> each
time the statement is reached. The breakpoint is triggered on the
third occurrence of a non-<span class="cmdname">NULL</span> val, then again
on the sixth occurrence, and so forth.</p>
  </li>
</ul>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To set an occasional or conditional breakpoint:</p>
<ol type="1"><li class="fi"><p>On the Location tab in the Edit Breakpoints
dialog box, specify the script and line number where you want the
breakpoint.</p><p>You can select an existing location or select New to enter
a new location.</p><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Set a simple breakpoint first</span> <p class="notepara">You must specify a line that contains executable code. Set
a simple breakpoint on the line before opening the Edit Breakpoints
dialog box to ensure the format and line number are correct.</p>
</div>  </li>
<li class="ds"><p>Specify an integer occurrence, a condition, or
both.</p><p>The condition must be a valid boolean PowerScript expression
(if it is not, the breakpoint is always triggered). PowerBuilder
displays the breakpoint expression in the Edit Breakpoints dialog
box and in the Breakpoints view. When PowerBuilder reaches the location
where the breakpoint is set, it evaluates the breakpoint expression
and triggers the breakpoint only when the expression is true.</p>  </li></ol>
</div></div><a name="TI1162"></a><div class="brhd"><h4>Setting a breakpoint when a variable changes</h4>
<p>You can interrupt execution every time the value of a variable
changes. The variable must be in scope when you set the breakpoint.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To set a breakpoint when a variable changes:</p>
<ol type="1"><li class="fi"><p>Do one of the following:<ul><li><p class="firstbullet">Select
the variable in the Variables view or Watch view and select Break
on Change from the pop-up menu.</p>
  </li>
<li><p class="ds">Drag the variable from the Variables view or Watch
view to the Breakpoints view.</p>  </li>
<li><p class="ds">Select New on the Variable tab in the Edit Breakpoints
dialog box and specify the name of a variable in the Variable box.</p>  </li>
</ul>
                        </p><p>The new breakpoint displays in the Breakpoints view and in
the Edit Breakpoints dialog box if it is open. PowerBuilder watches
the variable at runtime and interrupts execution when the value
of the variable changes.</p>  </li></ol>
</div></div><a name="TI1163"></a><div class="brhd"><h4>Disabling and clearing breakpoints</h4>
<p>If you want to bypass a breakpoint for a specific debugging
session, you can disable it and then enable it again later. If you
no longer need a breakpoint, you can clear it.</p>
<div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To disable a breakpoint:</p>
<ol type="1"><li class="fi"><p>Do one of the following:<ul><li><p class="firstbullet">Click
the red circle next to the breakpoint in the Breakpoints view or Edit
Breakpoints dialog box</p>
  </li>
<li><p class="ds">Select Disable Breakpoint from the pop-up menu in
the Source view</p>  </li>
<li><p class="ds">Select Disable from the pop-up menu in the Breakpoints
view</p>  </li>
</ul>
                        </p><p>The red circle next to the breakpoint is replaced with a white
circle.</p><p>You can enable a disabled breakpoint from the pop-up menus
or by clicking the white circle.</p>  </li></ol>
</div><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Disabling all breakpoints</span> <p class="notepara">To disable all breakpoints, select Disable All from the pop-up
menu in the Breakpoints view.</p>
</div><div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To clear a breakpoint:</p>
<ol type="1"><li class="fi"><p>Do one of the following:<ul><li><p class="firstbullet">Double-click
the line containing the breakpoint in the Source view</p>
  </li>
<li><p class="ds">Select Clear Breakpoint from the pop-up menu in
the Source view</p>  </li>
<li><p class="ds">Select Clear from the pop-up menu in the Breakpoints
view</p>  </li>
<li><p class="ds">Select the breakpoint in the Edit Breakpoints dialog
box and select Clear</p>  </li>
</ul>
                        </p><p>The red circle next to the breakpoint disappears.</p>  </li></ol>
</div><div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Clearing all breakpoints</span> <p class="notepara">To clear all breakpoints, select Clear All in the Edit Breakpoints
dialog box or from the pop-up menu in the Breakpoints view.</p>
</div></div>
</body>
</html>

