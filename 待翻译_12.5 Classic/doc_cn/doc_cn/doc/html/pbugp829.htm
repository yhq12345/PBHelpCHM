<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="../../styles/sybase.css" type="text/css"/>
<title>Specifying data for the OLE object</title>
</head>
<body id="bodyID">
<p class="navbtns"><span class="prevbtn"><a href="pbugp828.htm" title="Previous"><img src="../image/arrow-left.png" alt="pbugp828.htm"/></a></span><span class="nextbtn"><a href="pbugp830.htm" title="Next"><img src="../image/arrow-right.png" alt="pbugp830.htm"/></a></span></p><a name="bfcbhdhd"></a><h2>Specifying data for the OLE object</h2>
<p>You set data specifications for an OLE object in a DataWindow object on
the Data property page in the Properties view. You can also use
the Data property page to modify the data specifications you made
in the wizard for a DataWindow object using the OLE presentation style. </p>
<a name="TI1127"></a><div class="brhd"><h4>What the data is for</h4>
<p>When an OLE object is part of a DataWindow object, you can specify
that some or all of the data the DataWindow object retrieves be transferred
to the OLE object too. You can specify expressions instead of the
actual columns so that the data is grouped, aggregated, or processed
in some way before being transferred.</p>
</div><p>The way the OLE object uses the data depends on the server.
For example, data transferred to Microsoft Excel is displayed as
a spreadsheet. Data transferred to Microsoft Graph populates its
datasheet, which becomes the data being graphed. </p>
<p>Some ActiveX controls do not display data, so you would not
transfer any data to them. For an ActiveX control such as Visual
Speller, you would use automation to process text when the user
requests it.</p>
<a name="TI1128"></a><div class="brhd"><h4>Group By and Target Data boxes</h4>
<p>Two boxes on the Data property page list data columns or expressions:</p><ul><li><a name="TI1129"></a><div class="formalpara"><p class="title">Group By</p><p class="firstpara">Specifies how PowerBuilder groups the data it transfers to the OLE
object. Aggregation functions in the target data expressions use
the groupings specified here.</p>
</div>  </li>
<li><a name="TI1130"></a><div class="formalpara"><p class="title">Target Data</p><p class="firstpara">Specifies the data that you want to transfer to the OLE object. </p>
</div>  </li>
</ul>
</div><a name="TI1131"></a><div class="brhd"><h4>Populating the Group By and Target Data boxes</h4>
<p>If you are using the OLE presentation style, you populated
the Group By and Target Data boxes when you created the DataWindow object.
If you placed an OLE object in an existing DataWindow object, the boxes
are empty. You use the browse buttons next to the Group By and Target
Data boxes to open dialog boxes where you can select the data you
want to use or modify your selections.</p>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Modifying source data</span> <p class="notepara">You cannot modify the source data for the DataWindow object on the
Data property page. Select Design&gt;Data Source from the
menu bar if you need to modify the data source.</p>
</div><div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To select or modify how data will be grouped in
the OLE object:</p>
<ol type="1"><li class="fi"><p>Click the Browse button next to the Group
By box.</p>  </li>
<li class="ds"><p>In the Modify Group By dialog box, drag one or
more columns from the Source Data box to the Group By box.</p><p>You can rearrange columns and specify an expression instead
of the column name if you need to. For more information, see the
next procedure.</p>  </li></ol>
</div><div class="procedure"><p class="title"><img src="../image/proc.png" alt="Steps"/> To select or modify which data columns display
in the OLE object:</p>
<ol type="1"><li class="fi"><p>Click the Browse button next to the Target
Data box.</p>  </li>
<li class="ds"><p>In the Modify Target Data dialog box, drag one
or more columns from the Source Data box to the Target Data box.</p><p>The same source column can appear in both the Group By and
Target Data box.</p>  </li>
<li class="ds"><p>If necessary, change the order of columns by dragging
them up or down within the Target Data box.</p><p>The order of the columns and expressions is important to the
OLE server. You need to know how the server will use the data to
choose the order. </p>  </li>
<li class="ds"><p>Double-click an item in the Target Data box to
specify an expression instead of a column.</p><p>In the Modify Expression dialog box, you can edit the expression
or use the Functions or Columns boxes and the operator buttons to
select elements of the expression. For example, you may want to
specify an aggregation function for a column. Use the range <code class="ce">for
object</code> if you use an aggregation function; for
example, <code class="ce">sum (salary for object)</code>. </p><p>For more information about using operators, expressions, and
functions, see the <span class="doctitle">DataWindow Reference</span>.</p>  </li></ol>
</div></div><a name="TI1132"></a><div class="formalpara"><p class="title">Example of a completed Data property page</p><p class="firstpara">This example of the Data property page specifies two columns
to transfer to Microsoft Graph: <span class="dbobject">city</span> and <span class="dbobject">salary</span>.
Graph expects the first column to be the categories and the second column
to be the data values. The second column is an aggregate so that
the graph will show the sum of all salaries in each city:</p>
</div><div class="fig"><img src="../image/oledw1.gif" alt="oledw1.gif"/></div>
<a name="TI1133"></a><div class="brhd"><h4>Specifying a value for Rows</h4>
<p>The last setting on the Data property page specifies how the
OLE object is associated with rows in the DataWindow object. The selection
(all rows, current row, or page) usually corresponds with the band
where you placed the OLE object, as explained in this table. If
you used the OLE presentation style to create the DataWindow object, this
setting does not display on the property page: the OLE object is
always associated with all the rows in the DataWindow object.</p>
</div><a name="TI1134"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 31-1: Associating an OLE object with rows in the DataWindow</caption>
<tr><th rowspan="1"><p class="firstpara">Range of rows</p>
</th>
<th rowspan="1"><p class="firstpara">When to
use it</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">All</p>
</td>
<td rowspan="1"><p class="firstpara">When the OLE object is in the summary,
header, or footer band, or the foreground or background layer.</p>
<p>Rows must be All and Layer must be Foreground or Background if
you want the user to be able to activate the object.</p>
<p>Target data for all rows is transferred to the object.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Current Row</p>
</td>
<td rowspan="1"><p class="firstpara">When the OLE object is in the detail
band.</p>
<p>There is an instance of the OLE object for every row. Target
data for a single row is transferred to each object.</p>
<p>Because ActiveX controls must be in the foreground or background
layer, they cannot be associated with individual rows in the detail
band.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Page</p>
</td>
<td rowspan="1"><p class="firstpara">When the OLE object is in the group header
or trailer, foreground, or background.</p>
<p>Target data for the rows on the current page is transferred
to the OLE object.</p>
</td>
</tr>
</table>
<div class="note"><img src="../image/note.png" class="inline" alt="Note"/> <span class="title">Range of rows and activating the object</span> <p class="notepara">When the range of rows is Current Row or Page, the user <span class="emphasis">cannot</span> activate
the OLE object. The user can see contents of the object in the form
of an image presented by the server but cannot activate it.</p>
<p class="notepara">If you want the user to activate the object, Rows must be
set to All and Layer on the Position property page must be Foreground
or Background.</p>
</div><a name="TI1135"></a><div class="brhd"><h4>Additional settings in the Properties view</h4>
<p>The Options property page in the OLE object's Properties
view has some additional settings. They are similar to the settings
you can make for the OLE control in a window. These settings display
on the General property page for OLE DataWindow objects. <a href="pbugp829.htm#CACGBCFJ">Table 31-2</a> describes the settings
you can make.</p>
<a name="cacgbcfj"></a><table cellspacing="0" cellpadding="6" border="1" frame="void" rules="all"><caption>Table 31-2: Settings on the OLE object's
Options property page</caption>
<tr><th rowspan="1"><p class="firstpara">Property</p>
</th>
<th rowspan="1"><p class="firstpara">Effect</p>
</th>
</tr>
<tr><td rowspan="1"><p class="firstpara">Client Name</p>
</td>
<td rowspan="1"><p class="firstpara">A name for the OLE object that some server
applications use in the title bar of their window.</p>
<p>Corresponds to the ClientName DataWindow property.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Activation</p>
</td>
<td rowspan="1"><p class="firstpara">How the OLE object is activated. Choices
are:</p><ul><li><a name="TI1136"></a><div class="formalpara"><p class="title">Double click</p><p class="firstpara"> When the user double-clicks on the object, the server application
is activated.</p>
</div>  </li>
<li><a name="TI1137"></a><div class="formalpara"><p class="title">Manual</p><p class="firstpara"> The object can only be activated programmatically.</p>
</div>  </li>
</ul>
<p>The object can always be activated programmatically, regardless
of the Activation setting.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Contents</p>
</td>
<td rowspan="1"><p class="firstpara">Whether the object in the OLE container
is linked or embedded. The default is Any, which allows either method.</p>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Display Type</p>
</td>
<td rowspan="1"><p class="firstpara">What the OLE container displays. You
can choose:</p><ul><li><a name="TI1138"></a><div class="formalpara"><p class="title">Manual</p><p class="firstpara"> Display a representation of the object, reduced to fit within
the container.</p>
</div>  </li>
<li><a name="TI1139"></a><div class="formalpara"><p class="title">Icon</p><p class="firstpara"> Display the icon associated with the data. This is usually
an icon provided by the server application.</p>
</div>  </li>
</ul>
</td>
</tr>
<tr><td rowspan="1"><p class="firstpara">Link Update</p>
</td>
<td rowspan="1"><p class="firstpara">When the object in the OLE container
is linked, the method for updating link information. Choices are:</p><ul><li><a name="TI1140"></a><div class="formalpara"><p class="title">Automatic</p><p class="firstpara"> If the link is broken and PowerBuilder cannot find the linked
file, it displays a dialog box in which the user can specify the
file.</p>
</div>  </li>
<li><a name="TI1141"></a><div class="formalpara"><p class="title">Manual</p><p class="firstpara"> If the link is broken, the object cannot be activated.</p>
</div><p class="firstbullet">You can let the user re-establish the link in a script using the <span class="cmdname">UpdateLinksDialog</span> function.</p>
  </li>
</ul>
</td>
</tr>
</table>
</div>
</body>
</html>

